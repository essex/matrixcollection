var indexSectionsWithContent =
{
  0: "ls",
  1: "s",
  2: "s",
  3: "s",
  4: "s",
  5: "ls"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "typedefs",
  4: "groups",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Typedefs",
  4: "Modules",
  5: "Pages"
};

