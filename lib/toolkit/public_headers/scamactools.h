#ifndef SCAMACTOOLS_H
#define SCAMACTOOLS_H

/** \file 
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017
 *  \brief  @ref toolkit top-level header file
 *  \ingroup toolkit
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "scamactools.have"

#include "scamac_statistics.h"

#ifdef HAVE_SCAMAC_PLOT_H
 #include "scamac_plot.h"
#endif

#ifdef HAVE_SCAMAC_SPECTRUM_H
 #include "scamac_spectrum.h"
#endif

#ifdef HAVE_SCAMAC_LANCZOS_H
 #include "scamac_lanczos.h"
#endif

#ifdef HAVE_SCAMAC_CMK_H
 #include "scamac_cmk.h"
#endif

#ifdef __cplusplus
}
#endif


#endif /* SCAMACTOOLS_H */
