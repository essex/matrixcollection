#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scamac_cmk.h"
#include "scamac_sparsemat_internal.h"
#include "scamac_sparsemat_diagnostics.h"


static int cmp_fst(  const void *a, const void *b ) {
  int x,y;
  x=*(int *) a;
  y=*(int *) b;
  return (x-y);
}

ScamacErrorCode scamac_cmk(const ScamacMatrix *sm, int ** perm) {
  if (!sm)   { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!perm) { return scamac_error_set_par(SCAMAC_ENULL,2); }

  int dim = sm->nr;
  if (dim<1) { return scamac_error_set_par(SCAMAC_EINPUT,1); }

  int *my_perm;

  if (!(*perm)) {
    my_perm = malloc(dim * sizeof *my_perm);
    *perm = my_perm;
  } else {
    my_perm = *perm;
  }

  int maxrowlength= scamac_sparsemat_maxrowlength(sm);

  int * vdeg   = malloc(dim * sizeof * vdeg);
  int * nodelist = malloc(dim * sizeof * nodelist);
  int * sortarray = malloc(2 * maxrowlength * sizeof * sortarray);
  if (!vdeg || !nodelist || !sortarray) {
    return SCAMAC_EMALLOCFAIL;
  }

  int i, idx;

  for (i=0; i<dim; i++) {
    vdeg[i]=sm->rptr[i+1]-sm->rptr[i];
  }



  int dummy=vdeg[0];
  idx=0;
  for (i=1; i<dim; i++) {
    if (vdeg[i]<dummy) {
      dummy=vdeg[i];
      idx=i;
    }
  }

  for (i=0; i<dim; i++) {
    my_perm[i]=-1;
  }

  nodelist[0]=idx;
  my_perm[idx]=0;
  int ndone=1;

  for (idx=0; idx<dim; idx++) {
    int nidx=nodelist[idx];
    int nnew=0;
    for (i=0; i<sm->rptr[nidx+1]-sm->rptr[nidx]; i++) {
      if (my_perm[sm->cind[i+sm->rptr[nidx]]] < 0) {
        nodelist[ndone+nnew]=sm->cind[i+sm->rptr[nidx]];
        nnew++;
      }
    }
    SCAMAC_ASSERT (nnew + ndone <= dim);
    

    for (i=0; i<nnew; i++) {
      sortarray[2*i  ]=vdeg[nodelist[ndone+i]];
      sortarray[2*i+1]=nodelist[ndone+i];
    }
    qsort(sortarray,nnew, 2* sizeof * sortarray,cmp_fst);
    for (i=0; i<nnew; i++) {
      nodelist[ndone+i]=sortarray[2*i+1];
      my_perm[sortarray[2*i+1]]=ndone+i;
    }

    ndone=ndone+nnew;
  }

  free(vdeg);
  free(nodelist);
  free(sortarray);

  return SCAMAC_EOK;

}


ScamacErrorCode scamac_sparsemat_permute(const ScamacMatrix *sm, const int *perm, ScamacMatrix ** sm_perm) {
  if (!sm)      { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!perm)    { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!sm_perm) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  
  if (sm->nr != sm->nc) {
    return scamac_error_set_par(SCAMAC_EINPUT,1);
  }

  int dim = sm->nr;
  if (dim<1) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  
  int * invperm = malloc(sm->nr * sizeof *invperm);
  if (!invperm) { return SCAMAC_EMALLOCFAIL; }
  int i;
  for (i=0; i<dim; i++) {
    invperm[perm[i]]=i;
  }
  
  ScamacMatrix * smp;
  if (*sm_perm) {
    smp = *sm_perm;
    if ((smp->nr != dim) || (smp->nc != dim) || (smp->nemax < sm->ne)) { return scamac_error_set_par(SCAMAC_EINPUT,3); }
  } else {
    ScamacErrorCode err = scamac_sparsemat_alloc(sm->nr,sm->nc,sm->ne,sm->valtype, &smp);
    if (err) { return scamac_error_set_internal(err); }
    *sm_perm = smp;
  }
  
  ScamacIdx n = 0;
  smp->rptr[0]=0;
  ScamacIdx idx;
  for (idx=0; idx<sm->nr; idx++) {
    ScamacIdx orig_idx = invperm[idx];
    int k = sm->rptr[orig_idx+1] - sm->rptr[orig_idx];
    int i;
    for (i=0; i<k; i++) {
      smp->cind[n+i]=perm[sm->cind[sm->rptr[orig_idx]+i]];
    }
    memcpy(&(smp->val[n]), &(sm->val[sm->rptr[orig_idx]]), k * sizeof *(sm->val));
    n=n+k;
    smp->rptr[idx+1]=n;
  }
  SCAMAC_ASSERT (n == sm->ne);
  smp->ne=sm->ne;

  free(invperm);
  return SCAMAC_EOK;
}

