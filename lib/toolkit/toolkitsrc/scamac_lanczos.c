#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <complex.h>

#include "scamac_cblas.h"
#include "scamac_clapack.h"

#include "scamac_lanczos.h"
#include "scamac_vector.h"
#include "scamac_densemat.h"

ScamacErrorCode scamac_lanczos_ev_real(const ScamacMatop *sm, double tol, double *ev1, double *ev2, double *eps1, double *eps2) {
  ScamacErrorCode err;

  if (!sm)   { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!ev1)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!ev2)  { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (!eps1) { return scamac_error_set_par(SCAMAC_ENULL,4); }
  if (!eps2) { return scamac_error_set_par(SCAMAC_ENULL,5); }

  if (scamac_matop_query_valtype(sm) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (scamac_matop_query_nr(sm) != scamac_matop_query_nc(sm))               { return scamac_error_set_par(SCAMAC_EINPUT,1); }

  int nl=200;
  int il=0;


  int nv = scamac_matop_query_nc(sm);

  ScamacVectorMem *vmem = NULL;
  ScamacVector *x = NULL;
  ScamacVector *y = NULL;
  double *d=NULL,*e=NULL,*z=NULL,*work=NULL,*dhp=NULL,*ehp=NULL;


  err = scamac_vectormem_alloc(nv,2,SCAMAC_VAL_REAL, &vmem);    SCAMAC_GOTOERR(err,cleanup);
  err = scamac_vector_view_single(vmem,0,&x);                   SCAMAC_GOTOERR(err,cleanup);
  err = scamac_vector_view_single(vmem,1,&y);                   SCAMAC_GOTOERR(err,cleanup);
  
  err = scamac_densemat_real_alloc(nl, 1,     &d);    SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(nl, 1,     &e);    SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(nl, 1,     &dhp);  SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(nl, 1,     &ehp);  SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(nl, nl,    &z);    SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(2*nl-2, 1, &work); SCAMAC_GOTOERR(err,cleanup);
  
  char seed[9]; snprintf(seed,9,"zufall");
  err=scamac_vector_random(seed,true,true,x);  SCAMAC_GOTOERR(err,cleanup);

  int happybreak = 0;

  for (il=0; il<nl; il++) {
    if (il==0) {
      err=scamac_matop_mvm(sm,x,y, 1.0,0.0,0.0);     SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_dot(x,y,&d[0]);              SCAMAC_GOTOERR(err,cleanup);
      err=scamac_svector_axpby(x,y,-d[0], 1.0);      SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_nrm2(y,&e[0]);               SCAMAC_GOTOERR(err,cleanup);
      if ( fabs(e[0]) < DBL_EPSILON ) {
        happybreak=1;
      } else {
        err = scamac_svector_scal(y, 1.0/e[0]);      SCAMAC_GOTOERR(err,cleanup);
      }
    } else if ( il%2 == 0 ) {
      err=scamac_matop_mvm(sm,x,y, 1.0, -e[il-1],0.0);     SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_dot(x,y,&d[il]);                   SCAMAC_GOTOERR(err,cleanup);
      err=scamac_svector_axpby(x,y,-d[il],1.0);            SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_nrm2(y,&e[il]);                    SCAMAC_GOTOERR(err,cleanup);
      if ( fabs(e[il])< DBL_EPSILON ) {
        happybreak=1;
      } else {
        err=scamac_svector_scal(y,1.0/e[il]);              SCAMAC_GOTOERR(err,cleanup);
      }
    } else {
      err=scamac_matop_mvm(sm,y,x, 1.0, -e[il-1],0.0);     SCAMAC_GOTOERR(err,cleanup);
      
      err=scamac_vector_dot(y,x,&d[il]);        SCAMAC_GOTOERR(err,cleanup);
      err=scamac_svector_axpby(y,x,-d[il],1.0); SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_nrm2(x,&e[il]);         SCAMAC_GOTOERR(err,cleanup);
      if ( fabs(e[il]) < DBL_EPSILON ) {
        happybreak=1;
      } else {
        err=scamac_svector_scal(x,1.0/e[il]);   SCAMAC_GOTOERR(err,cleanup);
      }
    }

    if (il==0) {
      *ev1=d[0];
      *ev2=d[0];
      *eps1=0.0;
      *eps2=0.0;
    } else {
      cblas_dcopy(il+1,d,1,dhp,1);
      cblas_dcopy(il+1,e,1,ehp,1);
      char compz='I';
      int ilplus1 = il+1;
      int INFO;
      dsteqr_(&compz, &ilplus1,dhp,ehp,z,&nl,work,&INFO);
      if (INFO) { err = SCAMAC_EFAIL; SCAMAC_GOTOERR(err,cleanup); }
      *ev1=dhp[0];
      *ev2=dhp[il];
      *eps1=fabs(z[il+nl*0])*fabs(e[il]);
      *eps2=fabs(z[il+nl*il])*fabs(e[il]);
      if (*ev2-*ev1>sqrt(DBL_EPSILON)) {
        if ( (*eps1/(*ev2-*ev1)<tol) && (*eps2/(*ev2-*ev1)<tol) ) {
          break;
        }
      } else {
        if ( (*eps1<tol) && (*eps2<tol) ) {
          break;
        }
      }
    }

    if (happybreak) break;

  }

cleanup:

  free(d);
  free(e);
  free(dhp);
  free(ehp);
  free(z);
  free(work);

  scamac_vector_free(x);
  scamac_vector_free(y);
  scamac_vectormem_free(vmem);

  if (err) {
    return err;
  } else if (il >= nl) {
    return SCAMAC_ENOTCONVERGED;
  } else {
    return SCAMAC_EOK;
  }

}



ScamacErrorCode scamac_lanczos_ev_cplx(const ScamacMatop *sm, double tol, double *ev1, double *ev2, double *eps1, double *eps2) {
  ScamacErrorCode err;

  if (!sm)   { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!ev1)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!ev2)  { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (!eps1) { return scamac_error_set_par(SCAMAC_ENULL,4); }
  if (!eps2) { return scamac_error_set_par(SCAMAC_ENULL,5); }

  if (scamac_matop_query_valtype(sm) != SCAMAC_VAL_COMPLEX)   { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (scamac_matop_query_nr(sm) != scamac_matop_query_nc(sm)) { return scamac_error_set_par(SCAMAC_EINPUT,1); }

  int nl=200;
  int il=0;
  
  int nv = scamac_matop_query_nc(sm);
  
  ScamacVectorMem *vmem = NULL;
  ScamacVector *x = NULL;
  ScamacVector *y = NULL;
  double *d=NULL,*e=NULL,*z=NULL,*work=NULL,*dhp=NULL,*ehp=NULL;


  err = scamac_vectormem_alloc(nv,2,SCAMAC_VAL_COMPLEX, &vmem);    SCAMAC_GOTOERR(err,cleanup);
  err = scamac_vector_view_single(vmem,0,&x);                      SCAMAC_GOTOERR(err,cleanup);
  err = scamac_vector_view_single(vmem,1,&y);                      SCAMAC_GOTOERR(err,cleanup);
  
  err = scamac_densemat_real_alloc(nl, 1,     &d);    SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(nl, 1,     &e);    SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(nl, 1,     &dhp);  SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(nl, 1,     &ehp);  SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(nl, nl,    &z);    SCAMAC_GOTOERR(err,cleanup);
  err = scamac_densemat_real_alloc(2*nl-2, 1, &work); SCAMAC_GOTOERR(err,cleanup);
  
  char seed[9]; snprintf(seed,9,"zufall");
  err=scamac_vector_random(seed,true,true,x);       SCAMAC_GOTOERR(err,cleanup);

  int happybreak = 0;

  for (il=0; il<nl; il++) {
    if (il==0) {
      err=scamac_matop_mvm_cplx(sm,x,y, 1.0,0.0,0.0); SCAMAC_GOTOERR(err,cleanup);
      double complex hpc;
      err=scamac_vector_zdot(x,y,&hpc); SCAMAC_GOTOERR(err,cleanup);
      d[0]=creal(hpc);
      err=scamac_svector_zaxpby(x,y,-d[0], 1.0); SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_nrm2(y,&e[0]); SCAMAC_GOTOERR(err,cleanup);
      if ( fabs(e[0]) < DBL_EPSILON ) {
        happybreak=1;
      } else {
        err = scamac_svector_zscal(y, 1.0/e[0]); SCAMAC_GOTOERR(err,cleanup);
      }
    } else if ( il%2 == 0 ) {
      err=scamac_matop_mvm_cplx(sm,x,y, 1.0, -e[il-1],0.0); SCAMAC_GOTOERR(err,cleanup);
      double complex hpc;
      err=scamac_vector_zdot(x,y,&hpc); SCAMAC_GOTOERR(err,cleanup);
      d[il]=creal(hpc);
      err=scamac_svector_zaxpby(x,y,-d[il],1.0); SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_nrm2(y,&e[il]); SCAMAC_GOTOERR(err,cleanup);
      if ( fabs(e[il])< DBL_EPSILON ) {
        happybreak=1;
      } else {
        err=scamac_svector_zscal(y,1.0/e[il]); SCAMAC_GOTOERR(err,cleanup);
      }
    } else {
      err=scamac_matop_mvm_cplx(sm,y,x, 1.0, -e[il-1],0.0); SCAMAC_GOTOERR(err,cleanup);
      double complex hpc;
      err=scamac_vector_zdot(y,x,&hpc); SCAMAC_GOTOERR(err,cleanup);
      d[il]=creal(hpc);
      err=scamac_svector_zaxpby(y,x,-d[il],1.0); SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_nrm2(x,&e[il]); SCAMAC_GOTOERR(err,cleanup);
      if ( fabs(e[il]) < DBL_EPSILON ) {
        happybreak=1;
      } else {
        err=scamac_svector_zscal(x,1.0/e[il]); SCAMAC_GOTOERR(err,cleanup);
      }
    }

    if (il==0) {
      *ev1=d[0];
      *ev2=d[0];
      *eps1=0.0;
      *eps2=0.0;
    } else {
      cblas_dcopy(il+1,d,1,dhp,1);
      cblas_dcopy(il+1,e,1,ehp,1);
      char compz='I';
      int ilplus1 = il+1;
      int INFO;
      dsteqr_(&compz, &ilplus1,dhp,ehp,z,&nl,work,&INFO);
      if (INFO) { err = SCAMAC_EFAIL; SCAMAC_GOTOERR(err,cleanup); }
      *ev1=dhp[0];
      *ev2=dhp[il];
      *eps1=fabs(z[il+nl*0])*fabs(e[il]);
      *eps2=fabs(z[il+nl*il])*fabs(e[il]);
      if (*ev2-*ev1>sqrt(DBL_EPSILON)) {
        if ( (*eps1/(*ev2-*ev1)<tol) && (*eps2/(*ev2-*ev1)<tol) ) {
          break;
        }
      } else {
        if ( (*eps1<tol) && (*eps2<tol) ) {
          break;
        }
      }
    }

    if (happybreak) break;

  }

cleanup:

  free(d);
  free(e);
  free(dhp);
  free(ehp);
  free(z);
  free(work);

  scamac_vector_free(x);
  scamac_vector_free(y);
  scamac_vectormem_free(vmem);

  if (err) {
    return err;
  } else if (il >= nl) {
    return SCAMAC_ENOTCONVERGED;
  } else {
    return SCAMAC_EOK;
  }
}






ScamacErrorCode scamac_lanczos_ev(const ScamacMatop *sm, double tol, double *ev1, double *ev2, double *eps1, double *eps2) {
  ScamacErrorCode err;
  if (scamac_matop_query_valtype(sm) == SCAMAC_VAL_REAL) {
    err = scamac_lanczos_ev_real(sm, tol, ev1, ev2, eps1, eps2);
  } else if (scamac_matop_query_valtype(sm) == SCAMAC_VAL_COMPLEX) {
    err = scamac_lanczos_ev_cplx(sm, tol, ev1, ev2, eps1, eps2);
  } else {
    err = SCAMAC_ECORRUPTED;
  }
  return err;
}
