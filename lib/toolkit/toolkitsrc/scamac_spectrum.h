/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Compute entire spectrum of (sparse) matrix.
 *  \warning Use only for small matrices.
 *  \ingroup public
 */

#ifndef SCAMAC_SPECTRUM_H
#define SCAMAC_SPECTRUM_H

#include "scamac_error.h"
#include "scamac_sparsemat.h"

/** \brief Compute spectrum of real symmetric matrix.
 *  \ingroup toolkit
 * \param[in] sm sparse matrix
 * \param[out] spec pointer to array that contains the spectrum (allocated in routine)
 * \return Error code
 */
ScamacErrorCode scamac_spectrum_real_symmetric(const ScamacMatrix * sm, double **spec);
/** \brief Compute spectrum of general symmetric matrix.
 *  \ingroup toolkit
 * \param[in] sm sparse matrix
 * \param[out] spec pointer to array that contains the spectrum (allocated in routine)
 * \return Error code
 */
ScamacErrorCode scamac_spectrum_real_general  (const ScamacMatrix * sm, double **spec);
/** \brief Compute spectrum of hermitian complex matrix.
 *  \ingroup toolkit
 * \param[in] sm sparse matrix
 * \param[out] spec pointer to array that contains the spectrum (allocated in routine)
 * \return Error code
 */
ScamacErrorCode scamac_spectrum_cplx_hermitian(const ScamacMatrix * sm, double **spec);
/** \brief Compute spectrum of general complex matrix.
 *  \ingroup toolkit
 * \param[in] sm sparse matrix
 * \param[out] spec pointer to array that contains the spectrum (allocated in routine)
 * \return Error code
 */
ScamacErrorCode scamac_spectrum_cplx_general  (const ScamacMatrix * sm, double **spec);

#endif /* SCAMAC_SPECTRUM_H */
