/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Compute (approximate) bounds on the spectrum
 *  \ingroup public
 */

#ifndef SCAMAC_LANCZOS_H
#define SCAMAC_LANCZOS_H

#include "scamac_matop.h"
#include "scamac_generator.h"

/** \brief Lanczos for extremal eigenvalues of sparse matrix
 *  \ingroup toolkit
 */
//ScamacErrorCode scamac_lanczos_ev_mat(const ScamacMatrix *sm, double tol, double *ev1, double *ev2, double *eps1, double *eps2);
ScamacErrorCode scamac_lanczos_ev(const ScamacMatop *sm, double tol, double *ev1, double *ev2, double *eps1, double *eps2);

// the "working horses"
/** \brief Lanczos for extremal eigenvalues of real (symmetric) matrix (computational routine)
 *  \ingroup toolkit
 */
ScamacErrorCode scamac_lanczos_ev_real(const ScamacMatop *sm, double tol, double *ev1, double *ev2, double *eps1, double *eps2);
/** \brief Lanczos for extremal eigenvalues of complex (hermitian) matrix (computational routine)
 *  \ingroup toolkit
 */
ScamacErrorCode scamac_lanczos_ev_cplx(const ScamacMatop *sm, double tol, double *ev1, double *ev2, double *eps1, double *eps2);

#endif /* SCAMAC_LANCZOS_H */
