/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Plot the sparsity pattern
 *  \ingroup public
 */

#ifndef SCAMAC_PLOT_H
#define SCAMAC_PLOT_H

#include "scamac_error.h"
#include "scamac_statistics.h" // for scamac_pattern_st

// plot sparsity pattern "one pixel per entry". Use only for small matrices
ScamacErrorCode scamac_plot_pattern_onetoone(const scamac_matrix_pattern_st * pat, const char * filename);

/** \brief write pattern to file
 *  \ingroup toolkit
 */
ScamacErrorCode scamac_plot_pattern(const scamac_matrix_pattern_st * pat, int downscale, const char * filename);


#endif /* SCAMAC_PLOT_H */
