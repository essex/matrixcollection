/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   April 2018 --- today
 *  \brief  Cuthill-McKee algorithm
 *  \ingroup public
 */


#ifndef SCAMAC_CMK_H
#define SCAMAC_CMK_H

#include "scamac_error.h"
#include "scamac_sparsemat.h"

/** \brief return Cuthill-McKee permutation
 *  \ingroup toolkit
 */
ScamacErrorCode scamac_cmk(const ScamacMatrix *sm, int ** perm);

/** \brief permute sparse matrix
 *  \ingroup toolkit
 */
ScamacErrorCode scamac_sparsemat_permute(const ScamacMatrix *sm, const int *perm, ScamacMatrix ** sm_perm);


#endif /* SCAMAC_CMK_H */
