#include <stdio.h>
#include <string.h>

#include "scamac_hash.h"


const char str1[]="";
const char dig1[]="d41d8cd98f00b204e9800998ecf8427e";

const char str2[]="0123456789";
const char dig2[]="781e5e245d69b566979b86e28d23f2c7";

const char str3[]="This test certainly will pass.";
const char dig3[]="9a49eca0b84ebeec874323ec7dfc51f2";

const char str4[]="There's a feeling I get when I look to the west. And my spirit is crying for leaving.";
const char dig4[]="c107e76573b857f6abc8bdab2e4471a1";

#define test_hash(n) \
do { \
  scamac_hash_init(&st); \
  scamac_hash_process(&st, strlen(str##n) * sizeof (*str##n), (void *) str##n); \
  scamac_hash_finish(&st); \
  scamac_hash_digest(&st, dig); \
  if (compare_digest(dig, dig##n)) { return 1; } \
} while (0)

int compare_digest(uint32_t digest[4], const char digstr[]) {
  uint8_t * d = (uint8_t *) digest;
  char hex[3];
  char hexcmp[3];
  int i;
  for (i=0;i<16;i++) {
    snprintf(hex, 3, "%02"PRIx8, d[i]);
    snprintf(hexcmp, 3, "%2s", digstr + 2*i);
    if (strcmp(hex, hexcmp)) {
      return 1;
    }
  }
  return 0;
}

int main(int argc, char * argv[]) {

  uint32_t dig[4];
  ScamacHashState st;
    
  test_hash(1);
  test_hash(2);
  test_hash(3);
  test_hash(4);

  return 0;
}
