#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>

#include <scamac_dof_lattice.h>


int main(int argc, char * argv[]) {

  scamac_dof_lattice_square_st dof;

  dof.Lx=atoi(argv[1]);
  dof.Ly=atoi(argv[2]);
  dof.mx=atoi(argv[3]);
  dof.my=atoi(argv[4]);
  dof.flip_innerrow=false;
  dof.flip_outerrow=false;

  ScamacIdx idx;
  for (idx=0;idx<scamac_dof_lattice_square_ns(&dof);idx++) {
    ScamacIdx x,y;
    scamac_dof_lattice_square_decode(&dof,idx, &x,&y);
    ScamacIdx idx2;
    idx2=scamac_dof_lattice_square_encode(&dof, x,y);
    if (idx!=idx2) { return 1; }
  }

  return 0;
}
