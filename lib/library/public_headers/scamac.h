#ifndef SCAMAC_H
#define SCAMAC_H

/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  @ref library top-level header file
 *  \ingroup library
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "scamac_defs.h"
#include "scamac_error.h"
#include "scamac_inttypes.h"
#include "scamac_generator.h"
#include "scamac_collection.h"
#include "scamac_benchmarks.h"

#ifdef __cplusplus
}
#endif

#endif /* SCAMAC_H */
