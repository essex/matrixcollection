/* *** *** *** *** *** *** *** *** *** *** */
/* autogenerated by ScaMaC_build_generator */
/* Fri, 24 May 2019 12:40:36 +0200         */
/* *** *** *** *** *** *** *** *** *** *** */
#ifndef SCAMAC_MATRIX_HARMONIC_H
#define SCAMAC_MATRIX_HARMONIC_H

#include "scamac_internal.h"
#include "scamac_sparserow.h"

typedef struct {
  double omega;
  double lambda;
  int n_bos;
} scamac_matrix_Harmonic_params_st;

ScamacErrorCode scamac_matrix_Harmonic_check(const scamac_matrix_Harmonic_params_st * par, char ** desc);
ScamacErrorCode scamac_matrix_Harmonic_tables_create(const scamac_matrix_Harmonic_params_st * par, void ** tab, scamac_info_st * info);
ScamacErrorCode scamac_matrix_Harmonic_generate_row(const scamac_matrix_Harmonic_params_st * par, const void * tab, void * ws, ScamacIdx irow, ScamacFlag flag, scamac_sparserow_real_st * row);

#endif /* SCAMAC_MATRIX_HARMONIC_H */
