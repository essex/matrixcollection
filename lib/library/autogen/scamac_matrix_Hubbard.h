/* *** *** *** *** *** *** *** *** *** *** */
/* autogenerated by ScaMaC_build_generator */
/* Fri, 24 May 2019 12:40:36 +0200         */
/* *** *** *** *** *** *** *** *** *** *** */
#ifndef SCAMAC_MATRIX_HUBBARD_H
#define SCAMAC_MATRIX_HUBBARD_H

#include "scamac_internal.h"
#include "scamac_sparserow.h"
#include "scamac_option_inc.h"
#include "scamac_rng.h"
#include "scamac_multidx.h"
#include "scamac_dof_fermions.h"

typedef struct {
  double t;
  double U;
  int n_sites;
  int n_fermions;
  scamac_option_ty boundary_conditions;
  double ranpot;
  scamac_rng_seed_ty seed;
} scamac_matrix_Hubbard_params_st;

typedef struct {
  ScamacIdx ns;
  ScamacIdx maxnzrow;
  int maxrowlength;
  scamac_multidx_st *midx;
  scamac_dof_fermions_st ** dof;
  double * onsite;
} scamac_matrix_Hubbard_tables_st;

typedef struct {
  scamac_rep_fermions_st ** rep, ** repinit;
  scamac_dof_fermions_st ** dof;
} scamac_matrix_Hubbard_work_st;

ScamacErrorCode scamac_matrix_Hubbard_check(const scamac_matrix_Hubbard_params_st * par, char ** desc);
ScamacErrorCode scamac_matrix_Hubbard_tables_create(const scamac_matrix_Hubbard_params_st * par, scamac_matrix_Hubbard_tables_st ** tab, scamac_info_st * info);
ScamacErrorCode scamac_matrix_Hubbard_tables_destroy(scamac_matrix_Hubbard_tables_st * tab);
ScamacErrorCode scamac_matrix_Hubbard_work_alloc(const scamac_matrix_Hubbard_params_st * par, const scamac_matrix_Hubbard_tables_st * tab, scamac_matrix_Hubbard_work_st ** ws);
ScamacErrorCode scamac_matrix_Hubbard_work_free(scamac_matrix_Hubbard_work_st * ws);
ScamacErrorCode scamac_matrix_Hubbard_generate_row(const scamac_matrix_Hubbard_params_st * par, const scamac_matrix_Hubbard_tables_st * tab, scamac_matrix_Hubbard_work_st * ws, ScamacIdx irow, ScamacFlag flag, scamac_sparserow_real_st * row);

#endif /* SCAMAC_MATRIX_HUBBARD_H */
