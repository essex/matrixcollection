/* *** *** *** *** *** *** *** *** *** *** */
/* autogenerated by ScaMaC_build_generator */
/* Fri, 24 May 2019 12:40:36 +0200         */
/* *** *** *** *** *** *** *** *** *** *** */
#ifndef SCAMAC_MATRIX_TOPINS_H
#define SCAMAC_MATRIX_TOPINS_H

#include "scamac_internal.h"
#include "scamac_sparserow.h"
#include "scamac_option_inc.h"
#include "scamac_rng.h"
#include "scamac_dof_lattice.h"

typedef struct {
  int Lx;
  int Ly;
  int Lz;
  double t;
  double m;
  double D1;
  double D2;
  double ranpot;
  scamac_option_ty boundary_conditions;
  scamac_rng_seed_ty seed;
  int mx;
  int my;
  int mz;
} scamac_matrix_TopIns_params_st;

typedef struct {
  ScamacIdx ns;
  ScamacIdx maxnzrow;
  scamac_dof_lattice_cubic_st dof;
} scamac_matrix_TopIns_tables_st;

typedef struct {
  scamac_ransrc_st * rng;
} scamac_matrix_TopIns_work_st;

ScamacErrorCode scamac_matrix_TopIns_check(const scamac_matrix_TopIns_params_st * par, char ** desc);
ScamacErrorCode scamac_matrix_TopIns_tables_create(const scamac_matrix_TopIns_params_st * par, scamac_matrix_TopIns_tables_st ** tab, scamac_info_st * info);
ScamacErrorCode scamac_matrix_TopIns_tables_destroy(scamac_matrix_TopIns_tables_st * tab);
ScamacErrorCode scamac_matrix_TopIns_work_alloc(const scamac_matrix_TopIns_params_st * par, const scamac_matrix_TopIns_tables_st * tab, scamac_matrix_TopIns_work_st ** ws);
ScamacErrorCode scamac_matrix_TopIns_work_free(scamac_matrix_TopIns_work_st * ws);
ScamacErrorCode scamac_matrix_TopIns_generate_row(const scamac_matrix_TopIns_params_st * par, const scamac_matrix_TopIns_tables_st * tab, scamac_matrix_TopIns_work_st * ws, ScamacIdx irow, ScamacFlag flag, scamac_sparserow_cplx_st * row);

#endif /* SCAMAC_MATRIX_TOPINS_H */
