#include <scamacvecmat.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {

  ScamacVectorMem * vmem = NULL;
  ScamacVector * v = NULL;

  ScamacInt nv = 100;
  ScamacInt m = 10;

  double * dots = malloc(m * sizeof *dots);

  
  SCAMAC_TRY(scamac_vectormem_alloc(nv, m, SCAMAC_VAL_REAL, &vmem));
  SCAMAC_TRY(scamac_vector_view_range(vmem, 0,m-1, &v));
  SCAMAC_TRY(scamac_vector_zero(v));
  SCAMAC_TRY(scamac_vector_dot(v, v, dots));

  int i;
  for (i=0;i<m;i++) {
    printf("%d %e\n",i,dots[i]);
  }
  
  SCAMAC_TRY(scamac_vector_free(v));
  SCAMAC_TRY(scamac_vectormem_free(vmem));
  
  return 0;
}
