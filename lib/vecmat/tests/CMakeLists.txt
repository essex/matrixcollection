
if (HAVE_SCAMAC_MATOP_P)
  add_executable(TestVector scamac_test_vector.c)
  target_link_libraries(TestVector scamac scamacvecmat)
  add_test(TestVector TestVector)
endif()
