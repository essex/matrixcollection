include(TargetHeaders)

ADD_LIBRARY(scamacvecmat ${SCAMAC_LIB_TYPE} "")

target_link_libraries(scamacvecmat PRIVATE scamac)

target_include_directories(scamacvecmat PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src_densemat>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src_sparsemat>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/private_headers>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/cblas>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/cblas>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/public_headers>
  $<INSTALL_INTERFACE:${SCAMAC_INCDIR}>
  )

if (SCAMAC_BUILD_MATOP)
  target_include_directories(scamacvecmat PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src_vecmat_generic>
    )
endif()
if (SCAMAC_BUILD_MATOP_INTERNAL)
  target_include_directories(scamacvecmat PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src_vecmat_internal>
    )
endif()


target_sources(scamacvecmat PRIVATE src_densemat/scamac_densemat.c)
target_headers(scamacvecmat PUBLIC  src_densemat/scamac_densemat.h)

target_sources(scamacvecmat PRIVATE src_sparsemat/scamac_sparsemat.c src_sparsemat/scamac_sparsemat_io.c src_sparsemat/scamac_sparsemat_diagnostics.c)
target_headers(scamacvecmat PUBLIC  src_sparsemat/scamac_sparsemat.h src_sparsemat/scamac_sparsemat_io.h src_sparsemat/scamac_sparsemat_diagnostics.h)

if (HAVE_PACK_BLAS)
  configure_file(cblas/scamac_cblas.h.in cblas/scamac_cblas.h)
  target_include_directories(scamacvecmat PRIVATE cblas ${CMAKE_CURRENT_BINARY_DIR}/cblas ${PACK_BLAS_INCLUDE_DIR})
  target_include_directories(scamacvecmat PUBLIC
    $<BUILD_INTERFACE:${PACK_BLAS_INCLUDE_DIR}>
    )
  target_link_libraries(scamacvecmat PRIVATE ${PACK_BLAS_LIBRARIES})
  set(HAVE_SCAMAC_CBLAS_H TRUE             )
  set(HAVE_SCAMAC_CBLAS_H TRUE PARENT_SCOPE)
endif()

if (HAVE_PACK_LAPACK)
  target_link_libraries(scamacvecmat PRIVATE ${PACK_LAPACK_LIBRARIES})
  set(HAVE_SCAMAC_CLAPACK_H TRUE             )
  set(HAVE_SCAMAC_CLAPACK_H TRUE PARENT_SCOPE)
endif()
if (HAVE_SCAMAC_CBLAS_H AND HAVE_SCAMAC_CLAPACK_H)
  target_sources(scamacvecmat PRIVATE src_densemat/scamac_densemat_lapack.c)
  target_headers(scamacvecmat PUBLIC  src_densemat/scamac_densemat_lapack.h)
  set(HAVE_SCAMAC_DENSEMAT_LAPACK_H TRUE             )
  set(HAVE_SCAMAC_DENSEMAT_LAPACK_H TRUE PARENT_SCOPE)
endif()
if (SCAMAC_BUILD_MATOP_INTERNAL)
  target_headers(scamacvecmat PUBLIC  src_vecmat_generic/scamac_vector.h src_vecmat_generic/scamac_matop.h src_vecmat_generic/scamac_matop_analysis.h)
  target_sources(scamacvecmat PRIVATE src_vecmat_internal/scamac_sparsemat_mvm.c src_vecmat_internal/scamac_vector.c src_vecmat_internal/scamac_vector_orthogonalize.c src_vecmat_internal/scamac_matop.c src_vecmat_generic/scamac_matop_analysis.c)
  set(HAVE_SCAMAC_MATOP_P TRUE             )
  set(HAVE_SCAMAC_MATOP_P TRUE PARENT_SCOPE)
endif()


configure_file(public_headers/scamacvecmat.h.in public_headers/scamacvecmat.h)
target_headers(scamacvecmat PUBLIC ${CMAKE_CURRENT_BINARY_DIR}/public_headers/scamacvecmat.h)


INSTALL(TARGETS scamacvecmat
        ARCHIVE DESTINATION ${SCAMAC_LIBDIR}
        LIBRARY DESTINATION ${SCAMAC_LIBDIR}
        PUBLIC_HEADER DESTINATION ${SCAMAC_INCDIR}
)


include(ConfigurePkgCfg)
PkgCfg_SetVar("scamacvecmat")

configure_file(scamacvecmat.pc.in scamacvecmat.pc @ONLY)
INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/scamacvecmat.pc DESTINATION ${SCAMAC_LIBDIR}/pkgconfig)

if (BUILD_TESTING)
  add_subdirectory(tests)
endif()


