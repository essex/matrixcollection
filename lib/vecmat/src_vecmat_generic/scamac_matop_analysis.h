/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Sparse matrix analysis through linear algebra algorithms
 *  \ingroup vecmat
 */

#ifndef SCAMAC_MATOP_ANALYSIS_H
#define SCAMAC_MATOP_ANALYSIS_H

#include <stdbool.h>

#include "scamac_error.h"
#include "scamac_matop.h"

/** \brief analyse symmetry (hermiticity) of matrix operator
 *  \ingroup vecmat
 *  \param[in] sm matrix operator to be checked. Must be square operator.
 *  \param[out] dev_symm Deviation from symmetry
 *  \note allocates two vectors of size
 *  \return Error code
 */
ScamacErrorCode scamac_matop_analyse_symmetry(const ScamacMatop * op, double *dev_symm);

ScamacErrorCode scamac_matop_analyse_transpose(const ScamacMatop * opa, const ScamacMatop * opb, double *dev_transpose);

ScamacErrorCode scamac_matop_analyse_inverse(const ScamacMatop * opa, const ScamacMatop * opb, double *dev_inverse);

ScamacErrorCode scamac_matop_analyse_commutator(const ScamacMatop * opa, const ScamacMatop * opb, double *dev_comm);

ScamacErrorCode scamac_matop_analyse_norm(const ScamacMatop * opa, double *nrm);


#endif /* SCAMAC_MATOP_ANALYSIS_H */
