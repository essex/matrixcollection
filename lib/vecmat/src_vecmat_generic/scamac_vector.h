/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   May 2018 --- today
 *  \brief  Vector creation and manipulation
 *  \ingroup vecmat
 */

#ifndef SCAMAC_VECTOR_H
#define SCAMAC_VECTOR_H

#include "scamac_inttypes.h"
#include "scamac_defs.h"
#include "scamac_error.h"
#include <stdbool.h>
#include <complex.h>

/* abstract objects */
typedef struct scamac_vector_st  ScamacVector;
typedef struct scamac_vectormem_st  ScamacVectorMem;

/**
 */
ScamacErrorCode scamac_vecmat_init();
/**
 */
ScamacErrorCode scamac_vecmat_finalize();

/** \brief Allocate vector(s)
 *  \ingroup vecmat
 *   equivalent to calling scamac_vector_multiple_alloc with m=1
 *  \note: throws an error if *v != NULL on invocation
 */ 
ScamacErrorCode scamac_vectormem_alloc(ScamacInt nv, int m, ScamacValType valtype, ScamacVectorMem ** v);
/**
 *  \note: does nothing if v==NULL
 */
ScamacErrorCode scamac_vectormem_free(ScamacVectorMem * v);

/**
 *   construct the "identity view" of vm
 *   on first call with v, must have v==NULL
 */
ScamacErrorCode scamac_vector_view       (ScamacVectorMem * vm, ScamacVector ** v);
/**
 *   construct a "single vector view" of vm
 */
ScamacErrorCode scamac_vector_view_single(ScamacVectorMem * vm, int i, ScamacVector ** v);
ScamacErrorCode scamac_vector_view_range (ScamacVectorMem * vm, int ia, int ib, ScamacVector ** v);
ScamacErrorCode scamac_vector_view_mask  (ScamacVectorMem * vm, const bool *mask, ScamacVector ** v);

/**
 *  \note: does nothing if v==NULL
 */
ScamacErrorCode scamac_vector_free(ScamacVector * v);

    ScamacInt scamac_vector_query_nv     (const ScamacVector * v);
          int scamac_vector_query_m      (const ScamacVector * v);
// int scamac_vector_query_max_m  (const ScamacVector * v);
ScamacValType scamac_vector_query_valtype(const ScamacVector * v);


/**
 *   get a (view of) a subsets of vectors
 *  \note: if *w == NULL, the ScamacVector object is allocated first.
 *         if *w != NULL, it is overwritten.
 *         No actual copy of vectors is produced .
 *   /// This is incorrect [ It is legitimate that v == w (in which case, v is overwritten).  ]
 */
ScamacErrorCode scamac_vector_sub_single (const ScamacVector * v, int i, ScamacVector ** w);
ScamacErrorCode scamac_vector_sub_range  (const ScamacVector * v, int ia, int ib, ScamacVector ** w);
ScamacErrorCode scamac_vector_sub_mask   (const ScamacVector * v, const bool *mask, ScamacVector ** w);
/**
 *   set w to vectors not contained in v
 */
ScamacErrorCode scamac_vector_complement(const ScamacVector * v, ScamacVector ** w);


/**
 * move vectors in v to the front of vecmem - e.g. to guarantee consecutive arrangement in memory, or just to simplify bookkeeping
 */
ScamacErrorCode scamac_vector_align(ScamacVector * v);
ScamacErrorCode scamac_vector_align_front(ScamacVector * v);



/* +++++ +++++ +++++ operations +++++ +++++ +++++ */

ScamacErrorCode scamac_vector_zero (ScamacVector * v);
//ScamacErrorCode scamac_vector_random(int seed, ScamacVector * v);
ScamacErrorCode scamac_vector_random(const char * seed, bool use_entropy, bool normalize, ScamacVector * v);

/**
 *   for testing purposes: assign specify types of vectors
 *   which = "Hilbert", "Nearly_Dependent", "Unit"
 */
ScamacErrorCode scamac_vector_assign(const char *which, ScamacVector * v);

/**
 *  get an actual copy of vectors
 *  v, w must be exclusive [scamac_vector_check_exclusivity(v,w) == TRUE]
 */
ScamacErrorCode scamac_vector_copy (const ScamacVector * v, ScamacVector * w);


/**
 *  vector-by-vector dot product
 *  length v1 = length v2, length dot >= length v{1,2}
 */
ScamacErrorCode scamac_vector_dot (const ScamacVector * v1, const ScamacVector * v2, double * dot);
ScamacErrorCode scamac_vector_zdot(const ScamacVector * v1, const ScamacVector * v2, double complex * zdot);

/**
 *  vector norm
 */
ScamacErrorCode scamac_vector_nrm2 (const ScamacVector * v, double * nrm);

/**
 *  matrix of scalar products V1^+ V2
 *  length dotm >= length v1 * length v2
 *  on exit, dotm[i + j * (length v1) ]  = < v1[i], v2[j] >
 *           for 0<=i < length v1, 0<=j < length v2
 */
ScamacErrorCode scamac_vector_dotmatrix (const ScamacVector * v1, const ScamacVector * v2, double * dotm);
ScamacErrorCode scamac_vector_zdotmatrix(const ScamacVector * v1, const ScamacVector * v2, double complex * zdotm);

/**
 *  matrix of scalar products V^+ W
 *  if V==W, this is the Gram matrix
 *  length gram >= length v ** 2
 *  on exit, gram[i + j * (length v) ]  = < v[i], v[j] >
 *           for 0<= i,j < length v
 *
 *  remark: identical to dotmatrix with v1=v2=v,
 *          but computes less dot products
 */
ScamacErrorCode scamac_vector_gram (const ScamacVector * v, double * gram);
ScamacErrorCode scamac_vector_zgram(const ScamacVector * v, double complex * zgram);


/**
 *   y -> a*x + b*y
 */
ScamacErrorCode scamac_mvector_axpby (const ScamacVector * x, ScamacVector * y, const double *a, const double *b);
ScamacErrorCode scamac_mvector_zaxpby(const ScamacVector * x, ScamacVector * y, const double complex *a, const double complex *b);
ScamacErrorCode scamac_svector_axpby (const ScamacVector * x, ScamacVector * y, double a, double b);
ScamacErrorCode scamac_svector_zaxpby(const ScamacVector * x, ScamacVector * y, double complex a, double complex b);

/**
 *   x[i] -> s[i]*x[i] for all vectors i
 */
ScamacErrorCode scamac_mvector_scal (ScamacVector * x, const double * s);
ScamacErrorCode scamac_mvector_zscal(ScamacVector * x, const double complex * s);
ScamacErrorCode scamac_svector_scal (ScamacVector * x, double s);
ScamacErrorCode scamac_svector_zscal(ScamacVector * x, double complex s);

/**
 *  Y = X A, that is y_i = sum_j a_ji x_j
 *  requires matrix A = m x n, where
 *                  m: number of vectors in X
 *                  n: number of vectors in Y 
 */
ScamacErrorCode scamac_vector_transform (const ScamacVector * x, const double * a, ScamacVector * y);
ScamacErrorCode scamac_vector_ztransform(const ScamacVector * x, const double complex * a, ScamacVector * y);


/** 
 *  orthogonalize vectors in w against vectors in v
 *
 *  vectors in v must be orthogonal + normalized
 *
 *  if v = (v1, ..., vm) and w = (w1, ..., wn):
 *  on return, vectors in w are normalized, and the full set (v1, ..., vm, w1, ..., wn) is orthogonal
 * 
 * in this basis, the original input vectors in w have the expansion
 * W = (V W_out) R P
 * with the upper trapezoidal (m+n) x n matrix R and the n x n permutation matrix P.
 *
 * [up to a permutation, R is upper triangular. The permutation is returned in P]
 */
ScamacErrorCode scamac_vector_orthogonalize (const ScamacVector * v, ScamacVector * w, double * r, int * p);
ScamacErrorCode scamac_vector_zorthogonalize(const ScamacVector * v, ScamacVector * w, double complex * r, int * p);


ScamacErrorCode scamac_vector_check_orthogonality(const ScamacVector * v, double * err, double * errnrm, double * errperp);


ScamacErrorCode scamac_vector_print(const ScamacVector * v);

/* internal use, mainly */


bool scamac_vectormem_check(const ScamacVectorMem * vm);
bool scamac_vector_check(const ScamacVector * v);
bool scamac_vector_check_compatibility(const ScamacVector * v1, const ScamacVector * v2);
bool scamac_vector_check_compatible_columns(const ScamacVector * v1, const ScamacVector * v2);
bool scamac_vector_check_exclusivity  (const ScamacVector * v1, const ScamacVector * v2);
bool scamac_vector_check_aligned  (const ScamacVector * v);
void * scamac_vector_getptr(const ScamacVector * v, ScamacInt i);


/* +++ +++ +++  +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ */
/*     obsolete                                                             */
/* +++ +++ +++  +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ */

/**
 *  y = a_1 x_1 + ... a_n x_n
 */
/* ScamacErrorCode scamac_vector_lincomb (const ScamacVector * x, const double * a, ScamacVector * y); */
/* ScamacErrorCode scamac_vector_zlincomb(const ScamacVector * x, const double complex * a, ScamacVector * y); */

/**
 *   v -> v_p  such that  v = sum_i c[i] vm[i] + d v
 *   On input, len(c) >= size (vm)
 *   Orthogonalization is performed with a Gram-Schmidt step.
 *   \note: vectors vm must be orthogonal+normalized on input. Otherwise, use scamac_vector_orthogonalize
 *   The valtype of c,d has to conform to the valtype of vm,v.
 */
/* ScamacErrorCode scamac_vector_project (const ScamacVector * vm, ScamacVector * v, double * c, double * d); */
/* ScamacErrorCode scamac_vector_zproject(const ScamacVector * vm, ScamacVector * v, double complex * c, double complex * d); */

 
#endif /* SCAMAC_VECTOR_H */
