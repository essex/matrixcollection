#include "scamac_matop_analysis.h"
#include "scamac_vector.h"

#include "math.h"
#include "complex.h"

ScamacErrorCode scamac_matop_analyse_symmetry(const ScamacMatop * op, double *dev_symm) {
  if (!op) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!dev_symm) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_matop_query_nr(op) != scamac_matop_query_nc(op)) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if ((scamac_matop_query_valtype(op) != SCAMAC_VAL_REAL) && (scamac_matop_query_valtype(op) != SCAMAC_VAL_COMPLEX)) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  ScamacErrorCode err;
  
  ScamacVectorMem *vmem=NULL;
  ScamacVector *x=NULL, *y=NULL, *xsm=NULL, *ysm=NULL;

  err = scamac_vectormem_alloc(scamac_matop_query_nc(op), 4, scamac_matop_query_valtype(op), &vmem);  SCAMAC_GOTOERR(err,cleanup);
  err = scamac_vector_view_single(vmem,0,&x);                  SCAMAC_GOTOERR(err,cleanup);
  err = scamac_vector_view_single(vmem,1,&y);                  SCAMAC_GOTOERR(err,cleanup);
  err = scamac_vector_view_single(vmem,2,&xsm);                SCAMAC_GOTOERR(err,cleanup);
  err = scamac_vector_view_single(vmem,3,&ysm);                SCAMAC_GOTOERR(err,cleanup);
  
  double dev = 0.0;
  static const int how_many = 4;
  int i;
  for (i=0;i<how_many;i++) {
    err=scamac_vector_random("zufall", true, true, x);  SCAMAC_GOTOERR(err,cleanup);
    err=scamac_vector_random("zufaelliger", true, true, y);  SCAMAC_GOTOERR(err,cleanup);
    if (scamac_matop_query_valtype(op) == SCAMAC_VAL_REAL) {
      err=scamac_matop_mvm(op, x,xsm, 1.0,0.0,0.0); SCAMAC_GOTOERR(err,cleanup);
      err=scamac_matop_mvm(op, y,ysm, 1.0,0.0,0.0); SCAMAC_GOTOERR(err,cleanup);
      double dot1, dot2;
      err=scamac_vector_dot (xsm, y, &dot1); SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_dot (x, ysm, &dot2); SCAMAC_GOTOERR(err,cleanup);
      dev=fmax(dev,fabs(dot1-dot2));
    } else {
      err=scamac_matop_mvm_cplx(op, x,xsm, 1.0,0.0,0.0); SCAMAC_GOTOERR(err,cleanup);
      err=scamac_matop_mvm_cplx(op, y,ysm, 1.0,0.0,0.0); SCAMAC_GOTOERR(err,cleanup);
      double complex dot1, dot2;
      err=scamac_vector_zdot (xsm, y, &dot1); SCAMAC_GOTOERR(err,cleanup);
      err=scamac_vector_zdot (x, ysm, &dot2); SCAMAC_GOTOERR(err,cleanup);
      dev=fmax(dev,cabs(dot1-dot2));
    }
  }

  *dev_symm=dev;
  
 cleanup:
  scamac_vector_free(x);
  scamac_vector_free(y);
  scamac_vector_free(xsm);
  scamac_vector_free(ysm);
  scamac_vectormem_free(vmem);
  
  return SCAMAC_EOK;
}

