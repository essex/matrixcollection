/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Sparse matrix creation and manipulation
 *  \ingroup vecmat
 */

#ifndef SCAMAC_MATOP_H
#define SCAMAC_MATOP_H


#include "scamac_vector.h"
#include "scamac_generator.h"
#include "scamac_sparsemat.h"
#include <complex.h>

/* abstract object */
typedef struct scamac_matop_st  ScamacMatop;

/** \brief Create matop
 *  \ingroup vecmat
 */
//ScamacErrorCode scamac_matop_alloc(ScamacInt nr, ScamacInt nc, ScamacInt ne, ScamacValType valtype, ScamacMatop ** sm);

/** \brief Obtain matop from ScaMaC generator
 *  \ingroup vecmat
 */
ScamacErrorCode scamac_matop_from_generator(const ScamacGenerator * gen, ScamacMatop ** sm);

/** \brief Obtain matop from ScaMaC sparse matrix
 *  \ingroup vecmat
 */
ScamacErrorCode scamac_matop_from_matrix(const ScamacMatrix * mat, ScamacMatop ** sm);

/** \brief Free allocated matop
 *  \ingroup vecmat
 */
ScamacErrorCode scamac_matop_free(ScamacMatop * sm);

ScamacInt scamac_matop_query_nr(const ScamacMatop * sm);
ScamacInt scamac_matop_query_nc(const ScamacMatop * sm);
ScamacValType scamac_matop_query_valtype(const ScamacMatop * sm);

/** \brief Sparse matrix-vector multiplication: y = alpha SM x + beta y + gamma x
 *  \ingroup vecmat
 *  \note There is little information in declaring *x "const" - it should simply be understood as a reminder that this vector view is not changed
 */
//ScamacErrorCode scamac_sparsemat_mvm(const ScamacMatrix *sm, const double *x, double *y, double alpha, double beta, double gamma);
ScamacErrorCode scamac_matop_mvm(const ScamacMatop *sm, const ScamacVector *x, ScamacVector *y, double alpha, double beta, double gamma);

/** \brief Complex sparse matrix-vector multiplication: y = alpha SM x + beta y + gamma x
 *  \ingroup vecmat
 *  \note Requires complex vectors x,y, but accepts real and complex matrix sm
 */
//ScamacErrorCode scamac_sparsemat_mvm_cplx(const ScamacMatrix *sm, const double complex *x, double complex *y,
//    double complex alpha, double complex beta, double complex gamma);
ScamacErrorCode scamac_matop_mvm_cplx(const ScamacMatop *sm, const ScamacVector *x, ScamacVector *y,
    double complex alpha, double complex beta, double complex gamma);

#endif /* SCAMAC_MATOP_H */
