/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  ...
 *  \ingroup internal
 */

#ifndef SCAMAC_CBLAS_WRAPPER_H
#define SCAMAC_CBLAS_WRAPPER_H

#include <complex.h>

/* basic C interface for the few level 1 BLAS routines needed here */

/* COPYRIGHT: CBLAS .... */

#define BLAS_INT int

double cblas_ddot(const BLAS_INT N, const double *X, const BLAS_INT incX,
                  const double *Y, const BLAS_INT incY);
double cblas_dnrm2(const BLAS_INT N, const double *X, const BLAS_INT incX);

void cblas_daxpy(const BLAS_INT N, const double alpha, const double *X,
                 const BLAS_INT incX, double *Y, const BLAS_INT incY);
void cblas_dscal(const BLAS_INT N, const double alpha, double *X, const BLAS_INT incX);
void cblas_dcopy(const BLAS_INT N, const double *X, const BLAS_INT incX,
                 double *Y, const BLAS_INT incY);

void cblas_zdotc_sub(const BLAS_INT N, const double complex *X, const BLAS_INT incX,
                     const double complex *Y, const BLAS_INT incY, double complex *dotc);
double cblas_dznrm2(const BLAS_INT N, const double complex *X, const BLAS_INT incX);

void cblas_zaxpy(const BLAS_INT N, const double complex *alpha, const double complex *X,
                 const BLAS_INT incX, double complex *Y, const BLAS_INT incY);
void cblas_zdscal(const BLAS_INT N, const double alpha, double complex *X, const BLAS_INT incX);
void cblas_zcopy(const BLAS_INT N, const double complex *X, const BLAS_INT incX,
                 double complex *Y, const BLAS_INT incY);

#endif /* SCAMAC_CBLAS_WRAPPER_H */
