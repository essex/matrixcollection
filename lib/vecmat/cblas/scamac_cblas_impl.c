#include "scamac_cblas.h"

double cblas_ddot(const int N, const double *X, const int incX, const double *Y, const int incY) {
  double dot;
  int i, ix, iy;
  dot=0.0;
  ix=0;
  iy=0;
  for (i=0;i<N;i++) {
    dot += x[ix] * y[iy];
    ix += incX;
    iy += incY;
  }
  return dot;
}


double cblas_dnrm2(const int N, const double *X, const int incX) {
  BLAS_INT my_N=N, my_incX=incX;
  double nrm2 = dnrm2_(&my_N, X, &my_incX);
  return nrm2;
}

void cblas_daxpy(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY) {
  BLAS_INT my_N=N, my_incX=incX, my_incY=incY;
  daxpy_(&my_N, &alpha, X, &my_incX, Y, &my_incY);
}

void cblas_dscal(const int N, const double alpha, double *X, const int incX) {
  BLAS_INT my_N=N, my_incX=incX;
  dscal_(&my_N, &alpha, X, &my_incX);
}

void cblas_dcopy(const int N, const double *X, const int incX,
                 double *Y, const int incY) {
  BLAS_INT my_N=N, my_incX=incX, my_incY=incY;
  dcopy_(&my_N, X, &my_incX, Y, &my_incY);
}

void cblas_zdotc_sub(const int N, const double complex *X, const int incX,
                     const double complex *Y, const int incY, double complex *dotc) {
  BLAS_INT my_N=N, my_incX=incX, my_incY=incY;
  zdotc_(dotc, &my_N, X, &my_incX, Y, &my_incY);
}

double cblas_dznrm2(const int N, const double complex *X, const int incX) {
  BLAS_INT my_N=N, my_incX=incX;
  double nrm2 = dznrm2_(&my_N, X, &my_incX);
  return nrm2;
}

void cblas_zaxpy(const int N, const double complex *alpha, const double complex *X,
                 const int incX, double complex *Y, const int incY) {
  BLAS_INT my_N=N, my_incX=incX, my_incY=incY;
  zaxpy_(&my_N, alpha, X, &my_incX, Y, &my_incY);
}


void cblas_zdscal(const int N, const double alpha, double complex *X, const int incX) {
  BLAS_INT my_N=N, my_incX=incX;
  zdscal_(&my_N, &alpha, X, &my_incX);
}

void cblas_zcopy(const int N, const double complex *X, const int incX,
                 double complex *Y, const int incY) {
  BLAS_INT my_N=N, my_incX=incX, my_incY=incY;
  zcopy_(&my_N, X, &my_incX, Y, &my_incY);
}


