#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <limits.h>

#include "scamac_sparsemat_internal.h"
#include "scamac_sparsemat_io.h"


ScamacErrorCode scamac_sparsemat_io_write_mm(const ScamacMatrix *sm, char * fname) {
  if (!sm) {
    return SCAMAC_ENULL | 1 << SCAMAC_ESHIFT;
  }
  if (!fname) {
    return SCAMAC_ENULL | 2 << SCAMAC_ESHIFT;
  }

  if (!sm->nr || !sm->nc || !sm->ne) {
    return SCAMAC_EINVALID | 1 << SCAMAC_ESHIFT;
  }

  FILE * f;
  f=fopen(fname,"w");

  if (sm->valtype == SCAMAC_VAL_REAL) {
    fprintf(f, "%%%%MatrixMarket matrix coordinate real general\n");
  } else {
    fprintf(f, "%%%%MatrixMarket matrix coordinate complex general\n");
  }

  fprintf(f, "%"SCAMACPRINT" %"SCAMACPRINT" %"SCAMACPRINT" \n",sm->nr,sm->nc,sm->ne);

  ScamacInt i,j;
  if (sm->valtype == SCAMAC_VAL_REAL) {
    for (i=0; i<sm->nr; i++) {
      for (j=sm->rptr[i]; j<sm->rptr[i+1]; j++) {
        fprintf(f, "%"SCAMACPRINT" %"SCAMACPRINT" %20.16g\n", i+1, sm->cind[j]+1, sm->val[j]);
      }
    }
  } else {
    for (i=0; i<sm->nr; i++) {
      for (j=sm->rptr[i]; j<sm->rptr[i+1]; j++) {
        fprintf(f, "%"SCAMACPRINT" %"SCAMACPRINT" %20.16g %20.16g\n", i+1, sm->cind[j]+1, sm->val[2*j], sm->val[2*j+1]);
      }
    }
  }

  fclose(f);

  return SCAMAC_EOK;

}

ScamacErrorCode scamac_sparsemat_io_write_hb(const ScamacMatrix *sm, char * fname) {
  if (!sm) {
    return SCAMAC_ENULL | 1 << SCAMAC_ESHIFT;
  }
  if (!fname) {
    return SCAMAC_ENULL | 2 << SCAMAC_ESHIFT;
  }

  if (!sm->nr || !sm->nc || !sm->ne) {
    return SCAMAC_EINVALID | 1 << SCAMAC_ESHIFT;
  }
    
  char TITLE[72+1], KEY[8+1];
  ScamacInt TOTCRD, PTRCRD, INDCRD, VALCRD, RHSCRD;
  char TOTCRDs[14+1], PTRCRDs[14+1], INDCRDs[14+1], VALCRDs[14+1], RHSCRDs[14+1];
  char MXTYPE[3];
  ScamacInt NROW, NCOL, NNZERO, NELTVL;
  char NROWs[14+1], NCOLs[14+1], NNZEROs[14+1], NELTVLs[14+1];
  char PTRFMT[16+1], INDFMT[16+1], VALFMT[20+1], RHSFMT[20+1];

  snprintf(TITLE,72,"[SCAMAC] NO TITLE");
  snprintf(KEY,8,"NO KEY");

  if (sm->valtype == SCAMAC_VAL_REAL) {
    MXTYPE[0]='R';
  } else {
    MXTYPE[0]='C';
    return SCAMAC_ESCOPE | 1 << SCAMAC_ESHIFT;
  }
  MXTYPE[1]='U';
  MXTYPE[2]='A';

  PTRCRD=((sm->nr-1)/8)+1;
  INDCRD=((sm->ne-1)/8)+1;
  VALCRD=((sm->ne-1)/3)+1;
  RHSCRD=0;
  TOTCRD=PTRCRD+INDCRD+VALCRD+RHSCRD;

  NROW=sm->nr;
  NCOL=sm->nc;
  NNZERO=sm->ne;
  NELTVL=0;

  snprintf(TOTCRDs,14+1,"%"SCAMACPRINT,TOTCRD);
  snprintf(PTRCRDs,14+1,"%"SCAMACPRINT,PTRCRD);
  snprintf(INDCRDs,14+1,"%"SCAMACPRINT,INDCRD);
  snprintf(VALCRDs,14+1,"%"SCAMACPRINT,VALCRD);
  snprintf(RHSCRDs,14+1,"%"SCAMACPRINT,RHSCRD);

  snprintf(NROWs,14+1,"%"SCAMACPRINT,NROW);
  snprintf(NCOLs,14+1,"%"SCAMACPRINT,NCOL);
  snprintf(NNZEROs,14+1,"%"SCAMACPRINT,NNZERO);
  snprintf(NELTVLs,14+1,"%"SCAMACPRINT,NELTVL);

  snprintf(PTRFMT,16+1,"(8I10)");
  snprintf(INDFMT,16+1,"(8I10)");
  snprintf(VALFMT,20+1,"(3E25.17)");
  snprintf(RHSFMT,20+1,"(3E25.17)");

  FILE * f;
  f=fopen(fname,"w");

  fprintf(f,"%-72.72s%-8.8s\n",TITLE,KEY);
  fprintf(f,"%-14.14s%-14.14s%-14.14s%-14.14s%-14.14s\n",TOTCRDs,PTRCRDs,INDCRDs,VALCRDs,RHSCRDs);
  fprintf(f,"%-3.3s           %-14.14s%-14.14s%-14.14s%-14.14s\n",MXTYPE,NROWs,NCOLs,NNZEROs,NELTVLs);
  fprintf(f,"%-16.16s%-16.16s%-20.20s%-20.20s\n",PTRFMT,INDFMT,VALFMT,RHSFMT);


  ScamacInt i,j;
  char OLINE[80+1];
  j=0;
  for (i=0; i<=sm->nr-8; i+=8) {
    snprintf(OLINE,80+1,"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT,
             sm->rptr[i]+1, sm->rptr[i+1]+1, sm->rptr[i+2]+1, sm->rptr[i+3]+1, sm->rptr[i+4]+1, sm->rptr[i+5]+1, sm->rptr[i+6]+1, sm->rptr[i+7]+1 );
    fprintf(f,"%s\n",OLINE);
    j+=8;
  }
  if (j<=sm->nr) {
    snprintf(OLINE,80+1,"%80.80s"," ");
    for (i=j; i<=sm->nr; i++) {
      snprintf(&OLINE[10*(i-j)],10+1,"%10"SCAMACPRINT, sm->rptr[i]+1);
    }
    fprintf(f,"%s\n",OLINE);
  }
  j=0;
  for (i=0; i<sm->ne-8; i+=8) {
    snprintf(OLINE,80+1,"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT"%10"SCAMACPRINT,
             sm->cind[i]+1, sm->cind[i+1]+1, sm->cind[i+2]+1, sm->cind[i+3]+1, sm->cind[i+4]+1, sm->cind[i+5]+1, sm->cind[i+6]+1, sm->cind[i+7]+1 );
    fprintf(f,"%s\n",OLINE);
    j+=8;
  }
  if (j<sm->ne) {
    snprintf(OLINE,80+1,"%80.80s"," ");
    for (i=j; i<sm->ne; i++) {
      snprintf(&OLINE[10*(i-j)],10+1,"%10"SCAMACPRINT, sm->cind[i]+1);
    }
    fprintf(f,"%s\n",OLINE);
  }
  j=0;
  for (i=0; i<sm->ne-3; i+=3) {
    snprintf(OLINE,80+1,"%25.17e%25.17e%25.17e",
             sm->val[i], sm->val[i+1], sm->val[i+2] );
    fprintf(f,"%s\n",OLINE);
    j+=3;
  }
  if (j<sm->ne) {
    snprintf(OLINE,80+1,"%80.80s"," ");
    for (i=j; i<sm->ne; i++) {
      snprintf(&OLINE[25*(i-j)],25+1,"%25.17e", sm->val[i]);
    }
    fprintf(f,"%s\n",OLINE);
  }

  fclose(f);

  return SCAMAC_EOK;

}

ScamacErrorCode scamac_sparsemat_io_write_matlab(const ScamacMatrix *sm, char * fname) {
  assert(CHAR_BIT == 8);
  assert(sizeof *(sm->rptr) == 4);
  assert(sizeof *(sm->cind) == 4);
  assert(sizeof *(sm->val ) == 8);

   if (!sm) {
    return SCAMAC_ENULL | 1 << SCAMAC_ESHIFT;
  }
  if (!fname) {
    return SCAMAC_ENULL | 2 << SCAMAC_ESHIFT;
  }

  if (!sm->nr || !sm->nc || !sm->ne) {
    return SCAMAC_EINVALID | 1 << SCAMAC_ESHIFT;
  }
  
  FILE * f;
  f=fopen(fname,"wb");

  char buf[256];

  snprintf(buf,128,"%-127.127s","MATLAB 5.0 MAT-file");


  fwrite(buf, sizeof(char), 124, f);

  int16_t kj[2];
  kj[0]=256;
  kj[1]=19785;
  fwrite(kj, sizeof(int16_t), 2, f);


  int32_t li[4];


  int ntotbyte;
  ntotbyte = 16 + 16 + 8 + 3*8;
  ntotbyte += 4 * sm->ne + 4 * (sm->nc+1) + 8 * sm->ne;
  if (sm->ne % 2) {
    ntotbyte +=4;
  }
  if ((sm->nc+1) % 2) {
    ntotbyte +=4;
  }


  li[0]=14;
  li[1]=ntotbyte;
  fwrite(li, sizeof(*li), 2, f);

  li[0]=6;
  li[1]=8;
  li[2]=5 + 256*0;
  li[3]=sm->ne   ;
  fwrite(li, sizeof(*li), 4, f);

  li[0]=5;
  li[1]=8;
  li[2]=sm->nr;
  li[3]=sm->nc;
  fwrite(li, sizeof(*li), 4, f);

  buf[0]=1;
  buf[1]=0;
  buf[2]=4;
  buf[3]=0;
  snprintf(&buf[4],5,"%-4.4s","SMAT");
  fwrite(buf, sizeof(*buf), 8, f);

  li[0]=5;
  if (sm->ne % 2) {
    li[1] = 4 * (sm->ne + 1);
    fwrite(li, sizeof(int32_t), 2, f);
    fwrite(sm->cind, sizeof(int32_t), sm->ne-1, f);
    li[0]=sm->cind[sm->ne-1];
    li[1]=0;
    fwrite(li, sizeof(int32_t), 2, f);
  } else {
    li[1] = 4 * sm->ne;
    fwrite(li, sizeof(int32_t), 2, f);
    fwrite(sm->cind, sizeof(int32_t), sm->ne, f);
  }

  li[0]=5;
  if ((sm->nc+1) % 2) {
    li[1] = 4 * (sm->nc + 2);
    fwrite(li, sizeof(int32_t), 2, f);
    fwrite(sm->rptr, sizeof(int32_t), sm->nc, f);
    li[0]=sm->rptr[sm->nc];
    li[1]=0;
    fwrite(li, sizeof(int32_t), 2, f);
  } else {
    li[1] = 4 * (sm->nc + 1);
    fwrite(li, sizeof(int32_t), 2, f);
    fwrite(sm->rptr, sizeof(int32_t), sm->nc+1, f);
  }

  li[0]=9;
  li[1]=8 * sm->ne;
  fwrite(li, sizeof(*li), 2, f);
  fwrite(sm->val, sizeof(double), sm->ne, f);

  fclose(f);

  return SCAMAC_EOK;

}

ScamacErrorCode scamac_sparsemat_io_write_ghost(const ScamacMatrix *sm, char * fname) {
  if (!sm) {
    return SCAMAC_ENULL | 1 << SCAMAC_ESHIFT;
  }
  if (!fname) {
    return SCAMAC_ENULL | 2 << SCAMAC_ESHIFT;
  }

  if (!sm->nr || !sm->nc || !sm->ne) {
    return SCAMAC_EINVALID | 1 << SCAMAC_ESHIFT;
  }

  if ( sizeof *(sm->rptr) != 8 || sizeof *(sm->cind) != 8 || sizeof *(sm->val) !=8) {
    return SCAMAC_EINVALID | 1 << SCAMAC_ESHIFT;
  }

  FILE * f;
  f=fopen(fname,"wb");


  int16_t shint[5];
  shint[0]=0;
  shint[1]=1;
  shint[2]=0;
  shint[3]=1;
  shint[4]=6;
  fwrite(shint, sizeof (int16_t), 5, f);

  int64_t llint[3];
  llint[0]=sm->nr;
  llint[1]=sm->nc;
  llint[2]=sm->ne;
  fwrite(llint, sizeof (int64_t), 3, f);


  fwrite(sm->rptr, sizeof *(sm->rptr), sm->nr+1, f);
  fwrite(sm->cind, sizeof *(sm->cind), sm->ne, f);
  fwrite(sm->val, sizeof *(sm->val), sm->ne, f);

  fclose(f);

  return SCAMAC_EOK;

}

