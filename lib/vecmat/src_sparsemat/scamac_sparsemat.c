#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "scamac_sparsemat.h"
#include "scamac_sparsemat_internal.h"
#include "scamac_densemat.h"

#include "scamac_internal.h"
#include "scamac_aux.h"
#include "scamac_safeint.h"
#include "scamac_generator.h"

ScamacErrorCode scamac_sparsemat_alloc(ScamacInt nr, ScamacInt nc, ScamacInt ne, ScamacValType valtype, ScamacMatrix ** sm) {
  if ((valtype != SCAMAC_VAL_REAL) && (valtype != SCAMAC_VAL_COMPLEX)) {
    return SCAMAC_EINVALID;
  }
  if (!sm) {
    return SCAMAC_ENULL;
  }
  if ((nr<1) || (nc<1) || (ne<0)) {
    return SCAMAC_ERANGE;
  }
  if (ne==0) {
    ne=1;
  }

  ScamacMatrix * my_sm = malloc(sizeof * my_sm);
  if (!my_sm) {
    return SCAMAC_EMALLOCFAIL;
  }
  my_sm->nr = nr;
  my_sm->nc = nc;
  my_sm->ne=0;
  my_sm->nemax = ne;
  my_sm->rptr = malloc((nr+1) * sizeof *(my_sm->rptr));
  if (!my_sm->rptr) {
    return SCAMAC_EMALLOCFAIL;
  }
  my_sm->cind = malloc( ne    * sizeof *(my_sm->cind));
  if (!my_sm->cind) {
    return SCAMAC_EMALLOCFAIL;
  }
  if (valtype == SCAMAC_VAL_REAL) {
    my_sm->val  = malloc(   ne  * sizeof *(my_sm->val) );
  } else if (valtype == SCAMAC_VAL_COMPLEX) {
    my_sm->val  = malloc( 2*ne  * sizeof *(my_sm->val) );
  } else {
    my_sm->val = NULL;
  }
  if (!my_sm->val) {
    return SCAMAC_EMALLOCFAIL;
  }
  my_sm->valtype = valtype;

  *sm = my_sm;
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_sparsemat_free(ScamacMatrix * sm) {
  if (sm) {
    free(sm->rptr);
    free(sm->cind);
    free(sm->val );
    free(sm);
  }
  return SCAMAC_EOK;
}


ScamacInt scamac_sparsemat_query_nr(const ScamacMatrix * sm)  {
  if (sm) {
    return sm->nr;
  } else {
    return 0;
  }
}

ScamacInt scamac_sparsemat_query_nc(const ScamacMatrix * sm) {
  if (sm) {
    return sm->nc;
  } else {
    return 0;
  }
}

ScamacValType scamac_sparsemat_query_valtype(const ScamacMatrix * sm) {
  if (sm) {
    return sm->valtype;
  } else {
    return SCAMAC_VAL_NONE;
  }
}


ScamacErrorCode scamac_sparsemat_from_generator(const ScamacGenerator * gen, ScamacMatrix ** sm) {
  if (!gen || !sm) {
    return SCAMAC_ENULL;
  }
  if (gen->needs_finalization) {
    return SCAMAC_EFAIL;
  }

  ScamacErrorCode err;
  ScamacWorkspace * my_ws;
  err = scamac_workspace_alloc(gen, &my_ws);
  if (err) {
    return err|SCAMAC_EINTERNAL;
  }

  ScamacIdx nrow = scamac_generator_query_nrow      (gen);
  ScamacIdx ncol = scamac_generator_query_ncol      (gen);
  ScamacIdx mrow = scamac_generator_query_maxnzrow  (gen);
  ScamacIdx valtype = scamac_generator_query_valtype(gen);
  if ( (nrow<1) || (ncol<1) || (mrow<0) ) {
    return SCAMAC_ERANGE|SCAMAC_EINTERNAL;
  }
  if ((valtype != SCAMAC_VAL_REAL) && (valtype != SCAMAC_VAL_COMPLEX)) {
    return SCAMAC_EINVALID|SCAMAC_EINTERNAL;
  }

  ScamacIdx *cind = malloc(mrow * sizeof *cind);
  if (!cind) {
    return SCAMAC_EMALLOCFAIL;
  }
  double *val = NULL;
  if (valtype == SCAMAC_VAL_REAL) {
    val = malloc(  mrow * sizeof *val);
  } else if (valtype == SCAMAC_VAL_COMPLEX) {
    val = malloc(2*mrow * sizeof *val);
  }
  if (!val) {
    return SCAMAC_EMALLOCFAIL;
  }

  ScamacIdx idx;

  ScamacIdx ne = 0;
  for (idx=0; idx<nrow; idx++) {
    ScamacIdx k;
    err = scamac_generate_row(gen, my_ws, idx, SCAMAC_DEFAULT, &k, cind, val);
    if (err) {
      return err|SCAMAC_EINTERNAL;
    }
    ne = scamac_safeidx_add(ne,k);
    if (ne<0) {
      return SCAMAC_EOVERFLOW;
    }
  }

  ScamacMatrix * my_sm;
  err = scamac_sparsemat_alloc(nrow,ncol,ne,valtype,&my_sm);
  if (err) {
    return err|SCAMAC_EINTERNAL;
  }

  ScamacIdx n = 0;
  my_sm->rptr[0]=0;
  for (idx=0; idx<nrow; idx++) {
    ScamacIdx k;
    err = scamac_generate_row(gen, my_ws, idx, SCAMAC_DEFAULT, &k, cind, val);
    if (err) {
      return err|SCAMAC_EINTERNAL;
    }
    int i;
    for (i=0; i<k; i++) {
      my_sm->cind[i+n]=cind[i];
    }
    if (valtype == SCAMAC_VAL_REAL) {
      memcpy(&(my_sm->val [n]), val, k * sizeof *val);
    } else if (valtype == SCAMAC_VAL_COMPLEX) {
      memcpy(&(my_sm->val [2*n]), val, 2*k * sizeof *val);
    }
    n=n+k;
    my_sm->rptr[idx+1]=n;
  }
  my_sm->ne=ne;

  free(cind);
  free(val);
  err = scamac_workspace_free(my_ws);
  if (err) {
    return err|SCAMAC_EINTERNAL;
  }

  *sm = my_sm;

  return SCAMAC_EOK;

}

bool scamac_sparsemat_check(const ScamacMatrix * sm) {
  if (!sm) { return false; }
  if (sm->nr<0) { return false; }
  if (sm->nc<0) { return false; }
  if (sm->ne<0) { return false; }
  if (sm->ne>sm->nemax) { return false; }
  if (!sm->rptr) { return false; }
  if (!sm->cind) { return false; }
  if (!sm->val) { return false; }
  if ((sm->valtype != SCAMAC_VAL_REAL) && (sm->valtype != SCAMAC_VAL_COMPLEX)) { return false; }

  if (sm->rptr[0] != 0) { return false; }
  if (sm->rptr[sm->nr] != sm->ne) { return false; }
     
  ScamacInt i,j;
  for (i=0;i<sm->nr;i++) {
    if (sm->rptr[i]<0) { return false; }
    if (sm->rptr[i] > sm->ne) { return false; }
    if (sm->rptr[i] > sm->rptr[i+1]) { return false; }
    for (j=sm->rptr[i];j<sm->rptr[i+1];j++) {
      if (sm->cind[j]<0) { return false; }
      if (sm->cind[j]>=sm->nc) { return false; }
      if (j>sm->rptr[i]) {
	if (sm->cind[j] <= sm->cind[j-1]) { return false; } 
      }
    }
  }

  for (i=0;i<sm->ne;i++) {
    if (!isfinite(sm->val[i])) { return false; }
  }

  return true;
}

ScamacErrorCode scamac_sparsemat_to_dense(const ScamacMatrix * sm, void ** dm) {
  if (!sm) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!dm) { return scamac_error_set_par(SCAMAC_ENULL,2); }
 
  ScamacInt nrow = scamac_sparsemat_query_nr(sm);
  ScamacInt ncol = scamac_sparsemat_query_nc(sm);
  ScamacValType valtype = scamac_sparsemat_query_valtype(sm);
  if (valtype == SCAMAC_VAL_NONE) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
 
  double *my_dm;
  if (*dm) {
    my_dm=*dm;
  } else {
    ScamacErrorCode err;
    err=scamac_densemat_alloc(nrow,ncol, valtype, (void **) &my_dm);
    if (err) { return scamac_error_set_internal(err); }
    *dm=my_dm;
  }

  ScamacInt i,j;
  if (valtype == SCAMAC_VAL_REAL) {
    for (i=0;i<nrow*ncol;i++) {
      my_dm[i] = 0.0;
    }
    for (i=0;i<nrow;i++) {
      for (j=sm->rptr[i];j<sm->rptr[i+1];j++) {
	my_dm[i+sm->cind[j]*nrow] = sm->val[j];
      }
    }
  } else {
    for (i=0;i<2*nrow*ncol;i++) {
      my_dm[i] = 0.0;
    }
    for (i=0;i<nrow;i++) {
      for (j=sm->rptr[i];j<sm->rptr[i+1];j++) {
	my_dm[2*(i+sm->cind[j]*nrow)  ] = sm->val[2*j];
	my_dm[2*(i+sm->cind[j]*nrow)+1] = sm->val[2*j+1];
      }
    }
  }
  
  return SCAMAC_EOK;
}
