/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Sparse matrix creation and manipulation
 *  \ingroup vecmat
 */

#ifndef SCAMAC_SPARSEMAT_H
#define SCAMAC_SPARSEMAT_H

#include "scamac_generator.h"
#include <stdbool.h>

/* abstract object */
typedef struct scamac_sparsemat_st ScamacMatrix;

/** \brief Allocate sparse matrix
 *  \ingroup vecmat
 */
ScamacErrorCode scamac_sparsemat_alloc(ScamacInt nr, ScamacInt nc, ScamacInt ne, ScamacValType valtype, ScamacMatrix ** sm);
/** \brief Free allocated sparse matrix
 *  \ingroup vecmat
 */
ScamacErrorCode scamac_sparsemat_free(ScamacMatrix * sm);

ScamacInt scamac_sparsemat_query_nr(const ScamacMatrix * sm);
ScamacInt scamac_sparsemat_query_nc(const ScamacMatrix * sm);
ScamacValType scamac_sparsemat_query_valtype(const ScamacMatrix * sm);
// int scamac_sparsemat_query_type(const ScamacMatrix * sm);  // TODO: value or pattern matrix
// int scamac_sparsemat_query_symmetry(const ScamacMatrix * sm); // General, Symmetric, ...


/** \brief Obtain sparse matrix from ScaMaC generator
 *  \ingroup vecmat
 */
ScamacErrorCode scamac_sparsemat_from_generator(const ScamacGenerator * gen, ScamacMatrix ** sm);

/** \brief basic sanity check for sparse matrix
 *  \ingroup vecmat
 *  \return Error code
 *  \note Does not complain about zero matrix entries.
 */
bool scamac_sparsemat_check(const ScamacMatrix * sm);

/** \brief convert sparse matrix to dense matrix
 *  \note If *dm = NULL, the dense matrix is allocated in the function (and should be free'd eventually).
 *        Otherwise, we expect that dm points to an nrow*ncol*(1 for real, 2 for complex) array of doubles.
 */
ScamacErrorCode scamac_sparsemat_to_dense(const ScamacMatrix * sm, void ** dm);

#endif /* SCAMAC_SPARSEMAT_H */
