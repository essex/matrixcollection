/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Internal type of sparse matrix
 *  \ingroup internal
 */

#ifndef SCAMAC_SPARSEMAT_INTERNAL_H
#define SCAMAC_SPARSEMAT_INTERNAL_H

#include "scamac_inttypes.h"
#include "scamac_defs.h"

struct scamac_sparsemat_st {
  /// number of rows
  ScamacInt nr;
  /// number of columns
  ScamacInt nc;
  /// number of non-zero (i.e. stored) matrix entries
  ScamacInt ne;
  /// maximal number of non-zeroes
  ScamacInt nemax;
  /// row pointers (dimension NR+1)
  ScamacInt *rptr;
  /// column indices (dimension NE)
  ScamacInt *cind;
  /// values (dimension NE)
  double *val;
  ///
  /// type of entries
  ScamacValType valtype; // SCAMAC_VAL_REAL or SCAMAC_VAL_COMPLEX
};

#endif /* SCAMAC_SPARSEMAT_INTERNAL_H */
