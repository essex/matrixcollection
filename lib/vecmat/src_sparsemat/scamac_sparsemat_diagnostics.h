/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Sparse matrix creation and manipulation
 *  \ingroup vecmat
 */

#ifndef SCAMAC_SPARSEMAT_DIAGNOSTICS_H
#define SCAMAC_SPARSEMAT_DIAGNOSTICS_H

#include <stdbool.h>

#include "scamac_error.h"
#include "scamac_sparsemat.h"

ScamacInt scamac_sparsemat_maxrowlength(const ScamacMatrix *sm);

/** \brief query symmetry (hermiticity) of matrix
 *  \ingroup vecmat
 *  \pre assumes \em ordered, i.e., \em monotonically increasing column indices. This is true for matrices obtained from the ScaMaC generators with scamac_sparsemat_from_generator()
 *  \param[in] sm sparse matrix to be checked
 *  \param[out] symm_pattern Is the sparsity pattern symmetric?
 *  \param[out] symm_value Is the matrix symmetric (for real sm) or Hermitian (for complex sm)?
 *  \note symm_pattern==false implies symm_value==false
 *  \return Error code
 */
ScamacErrorCode scamac_sparsemat_check_symmetry(const ScamacMatrix * sm, bool * symm_pattern, bool * symm_value);

#endif /* SCAMAC_SPARSEMAT_DIAGNOSTICS_H */
