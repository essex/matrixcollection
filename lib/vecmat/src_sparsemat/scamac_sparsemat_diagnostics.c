#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "scamac_sparsemat_diagnostics.h"
#include "scamac_sparsemat_internal.h"

#include "scamac_aux.h"

ScamacInt scamac_sparsemat_maxrowlength(const ScamacMatrix * sm) {
  if (!sm) {
    return 0;
  } else {
    ScamacInt mrwl = 0;
    ScamacInt i;
    for (i=0; i<sm->nr; i++) {
      mrwl = MAX(mrwl,sm->rptr[i+1]-sm->rptr[i]);
    }
    return mrwl;
  }
}


ScamacErrorCode scamac_sparsemat_check_symmetry(const ScamacMatrix * sm, bool * symm_pattern, bool * symm_value) {
  if (!sm) {
    return SCAMAC_ENULL;
  }
  if (sm->nr != sm->nc) {
    if (symm_pattern) {
      *symm_pattern=false;
    }
    if (symm_value) {
      *symm_value=false;
    }
    return SCAMAC_EINVALID;
  }

  if (!symm_pattern && !symm_value) {
    return SCAMAC_EOK;
  }

  bool my_s_pat = true;
  bool my_s_val = true;


  ScamacInt *ivec = malloc(sm->nr * sizeof *ivec);
  if (!ivec) {
    return SCAMAC_EMALLOCFAIL;
  }
  memcpy(ivec, sm->rptr, sm->nr * sizeof *ivec);

  ScamacInt i,j;
  for (i=0; i<sm->nr; i++) {
    for (j=sm->rptr[i]; j<sm->rptr[i+1]; j++) {
      ScamacInt k = sm->cind[j];
      if (ivec[k] >= sm->rptr[k+1]) {
        my_s_pat=false;
        my_s_val=false;
      } else if (sm->cind[ivec[k]] != i) {
        my_s_pat=false;
        my_s_val=false;
      } else {
        if (sm->valtype == SCAMAC_VAL_REAL) {
          if (sm->val[j] != sm->val[ivec[k]]) {
            my_s_val=false;
          }
        } else {
          if (sm->val[2*j] != sm->val[2*ivec[k]] || sm->val[2*j+1] != - sm->val[2*ivec[k]+1]) {
            my_s_val=false;
          }
        }
        ivec[k]++;
      }
    }
    if (!my_s_pat) {
      break;
    }
  }

  free(ivec);

  if (symm_pattern) {
    *symm_pattern=my_s_pat;
  }
  if (symm_value) {
    *symm_value=my_s_val;
  }

  return SCAMAC_EOK;
}
