/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Write sparse matrices to file
 *  \ingroup vecmat
 */

#ifndef SCAMAC_SPARSEMAT_IO_H
#define SCAMAC_SPARSEMAT_IO_H

#include "scamac_error.h"
#include "scamac_sparsemat.h"

/* output in Matrix Market format */
ScamacErrorCode scamac_sparsemat_io_write_mm(const ScamacMatrix *sm, char * fname);

/* output in Harwell-Boeing format */
ScamacErrorCode scamac_sparsemat_io_write_hb(const ScamacMatrix *sm, char * fname);

/* output in MATLAB binary format */
ScamacErrorCode scamac_sparsemat_io_write_matlab(const ScamacMatrix *sm, char * fname);

/* output in GHOST binary format */
ScamacErrorCode scamac_sparsemat_io_write_ghost(const ScamacMatrix *sm, char * fname);


#endif /* SCAMAC_SPARSEMAT_IO_H */
