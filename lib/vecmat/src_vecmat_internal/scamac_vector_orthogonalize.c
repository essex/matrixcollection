#include "scamac_vector.h"
#include "scamac_cblas.h"
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <complex.h>


#define lapack_int int


extern void dgeqp3_(lapack_int *m, lapack_int *n, double * a, lapack_int * lda,  lapack_int * jpvt, double * tau, double * work, lapack_int * lwork, lapack_int * info);

extern void dorgqr_(lapack_int *m, lapack_int *n, lapack_int *k, double * a, lapack_int * lda, double * tau, double * work, lapack_int * lwork, lapack_int * info);

extern void dgetri_(lapack_int *n, double *a, lapack_int *lda, lapack_int *ipiv, double *work, lapack_int *lwork, lapack_int *info);

static void construct_modified_LU_real(const double *v, int n, int m, double * a, double * s) {
  for (int i=0;i<m;i++) {
    for (int j=0;j<m;j++) {
      a[i+j*m]=v[i+j*n];
    }
  }
  
  for (int i=0;i<m;i++) {
    if (a[i*(m+1)] < 0.0) {
      s[i]=1.0;
    } else {
      s[i]=-1.0;
    }
    a[i*(m+1)]=a[i*(m+1)]-s[i];
    for (int j=i+1;j<m;j++) {
      a[j+i*m]=a[j+i*m]/a[i*(m+1)];
    }
    for (int j=i+1;j<m;j++) {
      for (int k=i+1;k<m;k++) {
	a[k+j*m]=a[k+j*m]-a[k+i*m]*a[i+j*m];
      }
    }
  }
}

static ScamacErrorCode invert_modified_LU_real(double * a, int m) {
  ScamacErrorCode err = SCAMAC_EOK;

  lapack_int n = m;
  double llwork;
  lapack_int lwork, info;

  

  lwork=-1;
  dgetri_(&n,a,&n,NULL,&llwork,&lwork,&info);
  lwork=llwork+0.1;

  double *work = NULL;
  lapack_int *ipiv = NULL;
  work=malloc(lwork* sizeof *work); if (!work) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  ipiv=malloc(n * sizeof *ipiv);    if (!ipiv) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  
  for (int i=0;i<n;i++) {
    ipiv[i]=i+1;
  }
  dgetri_(&n,a,&n,ipiv,work,&lwork,&info);
  if (info) {
    err=SCAMAC_EFAIL;
  }
  
 clean_up:
  free(work);
  free(ipiv);
  return err;
}

static ScamacErrorCode apply_modified_LU_real(const double *v, int n, int m, const double * a, const double * s, double *w, int k) {
  ScamacErrorCode err = SCAMAC_EOK;

  double *x = NULL, *y = NULL, *z = NULL;
  x = malloc(m*k * sizeof *x); if (!x) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  y = malloc(m*k * sizeof *y); if (!y) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  z = malloc(m*k * sizeof *z); if (!z) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
    
  cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, m,k,n, 1.0, v,n, w,n, 0.0, x, m);

  for (int i=0;i<m;i++) {
    for (int j=0;j<k;j++) {
      y[i+j*m]=s[i]*x[i+j*m]-w[i+j*n];
    }
  }

  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m,k,m, 1.0, a,m, y,m, 0.0, z, m);
  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n,k,m, 1.0, v,n, z,m, 1.0, w, n);

  for (int i=0;i<m;i++) {
    for (int j=0;j<k;j++) {
      w[i+j*n]=x[i+j*m];
    }
  }

 clean_up:
  free(x);
  free(y);
  free(z);
  return err;
}

static ScamacErrorCode apply_inverse_modified_LU_real(const double *v, int n, int m, const double * a, const double * s, double *w, int k) {
  ScamacErrorCode err = SCAMAC_EOK;

  double *x = NULL, *y = NULL;
  x = malloc(m*k * sizeof *x); if (!x) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  y = malloc(m*k * sizeof *y); if (!y) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }

  cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, m,k,n, 1.0, v,n, w,n, 0.0, x, m);
  cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, m,k,m, 1.0, a,m, x,m, 0.0, y, m);

  for (int i=0;i<m;i++) {
    for (int j=0;j<k;j++) {
      w[i+j*n]=-y[i+j*m];
    }
  }

  for (int i=0;i<m;i++) {
    for (int j=0;j<k;j++) {
      y[i+j*m]=s[i]*y[i+j*m];
    }
  }

  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n,k,m, 1.0, v,n, y,m, 1.0, w, n);

 clean_up:
  free(x);
  free(y);
  return err;
}


static ScamacErrorCode orthogonalize_real(double *v, int ldv, int n, int m, double *r, int *p) {
  ScamacErrorCode err = SCAMAC_EOK;
  
  double llwork;
  lapack_int lwork, info;
  
  

  lwork = -1;
  dgeqp3_(&n, &m, v, &ldv, NULL, NULL, &llwork, &lwork, &info);
  lwork = llwork+0.1;
  double *work = NULL, *tau = NULL;
  lapack_int *jpvt = NULL;
  work = malloc(lwork * sizeof *work); if (!work) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  tau  = malloc(m * sizeof *tau);  if (!tau)  { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  jpvt = malloc(m * sizeof *jpvt); if (!jpvt) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }

  for (int j=0;j<m;j++) { jpvt[j]=0; }
  dgeqp3_(&n, &m, v, &ldv, jpvt, tau, work, &lwork, &info);
  if (info) {
    err=SCAMAC_EFAIL;
    goto clean_up;
  }
  if (r) {
    for (int j=0;j<m;j++) {
      for (int i=0;i<=j;i++) {
	r[i+m*j] = v[i+ldv*j];
      }
      for (int i=j+1;i<m;i++) {
	r[i+m*j] = 0.0;
      }
    }
  }

  if (p) {
    for (int j=0;j<m;j++) {
      p[j]=jpvt[j]-1;
    }
  }

  work = realloc(work, lwork * sizeof *work); if (!work) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  dorgqr_(&n, &m, &m, v, &ldv, tau, work, &lwork, &info);
  if (info) {
    err=SCAMAC_EFAIL;
    goto clean_up;
  }

 clean_up:
  free(tau);
  free(jpvt);
  free(work);
  return err;
}


ScamacErrorCode scamac_vector_orthogonalize (const ScamacVector * v, ScamacVector * w, double * r, int * p) {
  if (!w) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(w) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!scamac_vector_check_aligned(w)) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (v) {
    if (scamac_vector_query_valtype(v) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
    if (!scamac_vector_check_compatible_columns(v,w)) { return SCAMAC_EINVALID; }
    if (!scamac_vector_check_exclusivity(v,w))   { return SCAMAC_EINVALID; }
    if (!scamac_vector_check_aligned(v)) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  }

  ScamacInt nv = scamac_vector_query_nv(w);

  ScamacErrorCode err = SCAMAC_EOK;

  if (v) {
    int mv = scamac_vector_query_m(v);
    int mw = scamac_vector_query_m(w);
    double * S  = NULL;
    double * a  = NULL;
    double * rv = NULL;
    double * rw = NULL;

    S  = malloc(mv    * sizeof *S);      if (!S)  { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
    a  = malloc(mv*mv * sizeof *a);      if (!a)  { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
    rv = malloc(mv*mw * sizeof *rv);     if (!rv) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
    rw = malloc(mw*mw * sizeof *rw);     if (!rw) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }

    double *vp = scamac_vector_getptr(v,0);
    double *wp = scamac_vector_getptr(w,0);
    
    construct_modified_LU_real(vp, nv, mv, a, S);
    err=invert_modified_LU_real(a,mv);                   if (err) { goto clean_up; }
    err=apply_modified_LU_real(vp,nv, mv, a, S, wp, mw); if (err) { goto clean_up; }

    for (int i=0;i<mv;i++) {
      for (int j=0;j<mw;j++) {
	rv[i+j*mv]=wp[i+j*nv];
      }
    }
    
    for (int i=0;i<mv;i++) { 
      for (int j=0;j<mw;j++) { 
	wp[i+j*nv]=0.0; 
      } 
    }
      
    err=orthogonalize_real(wp + mv, nv, nv-mv, mw, rw, p);
    if (err) { goto clean_up; }

    if (r) {
      for (int i=0;i<mv;i++) {
	for (int j=0;j<mw;j++) {
	  r[i +j*(mv+mw)] = rv[i+p[j]*mv];
	}
      }
      for (int i=0;i<mw;i++) {
	for (int j=0;j<mw;j++) {
	  r[(i+mv)+j*(mv+mw)]=rw[i+j*mw];
	}
      }
    }

    err=apply_inverse_modified_LU_real(vp,nv, mv, a, S, wp, mw); if (err) { goto clean_up; }
     
  clean_up: ;
    free(S);
    free(a);
    free(rv);
    free(rw);
  } else {
    int mw = scamac_vector_query_m(w);
    double *wp = scamac_vector_getptr(w,0);
     
    orthogonalize_real(wp, nv, nv, mw, r, p);
  }
  return err;
}


extern void zgeqp3_(lapack_int *m, lapack_int *n, double complex * a, lapack_int * lda,  lapack_int * jpvt, double complex * tau, double complex * work, lapack_int * lwork, double * rwork, lapack_int * info);

extern void zungqr_(lapack_int *m, lapack_int *n, lapack_int *k, double complex * a, lapack_int * lda, double complex * tau, double complex * work, lapack_int * lwork, lapack_int * info);

extern void zgetri_(lapack_int *n, double complex *a, lapack_int *lda, lapack_int *ipiv, double complex *work, lapack_int *lwork, lapack_int *info);


static void construct_modified_LU_complex(const double complex *v, int n, int m, double complex * a, double complex * s) {
  for (int i=0;i<m;i++) {
    for (int j=0;j<m;j++) {
      a[i+j*m]=v[i+j*n];
    }
  }
  
  for (int i=0;i<m;i++) {
    if (creal(a[i*(m+1)]) < 0.0) {
      s[i]=1.0;
    } else {
      s[i]=-1.0;
    }
    a[i*(m+1)]=a[i*(m+1)]-s[i];
    for (int j=i+1;j<m;j++) {
      a[j+i*m]=a[j+i*m]/a[i*(m+1)];
    }
    for (int j=i+1;j<m;j++) {
      for (int k=i+1;k<m;k++) {
	a[k+j*m]=a[k+j*m]-a[k+i*m]*a[i+j*m];
      }
    }
  }
}

static ScamacErrorCode invert_modified_LU_complex(double complex * a, int m) {
  ScamacErrorCode err = SCAMAC_EOK;

  lapack_int n = m;
  double complex llwork;
  lapack_int lwork, info;

  

  lwork=-1;
  zgetri_(&n,a,&n,NULL,&llwork,&lwork,&info);
  lwork=llwork+0.1;

  double complex *work = NULL;
  lapack_int *ipiv = NULL;
  work=malloc(lwork* sizeof *work); if (!work) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  ipiv=malloc(n * sizeof *ipiv);    if (!ipiv) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  
  for (int i=0;i<n;i++) {
    ipiv[i]=i+1;
  }
  zgetri_(&n,a,&n,ipiv,work,&lwork,&info);
  if (info) {
    err=SCAMAC_EFAIL;
  }
  
 clean_up:
  free(work);
  free(ipiv);
  return err;
}

static ScamacErrorCode apply_modified_LU_complex(const double complex *v, int n, int m, const double complex * a, const double complex * s, double complex *w, int k) {
  ScamacErrorCode err = SCAMAC_EOK;

  double complex *x = NULL, *y = NULL, *z = NULL;
  x = malloc(m*k * sizeof *x); if (!x) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  y = malloc(m*k * sizeof *y); if (!y) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  z = malloc(m*k * sizeof *z); if (!z) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }

  double complex ZERO=0.0, ONE=1.0;
  cblas_zgemm(CblasColMajor, CblasConjTrans, CblasNoTrans, m,k,n, &ONE, v,n, w,n, &ZERO, x, m);

  for (int i=0;i<m;i++) {
    for (int j=0;j<k;j++) {
      y[i+j*m]=s[i]*x[i+j*m]-w[i+j*n];
    }
  }

  cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m,k,m, &ONE, a,m, y,m, &ZERO, z, m);
  cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n,k,m, &ONE, v,n, z,m, &ONE , w, n);

  for (int i=0;i<m;i++) {
    for (int j=0;j<k;j++) {
      w[i+j*n]=x[i+j*m];
    }
  }

 clean_up:
  free(x);
  free(y);
  free(z);
  return err;
}

static ScamacErrorCode apply_inverse_modified_LU_complex(const double complex *v, int n, int m, const double complex * a, const double complex * s, double complex *w, int k) {
  ScamacErrorCode err = SCAMAC_EOK;

  double complex *x = NULL, *y = NULL;
  x = malloc(m*k * sizeof *x); if (!x) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  y = malloc(m*k * sizeof *y); if (!y) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }

  double complex ZERO=0.0, ONE=1.0;
  cblas_zgemm(CblasColMajor, CblasConjTrans, CblasNoTrans, m,k,n, &ONE, v,n, w,n, &ZERO, x, m);
  cblas_zgemm(CblasColMajor, CblasConjTrans, CblasNoTrans, m,k,m, &ONE, a,m, x,m, &ZERO, y, m);

  for (int i=0;i<m;i++) {
    for (int j=0;j<k;j++) {
      w[i+j*n]=-y[i+j*m];
    }
  }

  for (int i=0;i<m;i++) {
    for (int j=0;j<k;j++) {
      y[i+j*m]=s[i]*y[i+j*m];
    }
  }

  cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n,k,m, &ONE, v,n, y,m, &ONE, w, n);

 clean_up:
  free(x);
  free(y);
  return err;
}


static ScamacErrorCode orthogonalize_complex(double complex *v, int ldv, int n, int m, double complex *r, int *p) {
  ScamacErrorCode err = SCAMAC_EOK;
  
  double complex llwork;
  lapack_int lwork, info;

  lwork = -1;
  zgeqp3_(&n, &m, v, &ldv, NULL, NULL, &llwork, &lwork, NULL, &info);
  lwork = creal(llwork)+0.1;
  double complex *work = NULL, *tau = NULL;
  lapack_int *jpvt = NULL;
  double *rwork = NULL;
  work = malloc(lwork * sizeof *work); if (!work) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  tau  = malloc(m * sizeof *tau);  if (!tau)  { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  jpvt = malloc(m * sizeof *jpvt); if (!jpvt) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  rwork = malloc(2*m * sizeof *rwork); if (!rwork) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  
  for (int j=0;j<m;j++) { jpvt[j]=0; }
  zgeqp3_(&n, &m, v, &ldv, jpvt, tau, work, &lwork, rwork, &info);
  if (info) {
    err=SCAMAC_EFAIL;
    goto clean_up;
  }
  if (r) {
    for (int j=0;j<m;j++) {
      for (int i=0;i<=j;i++) {
	r[i+m*j] = v[i+ldv*j];
      }
      for (int i=j+1;i<m;i++) {
	r[i+m*j] = 0.0;
      }
    }
  }
  
  if (p) {
    for (int j=0;j<m;j++) {
      p[j]=jpvt[j]-1;
    }
  }

  work = realloc(work, lwork * sizeof *work); if (!work) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
  zungqr_(&n, &m, &m, v, &ldv, tau, work, &lwork, &info);
  if (info) {
    err=SCAMAC_EFAIL;
    goto clean_up;
  }

 clean_up:
  free(tau);
  free(jpvt);
  free(work);
  free(rwork);
  return err;
}


ScamacErrorCode scamac_vector_zorthogonalize (const ScamacVector *v, ScamacVector *w, double complex *r, int *p) {
  if (!w) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(w) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!scamac_vector_check_aligned(w)) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (v) {
    if (scamac_vector_query_valtype(v) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
    if (!scamac_vector_check_compatible_columns(v,w)) { return SCAMAC_EINVALID; }
    if (!scamac_vector_check_exclusivity(v,w))   { return SCAMAC_EINVALID; }
    if (!scamac_vector_check_aligned(v)) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  }

  ScamacInt nv = scamac_vector_query_nv(w);

  ScamacErrorCode err = SCAMAC_EOK;

  if (v) {
    int mv = scamac_vector_query_m(v);
    int mw = scamac_vector_query_m(w);
    double complex * S  = NULL;
    double complex * a  = NULL;
    double complex * rv = NULL;
    double complex * rw = NULL;

    S  = malloc(mv    * sizeof *S);      if (!S)  { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
    a  = malloc(mv*mv * sizeof *a);      if (!a)  { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
    rv = malloc(mv*mw * sizeof *rv);     if (!rv) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }
    rw = malloc(mw*mw * sizeof *rw);     if (!rw) { err=SCAMAC_EMALLOCFAIL; goto clean_up; }

    double complex *vp = scamac_vector_getptr(v,0);
    double complex *wp = scamac_vector_getptr(w,0);
    
    construct_modified_LU_complex(vp, nv, mv, a, S);
    err=invert_modified_LU_complex(a,mv);                   if (err) { goto clean_up; }
    err=apply_modified_LU_complex(vp,nv, mv, a, S, wp, mw); if (err) { goto clean_up; }

    for (int i=0;i<mv;i++) {
      for (int j=0;j<mw;j++) {
	rv[i+j*mv]=wp[i+j*nv];
      }
    }
    
    for (int i=0;i<mv;i++) { 
      for (int j=0;j<mw;j++) { 
	wp[i+j*nv]=0.0; 
      } 
    }

    err=orthogonalize_complex(wp + mv, nv, nv-mv, mw, rw, p);
    if (err) { goto clean_up; }

    if (r) {
      for (int i=0;i<mv;i++) {
	for (int j=0;j<mw;j++) {
	  r[i +j*(mv+mw)] = rv[i+p[j]*mv];
	}
      }
      for (int i=0;i<mw;i++) {
	for (int j=0;j<mw;j++) {
	  r[(i+mv)+j*(mv+mw)]=rw[i+j*mw];
	}
      }
    }

    err=apply_inverse_modified_LU_complex(vp,nv, mv, a, S, wp, mw); if (err) { goto clean_up; }

  clean_up: ;
    free(S);
    free(a);
    free(rv);
    free(rw);
  } else {
    int mw = scamac_vector_query_m(w);
    double complex *wp = scamac_vector_getptr(w,0);
     
    orthogonalize_complex(wp, nv, nv, mw, r, p);
  }
  return err;
}
