#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "scamac_sparsemat_mvm.h"
#include "scamac_sparsemat_internal.h"

#include "scamac_internal.h"
#include "scamac_aux.h"
#include "scamac_safeint.h"
#include "scamac_generator.h"


static void scamac_sparsemat_mvm_raw(const ScamacMatrix * sm, const double * x, double * y, double alpha, double beta, double gamma) {
  
  ScamacIdx i,j;

  if (alpha==0.0 && beta==0.0) {
    for (i=0; i<sm->nr; i++) {
      y[i]=gamma*x[i];
    }
    return;
  }
  if (alpha==0.0) {
    for (i=0; i<sm->nr; i++) {
      y[i]=beta*y[i]+gamma*x[i];
    }
    return;
  }

  for (i=0; i<sm->nr; i++) {
    if (beta==0.0) {
      y[i]=gamma*x[i];
    } else {
      y[i]=beta*y[i]+gamma*x[i];
    }
    for (j=sm->rptr[i]; j<sm->rptr[i+1]; j++) {
      y[i]=y[i]+alpha*sm->val[j]*x[sm->cind[j]];
    }
  }

}

static void scamac_sparsemat_mvm_cplx_raw(const ScamacMatrix *sm, const double complex * x, double complex * y,
    double complex alpha, double complex beta, double complex gamma) {
  
  ScamacIdx i,j;

  if (alpha==0.0 && beta==0.0) {
    for (i=0; i<sm->nr; i++) {
      y[i]=gamma*x[i];
    }
    return;
  }
  if (alpha==0.0) {
    for (i=0; i<sm->nr; i++) {
      y[i]=beta*y[i]+gamma*x[i];
    }
    return;
  }

  double complex *myval = (double complex *) sm->val;

  for (i=0; i<sm->nr; i++) {
    if (beta==0.0) {
      y[i]=gamma*x[i];
    } else {
      y[i]=beta*y[i]+gamma*x[i];
    }
    for (j=sm->rptr[i]; j<sm->rptr[i+1]; j++) {
      y[i]=y[i]+alpha*myval[j]*x[sm->cind[j]];
    }
  }

}


static void scamac_sparsemat_mvm_real_cplx_raw(const ScamacMatrix *sm, const double complex * x, double complex * y,
    double complex alpha, double complex beta, double complex gamma) {
  
  ScamacIdx i,j;

  if (alpha==0.0 && beta==0.0) {
    for (i=0; i<sm->nr; i++) {
      y[i]=gamma*x[i];
    }
    return;
  }
  if (alpha==0.0) {
    for (i=0; i<sm->nr; i++) {
      y[i]=beta*y[i]+gamma*x[i];
    }
    return;
  }
  
  for (i=0; i<sm->nr; i++) {
    if (beta==0.0) {
      y[i]=gamma*x[i];
    } else {
      y[i]=beta*y[i]+gamma*x[i];
    }
    for (j=sm->rptr[i]; j<sm->rptr[i+1]; j++) {
      y[i]=y[i]+alpha*( (double complex) sm->val[j])*x[sm->cind[j]];
    }
  }

}



ScamacErrorCode scamac_sparsemat_mvm(const ScamacMatrix *sm, const ScamacVector *x, ScamacVector *y, double alpha, double beta, double gamma) {
  if (!sm) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!x)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!y)  { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (scamac_sparsemat_query_valtype(sm) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if ((gamma != 0.0) && (sm->nr != sm->nc)) {
    return scamac_error_set_par(SCAMAC_EINPUT,6);
  }
  if (scamac_sparsemat_query_nc(sm) != scamac_vector_query_nv(x)) { return scamac_error_set_par(SCAMAC_EINPUT, 2); }
  if (scamac_sparsemat_query_nr(sm) != scamac_vector_query_nv(y)) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT, 2); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
  
  if (!scamac_vector_check_compatibility(x,y)) { return SCAMAC_EINPUT; }
  if (!scamac_vector_check_exclusivity(x,y))   { return SCAMAC_EINPUT; }
  
  ScamacInt i;
  for (i=0;i<scamac_vector_query_m(x);i++) {
    scamac_sparsemat_mvm_raw(sm, scamac_vector_getptr(x,i), scamac_vector_getptr(y,i), alpha,beta,gamma);
  }
  
  return SCAMAC_EOK;
}


ScamacErrorCode scamac_sparsemat_mvm_cplx(const ScamacMatrix *sm, const ScamacVector *x, ScamacVector *y,
    double complex alpha, double complex beta, double complex gamma) {
  if (!sm) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!x)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!y)  { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if ((scamac_sparsemat_query_valtype(sm) != SCAMAC_VAL_REAL) && (scamac_sparsemat_query_valtype(sm) != SCAMAC_VAL_COMPLEX)) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if ((gamma != 0.0) && (sm->nr != sm->nc)) {
    return scamac_error_set_par(SCAMAC_EINPUT,6);
  }
  if (scamac_sparsemat_query_nc(sm) != scamac_vector_query_nv(x)) { return scamac_error_set_par(SCAMAC_EINPUT, 2); }
  if (scamac_sparsemat_query_nr(sm) != scamac_vector_query_nv(y)) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT, 2); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
    
  if (!scamac_vector_check_compatibility(x,y)) { return SCAMAC_EINPUT; }
  if (!scamac_vector_check_exclusivity(x,y))   { return SCAMAC_EINPUT; }
  
  ScamacInt i;
  for (i=0;i<scamac_vector_query_m(x);i++) {
    if (scamac_sparsemat_query_valtype(sm) == SCAMAC_VAL_REAL) {
      scamac_sparsemat_mvm_real_cplx_raw(sm, scamac_vector_getptr(x,i), scamac_vector_getptr(y,i), alpha,beta,gamma);
    } else {
      scamac_sparsemat_mvm_cplx_raw(sm, scamac_vector_getptr(x,i), scamac_vector_getptr(y,i), alpha,beta,gamma);
    }
  }
  
  return SCAMAC_EOK;
} 
