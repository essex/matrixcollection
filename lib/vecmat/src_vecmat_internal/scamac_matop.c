#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "scamac_matop.h"
#include "scamac_sparsemat_mvm.h"

#include "scamac_internal.h"
#include "scamac_aux.h"
#include "scamac_safeint.h"
#include "scamac_generator.h"

struct scamac_matop_st {
  ScamacInt nr;
  ScamacInt nc;
  ScamacValType valtype;
  ScamacMatrix * sm;
  bool sm_allocated;
};

static ScamacErrorCode scamac_matop_alloc(ScamacInt nr, ScamacInt nc, ScamacValType valtype, ScamacMatop ** sm) {
  if ((valtype != SCAMAC_VAL_REAL) && (valtype != SCAMAC_VAL_COMPLEX)) {
    return SCAMAC_EINVALID;
  }
  if (!sm) {
    return SCAMAC_ENULL;
  }
  if ((nr<1) || (nc<1)) {
    return SCAMAC_ERANGE;
  }
  
  ScamacMatop * my_sm = malloc(sizeof * my_sm);
  if (!my_sm) {
    return SCAMAC_EMALLOCFAIL;
  }
  my_sm->nr = nr;
  my_sm->nc = nc;
  my_sm->valtype = valtype;
  my_sm->sm = NULL;
  my_sm->sm_allocated = false;

  *sm = my_sm;
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_matop_from_matrix(const ScamacMatrix * mat, ScamacMatop ** sm) {
  if (!mat) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!sm)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  ScamacInt nr = scamac_sparsemat_query_nr(mat);
  ScamacInt nc = scamac_sparsemat_query_nc(mat);
  ScamacValType valtype = scamac_sparsemat_query_valtype(mat);

  ScamacErrorCode err;
  err=scamac_matop_alloc(nr,nc,valtype,sm);
  if (err) { return scamac_error_set_internal(err); }
  (*sm)->sm = (ScamacMatrix*) mat;
  (*sm)->sm_allocated = false;
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_matop_from_generator(const ScamacGenerator * gen, ScamacMatop ** sm) {
  if (!gen) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!sm)  { return scamac_error_set_par(SCAMAC_ENULL,2); }

  ScamacErrorCode err;
  
  ScamacMatrix * mat;
  err = scamac_sparsemat_from_generator(gen, &mat);
  if (err) { return err; }

  err = scamac_matop_from_matrix(mat, sm);
  if (err) { return err; }

  (*sm)->sm_allocated = true;
  
  return SCAMAC_EOK;
  
}

ScamacInt scamac_matop_query_nr(const ScamacMatop * sm) {
  if (sm) {
    return sm->nr;
  } else {
    return 0;
  }
}

ScamacInt scamac_matop_query_nc(const ScamacMatop * sm) {
  if (sm) {
    return sm->nc;
  } else {
    return 0;
  }
}

ScamacValType scamac_matop_query_valtype(const ScamacMatop * sm) {
  if (sm) {
    return sm->valtype;
  } else {
    return SCAMAC_VAL_NONE;
  }
}

ScamacErrorCode scamac_matop_free(ScamacMatop * sm) {
  ScamacErrorCode err;
  if (sm) {
    if (sm->sm_allocated) {
      err=scamac_sparsemat_free(sm->sm);
      if (err) return err;
    }
    free(sm);
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_matop_mvm(const ScamacMatop *sm, const ScamacVector *x, ScamacVector *y, double alpha, double beta, double gamma) {
  if (!sm) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!x)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!y)  { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (scamac_matop_query_valtype(sm) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if ((gamma != 0.0) && (scamac_matop_query_nr(sm) != scamac_matop_query_nc(sm))) {
    return scamac_error_set_par(SCAMAC_EINPUT,6);
  }
  if (scamac_matop_query_nc(sm) != scamac_vector_query_nv(x)) { return scamac_error_set_par(SCAMAC_EINPUT, 2); }
  if (scamac_matop_query_nr(sm) != scamac_vector_query_nv(y)) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT, 2); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
  
  if (!scamac_vector_check_compatibility(x,y)) { return SCAMAC_EINVALID; }
  if (!scamac_vector_check_exclusivity(x,y))   { return SCAMAC_EINVALID; }
  
  ScamacErrorCode err;
  assert(sm->sm != NULL);
  err = scamac_sparsemat_mvm(sm->sm, x, y, alpha,beta,gamma);
  if (err) { return scamac_error_set_internal(err); }
  
  return SCAMAC_EOK;
}


ScamacErrorCode scamac_matop_mvm_cplx(const ScamacMatop *sm, const ScamacVector *x, ScamacVector *y,
    double complex alpha, double complex beta, double complex gamma) {
  if (!sm) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!x)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!y)  { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if ((scamac_matop_query_valtype(sm) != SCAMAC_VAL_REAL) && (scamac_matop_query_valtype(sm) != SCAMAC_VAL_COMPLEX)) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if ((gamma != 0.0) && (scamac_matop_query_nr(sm) != scamac_matop_query_nc(sm))) {
    return scamac_error_set_par(SCAMAC_EINPUT,6);
  }
  if (scamac_matop_query_nc(sm) != scamac_vector_query_nv(x)) { return scamac_error_set_par(SCAMAC_EINPUT, 2); }
  if (scamac_matop_query_nr(sm) != scamac_vector_query_nv(y)) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT, 2); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
    
  if (!scamac_vector_check_compatibility(x,y)) { return SCAMAC_EINPUT; }
  if (!scamac_vector_check_exclusivity(x,y))   { return SCAMAC_EINPUT; }
  
  ScamacErrorCode err;
  assert(sm->sm != NULL);
  err = scamac_sparsemat_mvm_cplx(sm->sm, x, y, alpha,beta,gamma);
  if (err) { return scamac_error_set_internal(err); }
  
  return SCAMAC_EOK;
} 
