#include "scamac_vector.h"
#include "scamac_internal.h"
#include "scamac_cblas.h"
#include "scamac_rng.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <stdbool.h>


struct scamac_vectormem_st {
  ScamacInt nv;
  int m;
  ScamacValType valtype;
  void * memptr;
};


struct scamac_vector_st {
  ScamacVectorMem * vm;
  int m;
  int * idx;
  bool aligned;
};



ScamacErrorCode scamac_vectormem_alloc(ScamacInt nv, int m, ScamacValType valtype, ScamacVectorMem ** v) {
  if (nv<1) { return scamac_error_set_par(SCAMAC_ERANGE,1); }
  if (m<1)  { return scamac_error_set_par(SCAMAC_ERANGE,2); }
  if ((valtype != SCAMAC_VAL_REAL) && (valtype != SCAMAC_VAL_COMPLEX)) { return scamac_error_set_par(SCAMAC_EINPUT,3); }
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL, 4); }
  if (*v) { return scamac_error_set_par(SCAMAC_EINPUT, 4); }

  ScamacVectorMem * my_v;
  my_v = malloc(sizeof * my_v);
  if (!my_v) { return SCAMAC_EMALLOCFAIL; }
    
  my_v ->nv = nv;
  my_v ->m = m;
  my_v ->valtype = valtype;
    
  
  if (valtype == SCAMAC_VAL_REAL) {
    my_v->memptr = malloc(m*nv * sizeof(double));
  } else if (valtype == SCAMAC_VAL_COMPLEX) {
    my_v->memptr = malloc(m*nv * sizeof(double complex));
  }
  
  if (!my_v->memptr) { return SCAMAC_EMALLOCFAIL; } 
  
  *v = my_v;
  
  return SCAMAC_EOK;

}

ScamacErrorCode scamac_vectormem_free(ScamacVectorMem * v) {
  if (v) {
    free(v->memptr);
    free(v);
  }
  return SCAMAC_EOK;
}


static ScamacErrorCode scamac_vector_alloc(ScamacVectorMem * vm, ScamacVector ** v) {
  if (!vm) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!v)  { return scamac_error_set_par(SCAMAC_ENULL,2); }

  if (!(*v)) {
    ScamacVector * my_v;

    my_v = malloc(sizeof * my_v);
    if (!my_v) return SCAMAC_EMALLOCFAIL;
    my_v->vm = vm;
    my_v->m  = vm->m;
    my_v->idx = malloc(vm->m * sizeof *(my_v->idx));
    if (!(my_v->idx)) return SCAMAC_EMALLOCFAIL;
    *v = my_v;
  } else {
    if ((*v)->vm != vm) { return scamac_error_set_par(SCAMAC_ECORRUPTED,2); }
  }
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_free(ScamacVector * v) {
  if (v) {
    free(v->idx);
    free(v);
  }
  return SCAMAC_EOK;
}


ScamacErrorCode scamac_vector_view(ScamacVectorMem * vm, ScamacVector ** v) {
  if (!vm) { return scamac_error_set_par(SCAMAC_ENULL, 1); }
  if (!v)  { return scamac_error_set_par(SCAMAC_ENULL, 2); }
  if (!scamac_vectormem_check(vm)) {return scamac_error_set_par(SCAMAC_ECORRUPTED, 1);}
  
  ScamacErrorCode err;
  err = scamac_vector_alloc(vm, v);
  if (err) { return scamac_error_set_internal(err); }  
  
  int i;
  for (i=0;i<vm->m;i++) {
    (*v)->idx[i] = i;
  }
  (*v)->m=vm->m;
  (*v)->aligned=true;
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_view_single(ScamacVectorMem * vm, int i, ScamacVector ** v) {
  if (!vm) { return scamac_error_set_par(SCAMAC_ENULL, 1); }
  if (!scamac_vectormem_check(vm)) {return scamac_error_set_par(SCAMAC_ECORRUPTED, 1);}
  if ((i<0) || (i>=vm->m)) { return scamac_error_set_par(SCAMAC_ERANGE, 2); }
  if (!v)  { return scamac_error_set_par(SCAMAC_ENULL, 3); }
  
  ScamacErrorCode err;
  err = scamac_vector_view(vm, v);
  if (err) { return scamac_error_set_internal(err); }
  err = scamac_vector_sub_single(*v, i, v);
  if (err) { return scamac_error_set_internal(err); }
    
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_view_range (ScamacVectorMem * vm, int ia, int ib, ScamacVector ** v) {
  if (!vm) { return scamac_error_set_par(SCAMAC_ENULL, 1); }
  if (!scamac_vectormem_check(vm)) {return scamac_error_set_par(SCAMAC_ECORRUPTED, 1);}
  if ((ia<0) || (ia>=vm->m)) { return scamac_error_set_par(SCAMAC_ERANGE, 2); }
  if ((ib<0) || (ib>=vm->m)) { return scamac_error_set_par(SCAMAC_ERANGE, 3); }
  if (ia>ib) { return SCAMAC_EINVALID; }
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL, 4); }
  
  ScamacErrorCode err;
  err = scamac_vector_view(vm, v);
  if (err) { return scamac_error_set_internal(err); }
  err = scamac_vector_sub_range(*v, ia, ib, v);
  if (err) { return scamac_error_set_internal(err); }
    
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_view_mask  (ScamacVectorMem * vm, const bool *mask, ScamacVector ** v) {
  if (!vm) { return scamac_error_set_par(SCAMAC_ENULL, 1); }
  if (!scamac_vectormem_check(vm)) {return scamac_error_set_par(SCAMAC_ECORRUPTED, 1);}
  if (!mask) { return scamac_error_set_par(SCAMAC_ENULL, 2); }
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL, 3); }
    
  ScamacErrorCode err;
  err = scamac_vector_view(vm, v);
  if (err) { return scamac_error_set_internal(err); }
  err = scamac_vector_sub_mask(*v, mask, v);
  if (err) { return scamac_error_set_internal(err); }
    
  return SCAMAC_EOK;
}
  



ScamacInt scamac_vector_query_nv   (const ScamacVector * v) {
  if (!v) { return 0; }
  if (!v->vm) { return 0; }
  return v->vm->nv;
}

int scamac_vector_query_m    (const ScamacVector * v) {
  if (!v) { return 0; }
  return v->m;
}

ScamacValType scamac_vector_query_valtype(const ScamacVector *v ) {
  if (!v) { return SCAMAC_VAL_NONE; }
  if (!v->vm) { return SCAMAC_VAL_NONE; }
  return v->vm->valtype;
}

bool scamac_vector_check(const ScamacVector * v) {
  if (!v)        { return false; }
  if (!(v->vm))  { return false; }
  if (!(v->idx)) { return false; }
  if ( (v->m<0) || (v->m>v->vm->m) ) { return false;}
  ScamacInt i;
  for (i=0;i<v->m;i++) {
    if ( (v->idx[i]<0) || (v->idx[i]>=v->vm->m) ) { return false; }
  }
  for (i=0;i<v->m-1;i++) {
    if (v->idx[i]>=v->idx[i+1])  { return false; }
  }

	
  return true;
}

bool scamac_vectormem_check(const ScamacVectorMem * vm) {
  if (!vm)       { return false; }
  if (vm->nv<1) { return false; }
  if (vm->m<1)  { return false; }
  if ((vm->valtype != SCAMAC_VAL_REAL) && (vm->valtype != SCAMAC_VAL_COMPLEX)) { return false; }
  if (!vm->memptr) {return false; }
  return true;
}


bool scamac_vector_check_compatibility(const ScamacVector * v1, const ScamacVector * v2) {
  if (!v1) { return false; }
  if (!v2) { return false; }

  
  if ( (scamac_vector_query_nv(v1) != scamac_vector_query_nv(v2)) || (scamac_vector_query_m(v1) != scamac_vector_query_m(v2)) || (scamac_vector_query_valtype(v1) != scamac_vector_query_valtype(v2)) ) { return false; }
  
  return true;
}

bool scamac_vector_check_compatible_columns(const ScamacVector * v1, const ScamacVector * v2) {
  if (!v1) { return false; }
  if (!v2) { return false; }

  
  if ( (scamac_vector_query_nv(v1) != scamac_vector_query_nv(v2)) || (scamac_vector_query_valtype(v1) != scamac_vector_query_valtype(v2)) ) { return false; }
  
  return true;
}

bool scamac_vector_check_aligned  (const ScamacVector * v) {
  if (v) {
    return v->aligned;
  } else {
    return true;
  }
}

static void scamac_vector_set_aligned(ScamacVector * v) {
  if (v) {
    v->aligned=true;
    int i;
    for (i=0;i<v->m-1;i++) {
      if (v->idx[i+1] != v->idx[i]+1) {
	v->aligned=false;
	return;
      }
    }
  }
}

bool scamac_vector_check_exclusivity  (const ScamacVector * v1, const ScamacVector * v2) {
  if (!v1) { return false; }
  if (!v2) { return false; }
  if (v1->vm == v2->vm) {
    ScamacInt i,j;
    for (i=0;i<scamac_vector_query_m(v1);i++) {
      for (j=0;j<scamac_vector_query_m(v2);j++) {
	if (v1->idx[i]==v2->idx[j]) { return false; }
      }
    }
  }
  return true;
}


void * scamac_vector_getptr(const ScamacVector * v, ScamacInt i) {
  if (!v) { return NULL; }
  if ((i<0) || (i>=v->m)) { return NULL; }
  
  if (v->vm->valtype == SCAMAC_VAL_REAL) {
    double * p = (double *) v->vm->memptr;
    p = p + ((ScamacInt) v->idx[i]) * v->vm->nv;
    return p;
  } else {
    double complex * p = (double complex *) v->vm->memptr;
    p = p + ((ScamacInt) v->idx[i]) * v->vm->nv;
    return p;
  }
}


ScamacErrorCode scamac_vector_sub_range (const ScamacVector * v, int ia, int ib, ScamacVector ** w) {
  if (!v)   { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if ((ia<0) || (ia>=v->m)) { return scamac_error_set_par(SCAMAC_ERANGE, 2); }
  if ((ib<0) || (ib>=v->m)) { return scamac_error_set_par(SCAMAC_ERANGE, 3); }
  if (ia>ib) { return SCAMAC_EINVALID; }
  if (!w) { return scamac_error_set_par(SCAMAC_ENULL,4); }

  ScamacErrorCode err;
  err = scamac_vector_alloc(v->vm, w);
  if (err) { return scamac_error_set_internal(err); }
    
  int m = ib-ia+1;
  (*w)->m=m;
  int i;
  for (i=0;i<m;i++) {
    (*w)->idx[i]=v->idx[ia+i];
  }
  (*w)->aligned=v->aligned;
    
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_sub_single(const ScamacVector * v, int i, ScamacVector ** w) {
  return scamac_vector_sub_range(v, i,i, w);
}

ScamacErrorCode scamac_vector_sub_mask  (const ScamacVector * v, const bool *mask, ScamacVector ** w) {
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!mask) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!w) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  
  ScamacErrorCode err;
  err = scamac_vector_alloc(v->vm, w);
  if (err) { return scamac_error_set_internal(err); }
  
  int m = 0;
  int i;
  for (i=0;i<v->m;i++) {
    if (mask[i]) { m++; }
  }
  
  (*w)->m=m;
  int j = 0;
  for (i=0;i<v->m;i++) {
    if (mask[i]) { 
      (*w)->idx[j]=v->idx[i];
      j++;
    }
  }
  scamac_vector_set_aligned(*w);
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_complement(const ScamacVector * v, ScamacVector ** w) {
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!w) { return scamac_error_set_par(SCAMAC_ENULL,2); }

  ScamacErrorCode err;
  err = scamac_vector_alloc(v->vm, w);
  if (err) { return scamac_error_set_internal(err); }

  int m = v->vm->m - v->m;

  int i,j,k;
  i=0;
  j=v->m;
  for (k=0;k<v->vm->m;k++) {
    if (i<v->m && k==v->idx[i]) {
      i++;
    } else {
      (*w)->idx[j] = k;
      j++;
    }
  }
  for (j=0;j<m;j++) {
    (*w)->idx[j]=(*w)->idx[j+v->m];
  }
  (*w)->m=m;
  scamac_vector_set_aligned(*w);
    
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_align(ScamacVector * v) {
  if (!v)  { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (v->aligned) {
    return SCAMAC_EOK;
  }

  size_t sizedata;
  if (v->vm->valtype==SCAMAC_VAL_REAL) {
    sizedata=sizeof(double);
  } else {
    sizedata=sizeof(double complex);
  }

  int i0 = v->idx[0];
  for (int i=0;i<v->m;i++) {
    if (v->idx[i] != i0+i) {
      int j=i;
      for (int k=i+1;k<v->m;k++) {
	if (v->idx[k] == v->idx[i]+(k-i)) {
	  j++;
	} else {
	  break;
	}
      }
      char * dest = ((char*) v->vm->memptr) + sizedata * v->vm->nv * (i0+i);
      char * src  = ((char*) v->vm->memptr) + sizedata * v->vm->nv * v->idx[i];
      size_t size = sizedata * v->vm->nv * (j-i+1);
      memmove(dest, src, size);
      for (int k=i;k<=j;k++) {
	v->idx[k]=i0+k;
      }
    }
  }
  v->aligned=true;
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_align_front(ScamacVector * v) {
  if (!v)  { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (v->aligned && (v->idx[0]==0)) {
    return SCAMAC_EOK;
  }
  
  size_t sizedata;
  if (v->vm->valtype==SCAMAC_VAL_REAL) {
    sizedata=sizeof(double);
  } else {
    sizedata=sizeof(double complex);
  }

  int i0 = 0;
  for (int i=0;i<v->m;i++) {
    if (v->idx[i] != i0+i) {
      int j=i;
      for (int k=i+1;k<v->m;k++) {
	if (v->idx[k] == v->idx[i]+(k-i)) {
	  j++;
	} else {
	  break;
	}
      }
      char * dest = ((char*) v->vm->memptr) + sizedata * v->vm->nv * (i0+i);
      char * src  = ((char*) v->vm->memptr) + sizedata * v->vm->nv * v->idx[i];
      size_t size = sizedata * v->vm->nv * (j-i+1);
      memmove(dest, src, size);
      for (int k=i;k<=j;k++) {
	v->idx[k]=i0+k;
      }
    }
  }
  v->aligned=true;
  
  return SCAMAC_EOK;
}




ScamacErrorCode scamac_vector_copy (const ScamacVector * v, ScamacVector * w) {
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!w) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!scamac_vector_check_compatibility(v,w)) { return SCAMAC_EINVALID; }
  if (!scamac_vector_check_exclusivity(v,w))   { return SCAMAC_EINVALID; }
    
  size_t vecsize;
  if (scamac_vector_query_valtype(v) == SCAMAC_VAL_REAL) {
    vecsize = scamac_vector_query_nv(v) * sizeof(double);
  } else {
    vecsize = scamac_vector_query_nv(v) * sizeof(double complex);
  }
  int i;
  for (i=0;i<v->m;i++) {
    memcpy(scamac_vector_getptr(w,i), scamac_vector_getptr(v,i), vecsize);
  }
  
  return SCAMAC_EOK;
}



ScamacErrorCode scamac_vector_zero (ScamacVector * v) {
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!scamac_vector_check(v)) {return SCAMAC_EINPUT;}

  size_t vecsize;
  if (scamac_vector_query_valtype(v) == SCAMAC_VAL_REAL) {
    vecsize = scamac_vector_query_nv(v) * sizeof(double);
  } else {
    vecsize = scamac_vector_query_nv(v) * sizeof(double complex);
  }
  
  int i;
  for (i=0;i<v->m;i++) {
    memset(scamac_vector_getptr(v,i), 0, vecsize );
  }
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_random(const char * seed, bool use_entropy, bool normalize, ScamacVector * v) {
  if (!seed) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!v)    { return scamac_error_set_par(SCAMAC_ENULL,4); }
  if (!scamac_vector_check(v)) { return scamac_error_set_par(SCAMAC_EINPUT,4); }
  
  scamac_rng_seed_ty r_seed;
  if (use_entropy) {
    r_seed = scamac_rng_entropy(seed);
  } else {
    r_seed = scamac_rng_string_to_seed(seed);
  }
  
  ScamacErrorCode err;

  ScamacInt nv;
  
  if (scamac_vector_query_valtype(v)==SCAMAC_VAL_REAL) {
    nv = scamac_vector_query_nv(v);
  } else {
    nv = 2*scamac_vector_query_nv(v);
  }
  
  int i;
  for (i=0;i<v->m;i++) {
    err=scamac_ranvec_double(r_seed, (ScamacIdx) nv * i, nv, -1.0, 1.0, scamac_vector_getptr(v,i));
    if (err) { return err; }
    if (normalize) {
      while (true) {
	double nrm=cblas_dnrm2(nv, scamac_vector_getptr(v,i),1);
	if (nrm>DBL_EPSILON) { 
	  cblas_dscal(nv, 1.0/nrm, scamac_vector_getptr(v,i),1);
	  break;
	}
	err=scamac_ranvec_double(r_seed, (ScamacIdx) nv * i, nv, -1.0, 1.0, scamac_vector_getptr(v,i));
	if (err) { return err; }
      }
    }
  }
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_assign(const char *which, ScamacVector * v) {
  if (!which) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!v)     { return scamac_error_set_par(SCAMAC_ENULL,2); }

  
  if (!strcmp(which,"Hilbert")) {
    if (scamac_vector_query_valtype(v)==SCAMAC_VAL_REAL) {
      for (int j=0;j<scamac_vector_query_m(v);j++) {
	double *vp =scamac_vector_getptr(v,j);
	if (!vp) { return SCAMAC_EFAIL; }
	for (int i=0;i<scamac_vector_query_nv(v);i++) {
	  vp[i]=1.0/(1.0+i+j);
	}
      }
    } else {
      for (int j=0;j<scamac_vector_query_m(v);j++) {
	double complex *vp =scamac_vector_getptr(v,j);
	if (!vp) { return SCAMAC_EFAIL; }
	for (int i=0;i<scamac_vector_query_nv(v);i++) {
	  vp[i]=1.0/(1.0+i+j);
	}
      }
    }
  } else if (!strcmp(which,"Nearly_Dependent")) {
    if (scamac_vector_query_valtype(v)==SCAMAC_VAL_REAL) {
      for (int j=0;j<scamac_vector_query_m(v);j++) {
	double *vp =scamac_vector_getptr(v,j);
	if (!vp) { return SCAMAC_EFAIL; }
	for (int i=0;i<scamac_vector_query_nv(v);i++) {
	  vp[i]=0.0;
	}
	vp[j]=1.e-20;
	vp[0]=1.0;
      }
    } else {
      for (int j=0;j<scamac_vector_query_m(v);j++) {
	double complex *vp =scamac_vector_getptr(v,j);
	if (!vp) { return SCAMAC_EFAIL; }
	for (int i=0;i<scamac_vector_query_nv(v);i++) {
	  vp[i]=0.0;
	}
	vp[j]=1.e-20;
	vp[0]=1.0;
      }
    }
  } else if (!strcmp(which,"Unit")) {
    if (scamac_vector_query_valtype(v)==SCAMAC_VAL_REAL) {
      for (int j=0;j<scamac_vector_query_m(v);j++) {
	double *vp =scamac_vector_getptr(v,j);
	if (!vp) { return SCAMAC_EFAIL; }
	for (int i=0;i<scamac_vector_query_nv(v);i++) {
	  vp[i]=0.0;
	}
	vp[j]=1.0;
      }
    } else {
      for (int j=0;j<scamac_vector_query_m(v);j++) {
	double complex *vp =scamac_vector_getptr(v,j);
	if (!vp) { return SCAMAC_EFAIL; }
	for (int i=0;i<scamac_vector_query_nv(v);i++) {
	  vp[i]=0.0;
	}
	vp[j]=1.0;
      }
    }
  } else {
    return scamac_error_set_par(SCAMAC_EINPUT,1);
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_dot (const ScamacVector * v1, const ScamacVector * v2, double * dot) {
  if (!v1)  { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(v1) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!v2)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(v2) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!dot) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (!scamac_vector_check_compatibility(v1,v2)) {return SCAMAC_EINVALID;}

  ScamacInt nv = scamac_vector_query_nv(v1);
  ScamacInt i;
  for (i=0;i<v1->m;i++) {
    dot[i] = cblas_ddot(nv, scamac_vector_getptr(v1,i),1, scamac_vector_getptr(v2,i),1);
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_zdot(const ScamacVector * v1, const ScamacVector * v2, double complex * zdot) {
  if (!v1)  { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(v1) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!v2)  { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(v2) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!zdot) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (!scamac_vector_check_compatibility(v1,v2)) {return SCAMAC_EINVALID;}

  ScamacInt nv = scamac_vector_query_nv(v1);  
  ScamacInt i;
  for (i=0;i<v1->m;i++) {
    cblas_zdotc_sub(nv, scamac_vector_getptr(v1,i),1, scamac_vector_getptr(v2,i),1, &zdot[i]);
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_nrm2 (const ScamacVector * v, double * nrm) {
  if (!v)  { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!scamac_vector_check(v)) { return scamac_error_set_par(SCAMAC_ECORRUPTED,1); }
  if (!nrm) { return scamac_error_set_par(SCAMAC_ENULL,2); }

  if (scamac_vector_query_valtype(v)==SCAMAC_VAL_REAL) {
    ScamacInt nv = scamac_vector_query_nv(v);
    ScamacInt i;
    for (i=0;i<scamac_vector_query_m(v);i++) {
      nrm[i]=cblas_dnrm2(nv, scamac_vector_getptr(v,i),1);
    }
  } else {
    ScamacInt nv = scamac_vector_query_nv(v);
    ScamacInt i;
    for (i=0;i<scamac_vector_query_m(v);i++) {
      nrm[i]=cblas_dznrm2(nv, scamac_vector_getptr(v,i),1);
    }
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_dotmatrix (const ScamacVector * v1, const ScamacVector * v2, double * dotm) {
  if (!v1)   { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(v1) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!v2)   { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(v2) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!dotm) { return scamac_error_set_par(SCAMAC_ENULL,3); }

  int m1 = scamac_vector_query_m(v1);
  int m2 = scamac_vector_query_m(v2);
  ScamacInt nv = scamac_vector_query_nv(v1);

  if (scamac_vector_check_aligned(v1) && scamac_vector_check_aligned(v2)) {
    double * v1p = scamac_vector_getptr(v1,0);
    double * v2p = scamac_vector_getptr(v2,0);
    cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, m1,m2,nv, 1.0, v1p,nv, v2p,nv, 0.0, dotm, m1);
  } else {
    int i,j;
    for (i=0;i<m1;i++) {
      for (j=0;j<m2;j++) {
	dotm[i + m1*j] = cblas_ddot(nv, scamac_vector_getptr(v1,i),1, scamac_vector_getptr(v2,j),1);
      }
    }
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_zdotmatrix (const ScamacVector * v1, const ScamacVector * v2, double complex * dotm) {
  if (!v1)   { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(v1) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!v2)   { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(v2) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!dotm) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  
  int m1 = scamac_vector_query_m(v1);
  int m2 = scamac_vector_query_m(v2);
  ScamacInt nv = scamac_vector_query_nv(v1);

  if (scamac_vector_check_aligned(v1) && scamac_vector_check_aligned(v2)) {
    double complex * v1p = scamac_vector_getptr(v1,0);
    double complex * v2p = scamac_vector_getptr(v2,0);
    double complex ZERO=0.0, ONE=1.0;
    cblas_zgemm(CblasColMajor, CblasConjTrans, CblasNoTrans, m1,m2,nv, &ONE, v1p,nv, v2p,nv, &ZERO, dotm, m1);
  } else {
    int i,j;
    for (i=0;i<m1;i++) {
      for (j=0;j<m2;j++) {
	cblas_zdotc_sub(nv, scamac_vector_getptr(v1,i),1, scamac_vector_getptr(v2,j),1, &dotm[i + m1*j]);
      }
    }
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_gram (const ScamacVector * v, double * gram) {
  if (!v)    { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(v) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!gram) { return scamac_error_set_par(SCAMAC_ENULL,2); }

  int m = scamac_vector_query_m(v);
  ScamacInt nv = scamac_vector_query_nv(v);
  int i,j;
  for (i=0;i<m;i++) {
    for (j=0;j<=i;j++) {
      gram[i + m*j] = cblas_ddot(nv, scamac_vector_getptr(v,i),1, scamac_vector_getptr(v,j),1);
      gram[j + m*i] = gram[i + m*j];
    }
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_zgram(const ScamacVector * v, double complex * zgram) {
  if (!v)     { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(v) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!zgram) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  
  int m = scamac_vector_query_m(v);
  ScamacInt nv = scamac_vector_query_nv(v);
  int i,j;
  for (i=0;i<m;i++) {
    for (j=0;j<=i;j++) {
      cblas_zdotc_sub(nv, scamac_vector_getptr(v,i),1, scamac_vector_getptr(v,j),1, &zgram[i + m*j]);
      zgram[j + m*i] = conj(zgram[i + m*j]);
    }
  }
  return SCAMAC_EOK;
}


ScamacErrorCode scamac_vector_axpby (const ScamacVector * x, ScamacVector * y, const double *a, const double *b) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!y) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!a) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (!b) { return scamac_error_set_par(SCAMAC_ENULL,4); }
  if (!scamac_vector_check_compatibility(x,y)) { return SCAMAC_EINVALID; }

  ScamacInt nv = scamac_vector_query_nv(x);
  int i;
  for (i=0;i<x->m;i++) {
    cblas_dscal(nv, b[i], scamac_vector_getptr(y,i),1);
    cblas_daxpy(nv, a[i], scamac_vector_getptr(x,i),1, scamac_vector_getptr(y,i),1);
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_zaxpby(const ScamacVector * x, ScamacVector * y, const double complex *a, const double complex *b) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!y) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!a) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (!b) { return scamac_error_set_par(SCAMAC_ENULL,4); }
  if (!scamac_vector_check_compatibility(x,y)) { return SCAMAC_EINVALID; }

  ScamacInt nv = scamac_vector_query_nv(x);
  int i;
  for (i=0;i<x->m;i++) {
    cblas_zscal(nv, &b[i], scamac_vector_getptr(y,i),1);
    cblas_zaxpy(nv, &a[i], scamac_vector_getptr(x,i),1, scamac_vector_getptr(y,i),1);
  }
  return SCAMAC_EOK;
}


ScamacErrorCode scamac_svector_axpby (const ScamacVector * x, ScamacVector * y, double a, double b) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!y) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!scamac_vector_check_compatibility(x,y)) { return SCAMAC_EINVALID; }

  ScamacInt nv = scamac_vector_query_nv(x);
  int i;
  for (i=0;i<x->m;i++) {
    cblas_dscal(nv, b, scamac_vector_getptr(y,i),1);
    cblas_daxpy(nv, a, scamac_vector_getptr(x,i),1, scamac_vector_getptr(y,i),1);
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_svector_zaxpby(const ScamacVector * x, ScamacVector * y, double complex a, double complex b) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!y) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!scamac_vector_check_compatibility(x,y)) { return SCAMAC_EINPUT; }

  ScamacInt nv = scamac_vector_query_nv(x);
  int i;
  for (i=0;i<x->m;i++) {
    cblas_zscal(nv, &b, scamac_vector_getptr(y,i),1);
    cblas_zaxpy(nv, &a, scamac_vector_getptr(x,i),1, scamac_vector_getptr(y,i),1);
  }
  return SCAMAC_EOK;
}


ScamacErrorCode scamac_mvector_scal (ScamacVector * x, const double *s) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!s) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  
  ScamacInt nv = scamac_vector_query_nv(x);
  int i;
  for (i=0;i<x->m;i++) {
    cblas_dscal(nv, s[i], scamac_vector_getptr(x,i),1);
  }
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_mvector_zscal(ScamacVector * x, const double complex *s) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!s) { return scamac_error_set_par(SCAMAC_ENULL,2); }

  ScamacInt nv = scamac_vector_query_nv(x);
  int i;
  for (i=0;i<x->m;i++) {
    cblas_zscal(nv, &s[i], scamac_vector_getptr(x,i),1);
  }

  return SCAMAC_EOK;
}

ScamacErrorCode scamac_svector_scal (ScamacVector * x, double s) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_REAL) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  
  ScamacInt nv = scamac_vector_query_nv(x);
  int i;
  for (i=0;i<x->m;i++) {
    cblas_dscal(nv, s, scamac_vector_getptr(x,i),1);
  }
 
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_svector_zscal(ScamacVector * x, double complex s) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_COMPLEX) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  
  ScamacInt nv = scamac_vector_query_nv(x);
  int i;
  for (i=0;i<x->m;i++) {
    cblas_zscal(nv, &s, scamac_vector_getptr(x,i),1);
  }

  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_transform (const ScamacVector * x, const double * a, ScamacVector * y) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_REAL) { return  scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!a) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!y) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_REAL) { return  scamac_error_set_par(SCAMAC_EINPUT,3); }
  if (! (scamac_vector_query_nv(x) == scamac_vector_query_nv(y) )) { return SCAMAC_EINVALID; }
  if (!scamac_vector_check_exclusivity(x,y)) { return SCAMAC_EINVALID; }
  
  ScamacInt nv = scamac_vector_query_nv(x);
  int mx = scamac_vector_query_m(x);
  int my = scamac_vector_query_m(y);
  int i,j;
  for (i=0;i<my;i++) {
    cblas_dscal(nv, 0.0, scamac_vector_getptr(y,i),1);
    for (j=0;j<mx;j++) {
      cblas_daxpy(nv, a[j+mx*i], scamac_vector_getptr(x,j),1, scamac_vector_getptr(y,i),1);
    }
  }
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_ztransform(const ScamacVector * x, const double complex * a, ScamacVector * y) {
  if (!x) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (scamac_vector_query_valtype(x) != SCAMAC_VAL_COMPLEX) { return  scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!a) { return scamac_error_set_par(SCAMAC_ENULL,2); }
  if (!y) { return scamac_error_set_par(SCAMAC_ENULL,3); }
  if (scamac_vector_query_valtype(y) != SCAMAC_VAL_COMPLEX) { return  scamac_error_set_par(SCAMAC_EINPUT,3); }
  if (! (scamac_vector_query_nv(x) == scamac_vector_query_nv(y) )) { return SCAMAC_EINVALID; }
  if (!scamac_vector_check_exclusivity(x,y)) { return SCAMAC_EINVALID; }
  
  ScamacInt nv = scamac_vector_query_nv(x);
  int mx = scamac_vector_query_m(x);
  int my = scamac_vector_query_m(y);
  int i,j;
  for (i=0;i<my;i++) {
    double complex zero = 0.0;
    cblas_zscal(nv, &zero, scamac_vector_getptr(y,i),1);
    for (j=0;j<mx;j++) {
      cblas_zaxpy(nv, &a[j+mx*i], scamac_vector_getptr(x,j),1, scamac_vector_getptr(y,i),1);
    }
  }
  
  return SCAMAC_EOK;
}


ScamacErrorCode scamac_vector_check_orthogonality(const ScamacVector * v, double * err, double * errnrm, double * errperp) {
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  if (!scamac_vector_check(v)) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if ((!err) && (!errnrm) && (!errperp)) { return SCAMAC_ENULL; }

  ScamacInt nv = scamac_vector_query_nv(v);
  
  double enrm=0.0, eprp=0.0;


  if (scamac_vector_query_valtype(v)==SCAMAC_VAL_REAL) {
    int i,j;
    for (i=0;i<v->m;i++) {
      for (j=0;j<=i;j++) {
	double dot;
        dot = cblas_ddot(nv, scamac_vector_getptr(v,i),1, scamac_vector_getptr(v,j),1);
        if (i==j) {
          enrm=fmax(enrm,fabs(dot-1.0));
        } else {
          eprp=fmax(eprp,fabs(dot));
        }
      }
    }
  } else {
    int i,j;
    for (i=0;i<v->m;i++) {
      for (j=0;j<=i;j++) {
	double complex zdot;
        cblas_zdotc_sub(nv, scamac_vector_getptr(v,i),1, scamac_vector_getptr(v,j),1, &zdot);
        if (i==j) {
          enrm=fmax(enrm,cabs(zdot-1.0));
        } else {
          eprp=fmax(eprp,cabs(zdot));
        }
      }
    }
  }
  if (errnrm)  { *errnrm = enrm; }
  if (errperp) { *errperp = eprp; }
  if (err)     { *err = fmax(enrm,eprp); }
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_vector_print(const ScamacVector * v) {
  if (!v) { return scamac_error_set_par(SCAMAC_ENULL,1); }
  ScamacInt nv = scamac_vector_query_nv(v);
  int m = scamac_vector_query_m(v);
  if (scamac_vector_query_valtype(v)==SCAMAC_VAL_REAL) {
    int i,j;
    for (i=0;i<m;i++) {
      printf(" === %d ===\n",i);
      double * p = scamac_vector_getptr(v,i);
      for (j=0;j<nv;j++) {
	printf("%d : %e\n",j, p[j]);
      }
    }
  } else {
    int i,j;
    for (i=0;i<m;i++) {
      printf(" === %d ===\n",i);
      double complex * p = scamac_vector_getptr(v,i);
      for (j=0;j<nv;j++) {
	printf("%d : %e +I%e\n",j, creal(p[j]), cimag(p[j]));
      }
    }
  }
  return SCAMAC_EOK;
}
