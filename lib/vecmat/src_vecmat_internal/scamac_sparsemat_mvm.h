/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   October 2017 --- today
 *  \brief  Sparse matrix creation and manipulation
 *  \ingroup vecmat
 */

#ifndef SCAMAC_SPARSEMAT_MVM_H
#define SCAMAC_SPARSEMAT_MVM_H


#include "scamac_vector.h"
#include "scamac_sparsemat.h"
#include <complex.h>


/** \brief Sparse matrix-vector multiplication: y = alpha SM x + beta y + gamma x
 *  \ingroup vecmat
 *  \note There is little information in declaring *x "const" - it should simply be understood as a reminder that this vector view is not changed
 */
//ScamacErrorCode scamac_sparsemat_mvm(const ScamacMatrix *sm, const double *x, double *y, double alpha, double beta, double gamma);
ScamacErrorCode scamac_sparsemat_mvm(const ScamacMatrix *sm, const ScamacVector *x, ScamacVector *y, double alpha, double beta, double gamma);

/** \brief Complex sparse matrix-vector multiplication: y = alpha SM x + beta y + gamma x
 *  \ingroup vecmat
 *  \note Requires complex vectors x,y, but accepts real and complex matrix sm
 */
//ScamacErrorCode scamac_sparsemat_mvm_cplx(const ScamacMatrix *sm, const double complex *x, double complex *y,
//    double complex alpha, double complex beta, double complex gamma);
ScamacErrorCode scamac_sparsemat_mvm_cplx(const ScamacMatrix *sm, const ScamacVector *x, ScamacVector *y,
    double complex alpha, double complex beta, double complex gamma);

#endif /* SCAMAC_SPARSEMAT_MVM_H */
