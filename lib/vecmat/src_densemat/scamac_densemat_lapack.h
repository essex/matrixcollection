/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   June 2018 --- today
 *  \brief  Dense matrix manipulation [requires LAPACK]
 *  \ingroup vecmat
 *  \note A dense matrix is simply a pointer to memory. Reasoning: Dense matrices are, as far as we are concerned, always small.
 */

#ifndef SCAMAC_DENSEMAT_LAPACK_H
#define SCAMAC_DENSEMAT_LAPACK_H

#include "scamac_error.h"
#include "scamac_defs.h"
#include "scamac_inttypes.h"
#include <complex.h>

ScamacErrorCode scamac_densemat_symmetric_eigensystem(ScamacInt n, const double *dm, double *eig, double *eigvec);

#endif /* SCAMAC_DENSEMAT_LAPACK_H */
