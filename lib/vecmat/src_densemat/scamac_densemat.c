#include "scamac_densemat.h"
#include "scamac_safeint.h"
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

ScamacErrorCode scamac_densemat_alloc(ScamacInt nr, ScamacInt nc, ScamacValType valtype, void ** dm) {
  if (nr<1) { return scamac_error_set_par( SCAMAC_ERANGE, 1); }
  if (nc<1) { return scamac_error_set_par( SCAMAC_ERANGE, 2); }
  if ( (valtype != SCAMAC_VAL_REAL) && (valtype != SCAMAC_VAL_COMPLEX) ) { return scamac_error_set_par(SCAMAC_EINPUT, 3); }
  if (!dm) { return scamac_error_set_par(SCAMAC_ENULL, 4); }

  ScamacInt mult = scamac_safeint_mult( nr, nc );
  if (mult<0) { return SCAMAC_EOVERFLOW; }
  if (mult > SCAMAC_INT_MAX/2) { return SCAMAC_EHUGEINT; }

  if (valtype == SCAMAC_VAL_REAL) {
    *dm = malloc( mult * sizeof (double) );
  } else {
    *dm = malloc( 2 * mult * sizeof (double) );
  }

  if (!(*dm)) { return SCAMAC_EMALLOCFAIL; }
  
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_densemat_real_alloc(ScamacInt nr, ScamacInt nc, double ** dm) {
  return scamac_densemat_alloc(nr,nc, SCAMAC_VAL_REAL, (void **) dm);
}

ScamacErrorCode scamac_densemat_cplx_alloc(ScamacInt nr, ScamacInt nc, double complex ** dm) {
 return scamac_densemat_alloc(nr,nc, SCAMAC_VAL_COMPLEX, (void **) dm);
}

ScamacErrorCode scamac_densemat_identity(ScamacInt m, ScamacInt n, double *dm) {
  if (m<0) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (n<0) { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (!dm) { return scamac_error_set_par(SCAMAC_ENULL,3);  }
  ScamacInt i,j;
  for (j=0;j<n;j++) {
    for (i=0;i<m;i++) {
      if (i==j) {
	dm[i+m*j]=1.0;
      } else {
	dm[i+m*j]=0.0;
      }
    }
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_densemat_permute_columns(char dir, ScamacInt nr, ScamacInt nc, double *dm, ScamacInt *perm) {
  if (nr<=0)  { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (nc<=0)  { return scamac_error_set_par(SCAMAC_EINPUT,3); }
  if (!dm)   { return scamac_error_set_par(SCAMAC_ENULL,4);  }
  if (!perm) { return scamac_error_set_par(SCAMAC_ENULL,5);  }
  if ( (dir=='f') || (dir=='F') ) {
    for (int i=0;i<nc;i++) {
      perm[i]=-perm[i]-1;
    }
    for (int i=0;i<nc;i++) {
      if (perm[i]<0) {
	int j = i;
	perm[j]=-perm[j]-1;
	int in = perm[j];
	while (perm[in]<0) {
	  for (int ii=0;ii<nr;ii++) {
	    double temp = dm[ii+j*nr];
	    dm[ii+j*nr] = dm[ii+in*nr];
	    dm[ii+in*nr]= temp;
	  }
	  perm[in]=-perm[in]-1;
	  j = in;
	  in = perm[in];
	}
      }
    }
  } else  if ( (dir=='b') || (dir=='B') ) {
    for (int i=0;i<nc;i++) {
      perm[i]=-perm[i]-1;
    }
    for (int i=0;i<nc;i++) {
      if (perm[i]<0) {
	perm[i]=-perm[i]-1;
	int j = perm[i];
	while (j != i) {
	  for (int ii=0;ii<nr;ii++) {
	    double temp = dm[ii+i*nr];
	    dm[ii+i*nr] = dm[ii+j*nr];
	    dm[ii+j*nr] =  temp;
	  }
	  perm[j]=-perm[j]-1;
	  j=perm[j];
	}
      }
    }
  } else {
    return scamac_error_set_par(SCAMAC_EINPUT,1);
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_densemat_permute_rows   (char dir, ScamacInt nr, ScamacInt nc, double *dm, ScamacInt *perm) {
  if (nr<=0)  { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (nc<=0)  { return scamac_error_set_par(SCAMAC_EINPUT,3); }
  if (!dm)   { return scamac_error_set_par(SCAMAC_ENULL,4);  }
  if (!perm) { return scamac_error_set_par(SCAMAC_ENULL,5);  }
  if ( (dir=='f') || (dir=='F') ) {
    for (int i=0;i<nr;i++) {
      perm[i]=-perm[i]-1;
    }
    for (int i=0;i<nr;i++) {
      if (perm[i]<0) {
	int j = i;
	perm[j]=-perm[j]-1;
	int in = perm[j];
	while (perm[in]<0) {
	  for (int ii=0;ii<nc;ii++) {
	    double temp = dm[nr*ii+j];
	    dm[nr*ii+j] = dm[nr*ii+in];
	    dm[nr*ii+in]= temp;
	  }
	  perm[in]=-perm[in]-1;
	  j = in;
	  in = perm[in];
	}
      }
    }
  } else  if ( (dir=='b') || (dir=='B') ) {
    for (int i=0;i<nr;i++) {
      perm[i]=-perm[i]-1;
    }
    for (int i=0;i<nr;i++) {
      if (perm[i]<0) {
	perm[i]=-perm[i]-1;
	int j = perm[i];
	while (j != i) {
	  for (int ii=0;ii<nc;ii++) {
	    double temp = dm[nr*ii+i];
	    dm[nr*ii+i] = dm[nr*ii+j];
	    dm[nr*ii+j] =  temp;
	  }
	  perm[j]=-perm[j]-1;
	  j=perm[j];
	}
      }
    }
  } else {
    return scamac_error_set_par(SCAMAC_EINPUT,1);
  }
  return SCAMAC_EOK;
}


ScamacErrorCode scamac_densemat_cplx_permute_columns(char dir, ScamacInt nr, ScamacInt nc, double complex *dm, ScamacInt *perm) {
  if (nr<=0)  { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (nc<=0)  { return scamac_error_set_par(SCAMAC_EINPUT,3); }
  if (!dm)   { return scamac_error_set_par(SCAMAC_ENULL,4);  }
  if (!perm) { return scamac_error_set_par(SCAMAC_ENULL,5);  }
  if ( (dir=='f') || (dir=='F') ) {
    for (int i=0;i<nc;i++) {
      perm[i]=-perm[i]-1;
    }
    for (int i=0;i<nc;i++) {
      if (perm[i]<0) {
	int j = i;
	perm[j]=-perm[j]-1;
	int in = perm[j];
	while (perm[in]<0) {
	  for (int ii=0;ii<nr;ii++) {
	    double complex temp = dm[ii+j*nr];
	    dm[ii+j*nr] = dm[ii+in*nr];
	    dm[ii+in*nr]= temp;
	  }
	  perm[in]=-perm[in]-1;
	  j = in;
	  in = perm[in];
	}
      }
    }
  } else  if ( (dir=='b') || (dir=='B') ) {
    for (int i=0;i<nc;i++) {
      perm[i]=-perm[i]-1;
    }
    for (int i=0;i<nc;i++) {
      if (perm[i]<0) {
	perm[i]=-perm[i]-1;
	int j = perm[i];
	while (j != i) {
	  for (int ii=0;ii<nr;ii++) {
	    double complex temp = dm[ii+i*nr];
	    dm[ii+i*nr] = dm[ii+j*nr];
	    dm[ii+j*nr] =  temp;
	  }
	  perm[j]=-perm[j]-1;
	  j=perm[j];
	}
      }
    }
  } else {
    return scamac_error_set_par(SCAMAC_EINPUT,1);
  }
  return SCAMAC_EOK;
}

ScamacErrorCode scamac_densemat_cplx_permute_rows   (char dir, ScamacInt nr, ScamacInt nc, double complex *dm, ScamacInt *perm) {
  if (nr<=0)  { return scamac_error_set_par(SCAMAC_EINPUT,2); }
  if (nc<=0)  { return scamac_error_set_par(SCAMAC_EINPUT,3); }
  if (!dm)   { return scamac_error_set_par(SCAMAC_ENULL,4);  }
  if (!perm) { return scamac_error_set_par(SCAMAC_ENULL,5);  }
  if ( (dir=='f') || (dir=='F') ) {
    for (int i=0;i<nr;i++) {
      perm[i]=-perm[i]-1;
    }
    for (int i=0;i<nr;i++) {
      if (perm[i]<0) {
	int j = i;
	perm[j]=-perm[j]-1;
	int in = perm[j];
	while (perm[in]<0) {
	  for (int ii=0;ii<nc;ii++) {
	    double complex temp = dm[nr*ii+j];
	    dm[nr*ii+j] = dm[nr*ii+in];
	    dm[nr*ii+in]= temp;
	  }
	  perm[in]=-perm[in]-1;
	  j = in;
	  in = perm[in];
	}
      }
    }
  } else  if ( (dir=='b') || (dir=='B') ) {
    for (int i=0;i<nr;i++) {
      perm[i]=-perm[i]-1;
    }
    for (int i=0;i<nr;i++) {
      if (perm[i]<0) {
	perm[i]=-perm[i]-1;
	int j = perm[i];
	while (j != i) {
	  for (int ii=0;ii<nc;ii++) {
	    double complex temp = dm[nr*ii+i];
	    dm[nr*ii+i] = dm[nr*ii+j];
	    dm[nr*ii+j] =  temp;
	  }
	  perm[j]=-perm[j]-1;
	  j=perm[j];
	}
      }
    }
  } else {
    return scamac_error_set_par(SCAMAC_EINPUT,1);
  }
  return SCAMAC_EOK;
}
