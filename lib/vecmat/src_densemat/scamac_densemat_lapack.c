#include "scamac_densemat_lapack.h"
#include "scamac_cblas.h"
#include "scamac_clapack.h"
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

ScamacErrorCode scamac_densemat_symmetric_eigensystem(ScamacInt n, const double *dm, double *eig, double *eigvec) {
  if (n<=0) { return scamac_error_set_par(SCAMAC_EINPUT,1); }
  if (!dm) { return scamac_error_set_par(SCAMAC_ENULL,2);  }
  if ( (!eig) && (!eigvec) ) { return SCAMAC_EOK; }

  double * mat = NULL;
  double * w = NULL;

  if (!eig) {
    w = malloc(n * sizeof *w);
    if (!w) { return SCAMAC_EMALLOCFAIL; }
  } else {
    w = eig;
  }
  if (!eigvec) {
    eigvec = malloc(n*n * sizeof *eigvec);
    if (!eigvec) { return SCAMAC_EMALLOCFAIL; }
  } else {
    mat = eigvec;
  }

  cblas_dcopy(n*n, dm,1, mat,1);
  
  char jobz;
  if (eigvec) {
    jobz='V';
  } else {
    jobz='N';
  }
  char uplo='U';

  int nn = n;
  
  double *work=NULL;
  double wwork;
  int lwork;
  int lapinfo;


  lwork=-1;
  dsyev_(&jobz,&uplo,&nn,mat,&nn,w,&wwork,&lwork,&lapinfo);
  if (lapinfo) {
    return (SCAMAC_EFAIL | SCAMAC_EINTERNAL);
  }
  lwork=wwork+0.1;
  work=malloc(lwork * sizeof *work);
  if (!work) {
    return SCAMAC_EMALLOCFAIL;
  }
  dsyev_(&jobz,&uplo,&nn,mat,&nn,w,work,&lwork,&lapinfo);
  if (lapinfo) {
    return (SCAMAC_EFAIL | SCAMAC_EINTERNAL);
  }

  free(work);
  if (!eig)    { free(w); }
  if (!eigvec) { free(mat); }

  return SCAMAC_EOK;
  
}

