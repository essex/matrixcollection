/** \file
 *  \author Andreas Alvermann (University of Greifswald)
 *  \date   June 2018 --- today
 *  \brief  Dense matrix manipulation
 *  \ingroup vecmat
 *  \note A dense matrix is simply a pointer to memory. Reasoning: Dense matrices are, as far as we are concerned, always small.
 */

#ifndef SCAMAC_DENSEMAT_H
#define SCAMAC_DENSEMAT_H

#include "scamac_error.h"
#include "scamac_defs.h"
#include "scamac_inttypes.h"
#include <complex.h>


/** \brief Allocate dense matrix
 *  \ingroup vecmat
 *  \note 0 < nr * nc < SCAMAC_INT_MAX/2
 */
ScamacErrorCode scamac_densemat_alloc(ScamacInt nr, ScamacInt nc, ScamacValType valtype, void ** dm);

// wrappers
ScamacErrorCode scamac_densemat_real_alloc(ScamacInt nr, ScamacInt nc, double ** dm);
ScamacErrorCode scamac_densemat_cplx_alloc(ScamacInt nr, ScamacInt nc, double complex ** dm);

ScamacErrorCode scamac_densemat_identity(ScamacInt m, ScamacInt n, double *dm);

/**
 *   dir = 'f/F'orward, 'b/B'ackward
 */
/**
 *    perm: vector nc
 */
ScamacErrorCode scamac_densemat_permute_columns(char dir, ScamacInt nr, ScamacInt nc, double *dm, ScamacInt *perm);
/**
 *    perm: vector nr
 */
ScamacErrorCode scamac_densemat_permute_rows   (char dir, ScamacInt nr, ScamacInt nc, double *dm, ScamacInt *perm);

ScamacErrorCode scamac_densemat_cplx_permute_columns(char dir, ScamacInt nr, ScamacInt nc, double complex *dm, ScamacInt *perm);
/**
 *    perm: vector nr
 */
ScamacErrorCode scamac_densemat_cplx_permute_rows   (char dir, ScamacInt nr, ScamacInt nc, double complex *dm, ScamacInt *perm);

#endif /* SCAMAC_DENSEMAT_H */
