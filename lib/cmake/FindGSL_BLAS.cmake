
include(CheckLibraryExists)
include(CheckIncludeFiles)

message(STATUS "Try to find GSL (C)BLAS")
find_package(GSL QUIET)
if (DEFINED GSL_CONFIG_EXECUTABLE)
  mark_as_advanced(GSL_CONFIG_EXECUTABLE)
endif()

if (GSL_FOUND)
  set(GSL_BLAS_LIBRARIES ${GSL_CBLAS_LIBRARY})
  set(GSL_BLAS_INCLUDE_DIR ${GSL_INCLUDE_DIR})
  set(GSL_BLAS_HEADER "gsl/gsl_cblas.h")
  set(GSL_BLAS_FOUND TRUE)

  CHECK_LIBRARY_EXISTS(${GSL_BLAS_LIBRARIES} cblas_ddot "" GSL_BLAS_LIB_OK)
  if (NOT GSL_BLAS_LIB_OK)
    set(GSL_BLAS_FOUND FALSE)
    message(WARNING "---- GSL (C)BLAS library is not OK")
  endif()
  CHECK_INCLUDE_FILES(${GSL_BLAS_INCLUDE_DIR}/${GSL_BLAS_HEADER} GSL_BLAS_HEADER_OK) 
  if (NOT GSL_BLAS_HEADER_OK)
    set(GSL_BLAS_FOUND FALSE)
    message(WARNING "---- GSL (C)BLAS header gsl/gsl_cblas.h is not OK")
  endif()
else()
  message(STATUS "... GSL not found. Hint: Try to set GSL_ROOT_DIR.")
  set(GSL_BLAS_FOUND FALSE)
endif()


if (GSL_BLAS_FOUND)
  message(STATUS "Try to find GSL (C)BLAS -- found")
  else()
  message(STATUS "Try to find GSL (C)BLAS -- not found")
endif()

