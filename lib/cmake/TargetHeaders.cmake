include(CMakeParseArguments)

macro (target_headers TARGET)
  if ("${TARGET}" STREQUAL "")
    message(FATAL_ERROR "target_headers: expect non-empty target")
  elseif (${TARGET} STREQUAL "PUBLIC" OR ${TARGET} STREQUAL "PRIVATE")
    message(FATAL_ERROR "target_headers: must supply a valid target")
  endif()
  if (NOT TARGET "${TARGET}")
    message(FATAL_ERROR "target_headers: non-valid target >${TARGET}<")
  endif()

  cmake_parse_arguments(MY_ARGS "" ""
                          "PUBLIC;PRIVATE" ${ARGN} )


get_target_property(_my_1 ${TARGET} PUBLIC_HEADER)
if (NOT _my_1)
  set(_my_1)
endif()
get_target_property(_my_2 ${TARGET} PRIVATE_HEADER)
if (NOT _my_2)
  set(_my_2)
endif()

LIST(APPEND _my_1 "${MY_ARGS_PUBLIC}")
LIST(APPEND _my_2 "${MY_ARGS_PRIVATE}")

set_target_properties(${TARGET} PROPERTIES PUBLIC_HEADER "${_my_1}" PRIVATE_HEADER "${_my_2}")

endmacro()
