macro(_expand LIBS LIBS_EXPANDED REQS)
  set(_libs "${LIBS}")
  set(_reqs)
  set(_cond TRUE)
  while(_cond)
   set(_cond FALSE)
   set(__libs)
   foreach(x IN LISTS _libs)
     string(REGEX REPLACE "\\$<LINK_ONLY:(.*)>" "\\1" x "${x}")
     if (TARGET "${x}")
       get_target_property(y "${x}" INTERFACE_LINK_LIBRARIES)
       if (NOT "${y}" MATCHES ".*NOTFOUND$")
         LIST(APPEND __libs "${y}")
         set(_cond TRUE)
       endif()
       get_target_property(_is_imported "${x}" IMPORTED)
       if (_is_imported)
         get_target_property(y "${x}" LOCATION)
         if (NOT "${y}" MATCHES ".*NOTFOUND$")
           LIST(APPEND __libs "${y}")
         endif()
       else()
         LIST(APPEND _reqs "${x}")
       endif()
     else()
       LIST(APPEND __libs "${x}")
     endif()
   endforeach(x)
   set(_libs "${__libs}")
  endwhile()
  set(${LIBS_EXPANDED} "${_libs}")
  set(${REQS} "${_reqs}")
endmacro()


macro(_libs_to_str _LIBS _LIBSTR)
set(__paths)
set(__names)
foreach(x IN LISTS "${_LIBS}")
  get_filename_component(xp "${x}" PATH)
  get_filename_component(xn "${x}" NAME)
  if ("${xn}" MATCHES "lib(.*)\\.so")
    string(REGEX REPLACE "lib(.*)\\.so" "-l\\1" xn "${xn}")
  elseif ("${xn}" MATCHES "lib(.*)\\.dylib")
    string(REGEX REPLACE "lib(.*)\\.dylib" "-l\\1" xn "${xn}")
  elseif ("${xn}" MATCHES "(.*)\\.framework")
    string(REGEX REPLACE "(.*)\\.framework" "-framework \\1" xn "${xn}")
  else()
    string(CONCAT xn "-l" "${xn}")
  endif()
  if (xp)
    LIST(APPEND __paths "${xp}")
  endif()
  if (xn)
    LIST(APPEND __names "${xn}")
  endif()
endforeach()
if (__paths)
  LIST(REMOVE_DUPLICATES __paths)
endif()
if (__names)
  LIST(REMOVE_DUPLICATES __names)
endif()
  set("${_LIBSTR}" "")
  foreach(x IN LISTS __paths)
    string(CONCAT "${_LIBSTR}" "${${_LIBSTR}}" " -L" "${x}")
  endforeach(x)
  foreach(x IN LISTS __names)
    string(CONCAT "${_LIBSTR}" "${${_LIBSTR}}" " " "${x}")
  endforeach(x)
endmacro()

macro(_reqs_to_str _REQS _REQSTR)
  set("${_REQSTR}" "")
  foreach(x IN LISTS ${_REQS})
    string(CONCAT "${_REQSTR}" "${${_REQSTR}}" " " "${x}")
  endforeach(x)
endmacro()

macro(PkgCfg_SetVar TARGET)
  if (NOT TARGET "${TARGET}")
    message(FATAL_ERROR "PkgCfg_SetVar: ${TARGET} is not a target")
  endif()
  get_target_property(_type "${TARGET}" TYPE)

  get_target_property(_foo "${TARGET}" LINK_LIBRARIES)  
  _expand("${_foo}" libs reqs)
  _libs_to_str(libs _libs_str)
  _reqs_to_str(reqs _reqs_str)
  
  if ("${_type}" STREQUAL "STATIC_LIBRARY")
    set(PKG_LIBS_PRIVATE "")
    set(PKG_LIBS "${_libs_str}")
    set(PKG_REQUIRES "${_reqs_str}")
    set(PKG_REQUIRES_PRIVATE "")
  elseif("${_type}" STREQUAL "SHARED_LIBRARY")
    set(PKG_LIBS "")
    set(PKG_LIBS_PRIVATE "${_libs_str}")
    set(PKG_REQUIRES_PRIVATE "${_reqs_str}")
    set(PKG_REQUIRES "")
  else()
    message(FATAL_ERROR "PkgCfg_SetVar: ${TARGET} is neither a STATIC nor SHARED library")
  endif()
  
endmacro()


