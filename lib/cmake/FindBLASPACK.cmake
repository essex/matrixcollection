### - Find GENERIC implementation of BLAS & LAPACK



message(STATUS "Try to find generic BLAS & LAPACK ...")

set(BLASPACK_BLAS_FOUND TRUE)

find_package(BLAS QUIET)
if (BLAS_FOUND)
  set(BLASPACK_BLAS_LIBRARIES ${BLAS_LIBRARIES} CACHE STRING "BLAS libraries")
else()
  message(STATUS "... (C)BLAS library not found")
  set(BLASPACK_BLAS_FOUND FALSE)
endif()

set(BLASPACK_BLAS_HEADER "cblas.h" CACHE STRING "BLAS header")
if (BLASPACK_BLAS_HEADER) 
  find_path(BLASPACK_BLAS_INCLUDE_DIR ${BLASPACK_BLAS_HEADER} HINTS ${BLASPACK_BLAS_LIBRARIES}/*)
  if (BLASPACK_BLAS_INCLUDE_DIR) 
    set(BLASPACK_BLAS_INCLUDE_DIR ${BLASPACK_BLAS_INCLUDE_DIR} CACHE STRING "(C)BLAS include dir")
  else()
    message(STATUS "... (C)BLAS header not found")
    set(BLASPACK_BLAS_FOUND FALSE)
  endif()
endif()

if (BLASPACK_BLAS_FOUND)
  message(STATUS "... BLAS found")
else()
  message(STATUS "... BLAS not found")
endif()

find_package(LAPACK QUIET)
if (LAPACK_FOUND)
  set(BLASPACK_LAPACK_LIBRARIES ${LAPACK_LIBRARIES} CACHE STRING "LAPACK libraries")
  set(BLASPACK_LAPACK_FOUND TRUE)
else()
  set(BLASPACK_LAPACK_FOUND FALSE)
endif()

if (BLASPACK_LAPACK_FOUND)
  message(STATUS "... LAPACK found")
else()
  message(STATUS "... LAPACK not found")
endif()


mark_as_advanced(BLASPACK_BLAS_LIBRARIES BLASPACK_BLAS_HEADER BLASPACK_BLAS_INCLUDE_DIR BLASPACK_LAPACK_LIBRARIES)
