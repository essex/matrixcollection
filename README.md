ScaMaC - a Scalable Matrix Collection
=====================================

ScaMaC is part of the ESSEX (https://blogs.fau.de/essex/) project of the DFG
priority programme SPPEXA (http://www.sppexa.de).
The purpose of ScaMaC is to collect relevant application and benchmark matrices, including those used within ESSEX for algorithm and code development.
ScaMaC allows for serial and parallel generation of application and benchmark matrices of *scalable size*, and thus complements existing collections of fixed-size matrices.

ScaMaC has two components:
The ScaMaC library for matrix generation,
and the ScaMaC toolkit for exploration of the ScaMaC matrices.

Status
------

The current ScaMaC version (0.8.2) is still a development version.
Everything included in this version should work as expected,
but no guarantee is given and no responsibility assumed.

At present, many matrix examples are still missing, the
toolkit offers only basic functionality, and the documentation
remains incomplete.

The next ScaMaC version (0.9) is scheduled for the second half of 2019.
Until then, revisions (0.8.x) may appear sporadically in the ScaMaC repository (https://bitbucket.org/essex/matrixcollection).

Dependencies
------------

General build dependencies:

- CMake (version >= 3.0 [probably])
- C compiler (C99 standard at least, C11 for some examples)
  
ScaMaC toolkit & toolkit application ("scamact"):

- BLAS & LAPACK [optional]
- libpng [optional]

Installation
------------

1. Put the source files in a directory "FOO", e.g., clone the repository.  
   `$ mkdir FOO`  
   `$ git clone git@bitbucket.org:essex/matrixcollection.git FOO`

2. We recommend out-of-source builds, e.g. in a sub-directory of "FOO".  
   `$ cd FOO`  
   `$ mkdir build`  
   `$ cd build`  
   `$ cmake ../lib`  
   `$ make install`  
   `$ make test`

   By default, only the ScaMaC library is built.
   The default installation directory is "build/scamac".

3. The CMake build script accepts various options, which can be passed on the
   command line (via "cmake -D...=...") or manipulated interactively with the
   "ccmake" editor.

   The most important choices are:
   
   * build the toolkit + toolkit-application in addition to the library:  
   `$ cmake -DSCAMAC_BUILD=FULL ......`

   * Use 32 bit instead of 64 bit indices:  
   `$ cmake -DSCAMAC_INDEX=32 ......`

   * Build a shared library:  
   `$ cmake -DSCAMAC_LIB_TYPE=SHARED ......`

   Further options for the ScaMaC toolkit build include:
  
   * SCAMAC_WHICH_BLAS   [NONE/GENERIC/MKL/GSL]
   * SCAMAC_WHICH_MATOP  [NONE/INTERNAL/GHOST/PHIST/PETSc/TRILINOS]
   * SCAMAC_WHICH_OPENMP [NONE/DEFAULT]
   * SCAMAC_WHICH_PNG    [NONE/LIBPNG]

These options are configured automatically by the CMake build script, but manual adjustment may be required occasionally. The possible values of each option depend on the build environment, especially the availability of external libraries.
For example to use Intel MKL (BLAS & LAPACK), it is sufficient to set the environment variable MKLROOT appropriately.

A ScaMaC installation in directory "scamac/", e.g. in the default "build/scamac" location in the build directory, includes the following files:

  * scamac/include/scamac.h --- the top-level ScaMaC library header
  * scamac/include/scamactools.h --- the top-level ScaMaC toolkit header
  * scamac/lib/libscamac.*  --- the ScaMaC library
  * scamac/lib/libscamactools.* --- the ScaMaC toolkit
  * scamac/lib/pkgconfig/ --- .pc files for pkg-config
  * scamac/lib/cmake/ --- scamac-config.cmake file for CMake
  * scamac/bin/scamact --- the ScaMaC toolkit application

Documentation
-------------

Documentation (HTML/doxygen) is located at "docs_html/index.html" in the source directory, and also available [online](https://alvbit.bitbucket.io/scamac_docs/).

Examples
--------

Examples are found in "examples/".

Compilation is possible either with the CMakeLists.txt or makefile in the respective sub-directory.  
In both cases, the ScaMaC installation directory has to be specified in the SCAMAC_DIR variable, as in
```
$ cmake -DSCAMAC_DIR=FOO/build/scamac ......
$ SCAMAC_DIR=FOO/build/scamac make ......
```

Alternatively, compiler and linker flags can be obtained with pkg-config,
after having adjusted the PKG_CONFIG_PATH:
```
$ export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:path-to-scamac/lib/pkgconfig
$ pkg-config --cflags --libs scamac
```

Using only the ScaMaC library, compilation requires little more than  
`$ cc -I$(SCAMAC_INCDIR) -o mwe mwe.c -L$(SCAMAC_LIBDIR) -lscamac -lm`

### Basic examples (`examples/library/`)

Basic usage of the ScaMaC library functions for matrix generation.

* `ex_scale` : Scale up the dimension of a matrix by changing matrix parameters.
             Does not (need to) generate matrix rows. 

* `ex_count` : Serial matrix generation, row by row.  
             Counts the number of non-zeros matrix entries,
	     and computes the entrywise max-norm and row-sum norm. Expects a matrix argument string on the command line, e.g.  
	     `$ ./ex_count Anderson,Lx=200,Ly=200,Lz=200,ranpot=1.3`
	     
* `ex_count_openmp` : Parallel matrix generation with OpenMP, row by row.
                    Otherwise, identical to "ex_count".

### OpenMP

See `ex_count_openmp` in `examples/library/`

### MPI (`examples/MPI/`)

Basic usage of the ScaMaC library functions for matrix generation in an MPI program.

* `ex_count_mpi` : Parallel matrix generation with MPI, row by row.
  		   Otherwise, identical to "ex_count" or "ex_count_openmp".

### PETSc/SLEPc (`examples/PETSc_SLEPC/`)

Integration with SLEPc eigensolvers -- compute an eigenvalue.  
To compile these examples, the environment variables PETSC_DIR, PETSC_ARCH, SLEPC_DIR have to be set according to your PETSc/SLEPc installation.

* `ex_slepc` : Compute an eigenvalue for the matrix specified by a
  	     matrix argument string on the command line.  
	     `$ ./ex_slepc Anderson,Lx=200,Ly=200,Lz=200,ranpot=1.3 -eps_smallest_magnitude`

### ScaMaC toolkit (`examples/toolkit/`)

Empty.

Toolkit application
-------------------

The ScaMaC toolkit application "scamact" (beware of incorrect hyphenation!)
provides several options for exploration of the ScaMaC matrices.

By default, the toolkit application is installed in "scamac/bin/" in the build directory.

For example, try the following:

`$ scamact`  
  Print a usage info.
  Only available commands are listed (this depends on the ScaMaC build -- e.g. the "plot" command requires "libpng" during compilation).
  
`$ scamact list`  
  List the available matrices.
  This list contains, among other examples, the matrix "Hubbard".

`$ scamact info Hubbard`  
  Print information about the matrix "Hubbard", including a list of the matrix parameters.
  The list of parameters contains "n_sites" and "n_fermions", which take integer values.

`$ scamact query Hubbard`  
  Query information about the matrix "Hubbard" for given parameter values, including the matrix dimension.
  Every parameter has a default value. To set a parameter, we use the standard syntax of ScaMaC argument strings:

`$ scamact query Hubbard,n_sites=12`  
`$ scamact query Hubbard,n_sites=12,n_fermions=6`  
  Evidently, the matrix dimension depends on n_sites and n_fermions.
    
Some parameter values do not work for different reasons, which are reported by "scamact":  
`$ scamact query Hubbard,n_sites=0`  
`$ scamact query Hubbard,n_sites=10,n_fermions=20`  
This should work (for 64 bit ScaMaC indices):  
`$ scamact query Hubbard,n_sites=30,n_fermions=15`  
This should not work:  
`$ scamact query Hubbard,n_sites=32,n_fermions=16`  

The above calls do not actually generate any matrix rows.
The following calls generate the entire matrix, row by row.

`$ scamact stat Hubbard`  
  Print pattern and value statistics.

`$ scamact plot Hubbard`  
  Plot the sparsity pattern ("scamac.pattern.png").

The above calls do not store the generated matrix.
The following calls have to keep the entire (sparse) matrix in memory.

`$ scamact lanczos Hubbard`  
  Estimate the minimal and maximal eigenvalue [only symmetric matrices].

`$ scamact spectrum Hubbard`  
  Compute the entire spectrum.
  This requires that the entire matrix is kept as a dense (!) matrix in memory,
  and is therefore restricted to very small matrix sizes.
  
`$ scamact output mm Hubbard`  
  Output matrix in MatrixMarket [mm] format.


License
-------

ScaMaC is relased under a modified BSD license (see the "LICENSE.txt" file).
