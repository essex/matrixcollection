#include "scamac.h"

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>

#ifndef MAX
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif

/**  examples/MPI/ex_count_mpi.c
 *
 *   basic example:
 *   - read a matrix name/argument string from the command line
 *   - count the number of non-zeros, and compute the maximum norm (=max |entry|) and row-sum norm
 *
 *   Matrix rows are generated in parallel MPI processes.
 *   The ScamacGenerator and ScamacWorkspace is allocated per process.
 */


/* helper function:
 * split integer range [a...b-1] in n nearly equally sized pieces [ia...ib-1], for i=0,...,n-1 */
void split_range(ScamacIdx a, ScamacIdx b, ScamacIdx n, ScamacIdx i, ScamacIdx *ia, ScamacIdx *ib) {
  ScamacIdx m = (b-a-1)/n + 1;
  ScamacIdx d = n-(n*m -(b-a));
  if (i < d) {
    *ia = m*i + a;
    *ib = m*(i+1) + a;
  } else {
    *ia = m*d + (i-d)*(m-1) + a;
    *ib = m*d + (i-d+1)*(m-1) + a;
  }
}

/* our MPI error handler */
void my_mpi_error_handler() {
  fflush(stdout); // get the message out
  MPI_Abort(MPI_COMM_WORLD, 1);
}

int main(int argc, char *argv[]) {

  int mpi_world_size, mpi_rank;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

  char *matargstr = NULL;
  if (argc<=1) {
    printf("usage: ex_count <matrix-argument string>\n\nexample: ex_count Hubbard\n         ex_count Hubbard,n_sites=14,n_fermions=8,U=1.3\n         ex_count TridiagonalReal,subdiag=0.5,supdiag=2\n");
    my_mpi_error_handler();
  }
  matargstr=argv[1];

  ScamacErrorCode err;
  ScamacGenerator *my_gen;
  char *errstr = NULL;
    
  // set error handler for MPI (the only global ScaMaC variable!)
  scamac_error_handler = my_mpi_error_handler;

  /* parse matrix name & parameters from command line to obtain a ScamacGenerator ... */
  /* an identical generator is created per MPI process */
  err = scamac_parse_argstr(matargstr, &my_gen, &errstr);
  /* ... and check for errors */
  if (err) {
    printf("-- Problem with matrix-argument string:\n-- %s\n---> Abort.\n",errstr);
    my_mpi_error_handler();
  }
  
  /* check matrix parameters */
  err = scamac_generator_check(my_gen, &errstr);
  if (err) {
    printf("-- Problem with matrix parameters:\n-- %s---> Abort.\n",errstr);
    my_mpi_error_handler();
  }
  
  /* finalize the generator ... */
  err=scamac_generator_finalize(my_gen);
  /* ... and check, whether the matrix dimension is too large */
  if (err==SCAMAC_EOVERFLOW) {
    printf("-- matrix dimension exceeds max. IDX value (%"SCAMACPRIDX")\n---> Abort.\n",SCAMAC_IDX_MAX);
    my_mpi_error_handler();
  }
  /* catch remaining errors */
  SCAMAC_CHKERR(err);
  
  /* query number of rows and max. number of non-zero entries per row */
  ScamacIdx nrow = scamac_generator_query_nrow(my_gen);
  ScamacIdx maxnzrow = scamac_generator_query_maxnzrow(my_gen);

  double t1 = MPI_Wtime();

  /* ScamacWorkspace is allocated per MPI process */
  ScamacWorkspace * my_ws;
  SCAMAC_TRY(scamac_workspace_alloc(my_gen, &my_ws));

  /* allocate memory for column indices and values per MPI process*/
  ScamacIdx *cind = malloc(maxnzrow * sizeof *cind);
  double *val;
  if (scamac_generator_query_valtype(my_gen) == SCAMAC_VAL_REAL) {
    val = malloc(maxnzrow * sizeof *val);
  } else {
    /* valtype == SCAMAC_VAL_COMPLEX */
    val = malloc(2*maxnzrow * sizeof *val);
  }

  ScamacIdx ia,ib;
  // this MPI process generates rows ia ... ib-1
  split_range(0,nrow, mpi_world_size, mpi_rank, &ia, &ib);

  /* local variables per MPI process */
  ScamacIdx my_nrow = 0;
  ScamacIdx my_nz = 0;
  double my_maxnrm=0.0;
  double my_rownrm=0.0;
  
  for (ScamacIdx idx=ia; idx<ib; idx++) {
    ScamacIdx k;
    /* generate single row ... */
    SCAMAC_TRY(scamac_generate_row(my_gen, my_ws, idx, SCAMAC_DEFAULT, &k, cind, val));
    /* ... which has 0 <=k <= maxnzrow entries */
    my_nz += k;
    my_nrow++;
    /* update maximum and row-sum norm */
    double locmaxnrm=0.0, locrownrm=0.0;
    if (scamac_generator_query_valtype(my_gen) == SCAMAC_VAL_REAL) {
      for (ScamacIdx i=0;i<k;i++) {
	locmaxnrm=fmax(locmaxnrm,fabs(val[i]));
	locrownrm=locrownrm+fabs(val[i]);
      }
    } else {
      double complex *cval = (double complex *) val;
      for (ScamacIdx i=0;i<k;i++) {
	locmaxnrm=fmax(locmaxnrm,cabs(cval[i]));
	locrownrm=locrownrm+cabs(cval[i]);
      }
    }
    my_maxnrm=fmax(my_maxnrm,locmaxnrm);
    my_rownrm=fmax(my_rownrm,locrownrm);
  }
  
  /* free local objects */
  free(cind);
  free(val);
  SCAMAC_TRY(scamac_workspace_free(my_ws));
  SCAMAC_TRY(scamac_generator_destroy(my_gen));

  double t2 = MPI_Wtime();
  
  double dt = t2-t1;
    
  /* gather results from all MPI processes*/

  ScamacIdx * all_nrow = NULL;
  ScamacIdx * all_nz   = NULL;
  double * all_maxnrm  = NULL;
  double * all_rownrm  = NULL;
  double * all_dt      = NULL;
 
  if (mpi_rank==0) {
    all_nrow   = malloc(mpi_world_size * sizeof * all_nrow   );        
    all_nz     = malloc(mpi_world_size * sizeof * all_nz     );
    all_maxnrm = malloc(mpi_world_size * sizeof * all_maxnrm );
    all_rownrm = malloc(mpi_world_size * sizeof * all_rownrm );
    all_dt     = malloc(mpi_world_size * sizeof * all_dt     );
  }
#ifdef SCAMAC_INDEX_TYPE_int64
  MPI_Gather( &my_nrow,   1, MPI_INT64_T, all_nrow,     1, MPI_INT64_T, 0, MPI_COMM_WORLD);
  MPI_Gather( &my_nz,     1, MPI_INT64_T, all_nz,       1, MPI_INT64_T, 0, MPI_COMM_WORLD);
#elif defined SCAMAC_INDEX_TYPE_int32
  MPI_Gather( &my_nrow,   1, MPI_INT32_T, all_nrow,     1, MPI_INT32_T, 0, MPI_COMM_WORLD);
  MPI_Gather( &my_nz,     1, MPI_INT32_T, all_nz,       1, MPI_INT32_T, 0, MPI_COMM_WORLD);
#endif
  MPI_Gather( &my_maxnrm, 1, MPI_DOUBLE,  all_maxnrm,   1, MPI_DOUBLE,  0, MPI_COMM_WORLD);
  MPI_Gather( &my_rownrm, 1, MPI_DOUBLE,  all_rownrm,   1, MPI_DOUBLE,  0, MPI_COMM_WORLD);
  MPI_Gather( &dt,        1, MPI_DOUBLE,  all_dt,       1, MPI_DOUBLE,  0, MPI_COMM_WORLD);

  /* accumulate results and output */  
  if (mpi_rank==0) {
    
    /* for statistics: max./min. number of rows generated, and time elapsed, per process */
    ScamacIdx min_nrow, max_nrow;
    double min_elapsedt, max_elapsedt;
      
    /* result variables */
    double maxnrm, rownrm;
    ScamacIdx n_nz;
    
    /* total number of rows actually generated */
    ScamacIdx tot_nrow = 0;

    
    /* rank 0 */
    n_nz += all_nz[0];
    maxnrm=all_maxnrm[0];
    rownrm=all_rownrm[0];
    
    min_nrow=all_nrow[0];
    max_nrow=all_nrow[0];
    min_elapsedt=all_dt[0];
    max_elapsedt=all_dt[0];

    tot_nrow=all_nrow[0];

    /* ranks > 0 */
    int irank;
    for (irank=1;irank<mpi_world_size;irank++) {
      /* accumulate results */
      n_nz += all_nz[irank];
      maxnrm=fmax(maxnrm,all_maxnrm[irank]);
      rownrm=fmax(rownrm,all_rownrm[irank]);

      /* accumulate statistics */
      min_nrow=MIN(min_nrow, all_nrow[irank]);
      max_nrow=MAX(max_nrow, all_nrow[irank]);
      min_elapsedt=fmin(min_elapsedt, all_dt[irank]);
      max_elapsedt=fmax(max_elapsedt, all_dt[irank]);

      tot_nrow+=all_nrow[irank];
    }

    printf("= matrix =================================================\n\n  %s\n",matargstr);
    printf("\n= results ================================================\n\n  Generated %"SCAMACPRIDX" rows with %"SCAMACPRIDX" non-zeros\n  max. matrix entry: %g\n  row-sum norm:      %g\n",  nrow, n_nz,maxnrm,rownrm);
    
    printf("\n= statistics =============================================\n\n  no. of processes:      %d\n  min. #rows in process: %"SCAMACPRIDX"\n  max. #rows in process: %"SCAMACPRIDX"\n  min. time in process:  %g\n  max. time in process:  %g\n  rows/sec:              %g\n==========================================================\n",mpi_world_size,min_nrow,max_nrow,min_elapsedt,max_elapsedt,((double) nrow/max_elapsedt));
 
    /* double check */
    if (tot_nrow != nrow) {
      printf("There's a problem: tot_nrow=%"SCAMACPRIDX" != nrow=%"SCAMACPRIDX"\nThis shouldn't have happened.\n",tot_nrow,nrow);
    }
          
  }

  MPI_Finalize();

  return 0;
}
