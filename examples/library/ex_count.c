#include "scamac.h"

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/**  examples/library/ex_count.c
 *
 *   basic example:
 *   - read a matrix name/argument string from the command line
 *   - count the number of non-zeros, and compute the maximum norm (=max |entry|) and row-sum norm
 *
 *   Matrix rows are generated sequentially.
 */


int main(int argc, char *argv[]) {

 
  char *matargstr = NULL;
  if (argc<=1) {
    printf("usage: ex_count <matrix-argument string>\n\nexample: ex_count Hubbard\n         ex_count Hubbard,n_sites=14,n_fermions=8,U=1.3\n         ex_count TridiagonalReal,subdiag=0.5,supdiag=2\n");
    exit(EXIT_FAILURE);
  }
  matargstr = argv[1];

  ScamacErrorCode err;
  ScamacGenerator *gen;
  char *errstr = NULL;
  
  /* parse matrix name & parameters to obtain a ScamacGenerator ... */
  err = scamac_parse_argstr(matargstr, &gen, &errstr);
  /* ... and check for errors */
  if (err) {
    printf("-- Problem with matrix-argument string:\n-- %s\n---> Abort.\n",errstr);
    exit(EXIT_FAILURE);
  }
 
  /* check matrix parameters */
  err = scamac_generator_check(gen, &errstr);
  if (err) {
    printf("-- Problem with matrix parameters:\n-- %s---> Abort.\n",errstr);
    exit(EXIT_FAILURE);
  }
 
  /* finalize the generator ... */
  err=scamac_generator_finalize(gen);
  /* ... and check, whether the matrix dimension is too large */
  if (err==SCAMAC_EOVERFLOW) {
    printf("-- matrix dimension exceeds max. IDX value (%"SCAMACPRIDX")\n---> Abort.\n",SCAMAC_IDX_MAX);
    exit(EXIT_FAILURE);
  }
  /* catch remaining errors */
  SCAMAC_CHKERR(err);
  
  /* query number of rows and max. number of non-zero entries per row */
  ScamacIdx nrow = scamac_generator_query_nrow(gen);
  ScamacIdx maxnzrow = scamac_generator_query_maxnzrow(gen);

  /* safety check, to avoid sequential generation of huge matrices */
  /* omit this check, if you prefer */
  if (nrow > 10000000) {
    printf("-- Matrix dimension nrow=%" SCAMACPRIDX "\n   is probably too huge for sequential matrix generation.\n---> Abort.\n",nrow);
    exit(EXIT_FAILURE);
  }
  
  /* allocate vectors for column indices and entry values according to maxnzrow */
  ScamacIdx *cind = malloc(maxnzrow * sizeof *cind);
  double *val;
  if (scamac_generator_query_valtype(gen) == SCAMAC_VAL_REAL) {
    val = malloc(maxnzrow * sizeof *val);
  } else {
    /* valtype == SCAMAC_VAL_COMPLEX */
    val = malloc(2*maxnzrow * sizeof *val);
  }
  if (!cind || !val) {
    printf("-- Couldn't allocate cind or val\n---> Abort.\n");
    exit(EXIT_FAILURE);
  }

  /* allocate workspace for matrix generation */
  ScamacWorkspace * ws;
  SCAMAC_TRY(scamac_workspace_alloc(gen, &ws));

  /* result variables */
  double maxnrm=0.0;
  double rownrm=0.0;
  ScamacIdx n_nz=0;
  
  /* generate matrix row by row */
  for (ScamacIdx idx=0; idx<nrow; idx++) {
    ScamacIdx k;
    /* generate single row ... */
    SCAMAC_TRY(scamac_generate_row(gen, ws, idx, SCAMAC_DEFAULT, &k, cind, val));
    /* ... which has 0 <=k <= maxnzrow entries */
    n_nz += k;
    /* update maximum and row-sum norm */
    double locmaxnrm=0.0, locrownrm=0.0;
    if (scamac_generator_query_valtype(gen) == SCAMAC_VAL_REAL) {
      for (ScamacIdx i=0;i<k;i++) {
	locmaxnrm=fmax(locmaxnrm,fabs(val[i]));
	locrownrm=locrownrm+fabs(val[i]);
      }
    } else {
      double complex *cval = (double complex *) val;
      for (ScamacIdx i=0;i<k;i++) {
	locmaxnrm=fmax(locmaxnrm,cabs(cval[i]));
	locrownrm=locrownrm+cabs(cval[i]);
      }
    }
    maxnrm=fmax(maxnrm,locmaxnrm);
    rownrm=fmax(rownrm,locrownrm);
  }

    printf("= matrix =================================================\n\n  %s\n",matargstr);
    printf("\n= results ================================================\n\n  Generated %"SCAMACPRIDX" rows with %"SCAMACPRIDX" non-zeros\n  max. matrix entry: %g\n  row-sum norm: %g\n\n==========================================================\n",  nrow, n_nz,maxnrm,rownrm);

  /* free allocated objects */
  free(cind);
  free(val);
  SCAMAC_TRY(scamac_workspace_free(ws));
  SCAMAC_TRY(scamac_generator_destroy(gen));

  return 0;
}

    
