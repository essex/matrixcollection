#include "scamac.h"

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef _OPENMP
#include <omp.h>
#else
#error "Require OpenMP"
#endif

#ifndef MAX
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif

/**  examples/library/ex_count_openmp.c
 *
 *   basic example:
 *   - read a matrix name/argument string from the command line
 *   - count the number of non-zeros, and compute the maximum norm (=max |entry|) and row-sum norm
 *
 *   Matrix rows are generated in parallel OpenMP threads.
 *
 *   Option "--gpt":
 *
 *   Without "--gpt", the ScamacGenerator is created globally
 *   and shared among all threads.
 *
 *   With "--gpt", one ScamacGenerator is created per thread.
 *
 *   ScamacWorkspace has to be allocated per thread.
 *
 *   Option "--static":
 *   Use static scheduling for parallel generation of matrix rows
 *
 *   Option "--dynamic":
 *   Use dynamic scheduling for parallel generation of matrix rows
 *
 *   Default is "schedule(runtime)"
 */

int main(int argc, char *argv[]) {

 
  char *matargstr = NULL;
  bool genPerThread   =false;
  bool scheduleStatic =false;
  bool scheduleDynamic=false;
  /* read command line */

  int iarg=1;
  while(true) {
    if (iarg>=argc) { break; }
    if (!strcmp(argv[iarg],"--gpt")) {
      genPerThread=true;
    } else if (!strcmp(argv[iarg],"--static")) {
      scheduleStatic=true;
    } else if (!strcmp(argv[iarg],"--dynamic")) {
      scheduleDynamic=true;
    } else {
      break;
    }
    iarg++;
  };
  
  if (iarg<=argc) {
    matargstr = argv[iarg];
  } else {
    matargstr = NULL;
  }
  
  if (!matargstr) {
    printf("usage: ex_count [--gpt] [--static|--dynamic] <matrix-argument string>\n\nexample: ex_count Hubbard\n         ex_count Hubbard,n_sites=14,n_fermions=8,U=1.3\n         ex_count TridiagonalReal,subdiag=0.5,supdiag=2\n");
    exit(EXIT_FAILURE);
  }

  if (scheduleStatic && scheduleDynamic) {
    printf("Options --static and --dynamic are mutually exclusive.\n");
    exit(EXIT_FAILURE);
  }
  if (scheduleStatic) {
    omp_set_schedule(omp_sched_static,0);
  }
  if (scheduleDynamic) {
    omp_set_schedule(omp_sched_dynamic,0);
  }

  ScamacErrorCode err;
  ScamacGenerator *gen;
  char *errstr = NULL;

  /* parse matrix name & parameters to obtain a ScamacGenerator ... */
  err = scamac_parse_argstr(matargstr, &gen, &errstr);
  /* ... and check for errors */
  if (err) {
    printf("-- Problem with matrix-argument string:\n-- %s\n---> Abort.\n",errstr);
    exit(EXIT_FAILURE);
  }
   
  /* check matrix parameters */
  err = scamac_generator_check(gen, &errstr);
  if (err) {
    printf("-- Problem with matrix parameters:\n-- %s---> Abort.\n",errstr);
    exit(EXIT_FAILURE);
  }
  
  /* finalize the generator ... */
  err=scamac_generator_finalize(gen);
  /* ... and check, whether the matrix dimension is too large */
  if (err==SCAMAC_EOVERFLOW) {
    printf("-- matrix dimension exceeds max. IDX value (%"SCAMACPRIDX")\n---> Abort.\n",SCAMAC_IDX_MAX);
    exit(EXIT_FAILURE);
  }
  /* catch remaining errors */
  SCAMAC_CHKERR(err);
  
  /* query number of rows and max. number of non-zero entries per row */
  ScamacIdx nrow = scamac_generator_query_nrow(gen);
  ScamacIdx maxnzrow = scamac_generator_query_maxnzrow(gen);


  /* for statistics: max./min. number of rows generated, and time elapsed, per thread, */
  ScamacIdx min_nrow, max_nrow;
  double min_elapsedt, max_elapsedt;
  int n_threads;

  /* result variables */
  double maxnrm, rownrm;
  ScamacIdx n_nz;

  
#pragma omp parallel
  { /* begin of OpenMP parallel region ... */

    ScamacGenerator *my_gen;
    if (genPerThread) {
      /* Create & finalize generator per thread. No errors should occur. */      
      SCAMAC_TRY(scamac_parse_argstr(matargstr, &my_gen, NULL));
      SCAMAC_TRY(scamac_generator_finalize(my_gen));
    } else {
      /* Use global generator */
      my_gen=gen;
    }

    /* ScamacWorkspace has to be allocated per thread */
    ScamacWorkspace * my_ws;
    SCAMAC_TRY(scamac_workspace_alloc(my_gen, &my_ws));

    /* allocate memory for column indices and values per thread */
    ScamacIdx *cind = malloc(maxnzrow * sizeof *cind);
    double *val;
    if (scamac_generator_query_valtype(my_gen) == SCAMAC_VAL_REAL) {
      val = malloc(maxnzrow * sizeof *val);
    } else {
      /* valtype == SCAMAC_VAL_COMPLEX */
      val = malloc(2*maxnzrow * sizeof *val);
    }

    /* local variables per thread */
    ScamacIdx my_nrow = 0;
    ScamacIdx my_nz = 0;
    double my_maxnrm=0.0;
    double my_rownrm=0.0;

    double t1 = omp_get_wtime();

    /* generate matrix row by row */
#pragma omp for schedule(runtime)
    for (ScamacIdx idx=0; idx<nrow; idx++) {
      ScamacIdx k;
      /* generate single row ... */
      /* my_ws is local to each thread */
      /* for genPerThread==false, my_gen is global and shared among all threads */
      /* for genPerThread==true, my_gen is local to each thread */
      SCAMAC_TRY(scamac_generate_row(my_gen, my_ws, idx, SCAMAC_DEFAULT, &k, cind, val));
      /* ... which has 0 <=k <= maxnzrow entries */
      my_nz += k;
      my_nrow++;
      /* update maximum and row-sum norm */
      double locmaxnrm=0.0, locrownrm=0.0;
      if (scamac_generator_query_valtype(my_gen) == SCAMAC_VAL_REAL) {
	for (ScamacIdx i=0;i<k;i++) {
	  locmaxnrm=fmax(locmaxnrm,fabs(val[i]));
	  locrownrm=locrownrm+fabs(val[i]);
	}
      } else {
	double complex *cval = (double complex *) val;
	for (ScamacIdx i=0;i<k;i++) {
	  locmaxnrm=fmax(locmaxnrm,cabs(cval[i]));
	  locrownrm=locrownrm+cabs(cval[i]);
	}
      }
      my_maxnrm=fmax(my_maxnrm,locmaxnrm);
      my_rownrm=fmax(my_rownrm,locrownrm);
    }
    double t2 = omp_get_wtime();
    
    /* free local objects */
    free(cind);
    free(val);
    SCAMAC_TRY(scamac_workspace_free(my_ws));
    if (genPerThread) {
      SCAMAC_TRY(scamac_generator_destroy(my_gen));
    }

#pragma omp single
    {
      n_threads = omp_get_num_threads();
      n_nz=0;
      maxnrm=0.0;
      rownrm=0,0;
      
      min_nrow=my_nrow;
      max_nrow=my_nrow;
      min_elapsedt=t2-t1;
      max_elapsedt=t2-t1;
    }
    
#pragma omp critical
    {
      /* accumulate results */
      n_nz += my_nz;
      maxnrm=fmax(maxnrm,my_maxnrm);
      rownrm=fmax(rownrm,my_rownrm);

      /* accumulate statistics */
      min_nrow=MIN(min_nrow, my_nrow);
      max_nrow=MAX(max_nrow, my_nrow);
      min_elapsedt=fmin(min_elapsedt, t2-t1);
      max_elapsedt=fmax(max_elapsedt, t2-t1);
    }

  } /* ... end of OpenMP parallel region */
  

  printf("= matrix =================================================\n\n  %s\n",matargstr);
  printf("\n= results ================================================\n\n  Generated %"SCAMACPRIDX" rows with %"SCAMACPRIDX" non-zeros\n  max. matrix entry: %g\n  row-sum norm: %g\n",  nrow, n_nz,maxnrm,rownrm);

  if (genPerThread || scheduleStatic || scheduleDynamic) {
    printf("\n= OpenMP options =============================================\n\n");
    if (genPerThread) {
      printf(" --gpt");
    }
    if (scheduleStatic) {
      printf(" --static");
    }
    if (scheduleDynamic) {
      printf(" --dynamic");
    }
    printf("\n");
  }
  
  printf("\n= statistics =============================================\n\n  no. of threads:       %d\n  min. #rows in thread: %"SCAMACPRIDX"\n  max. #rows in thread: %"SCAMACPRIDX"\n  min. time in thread:  %g\n  max. time in thread:  %g\n  rows/sec:             %g\n==========================================================\n",n_threads,min_nrow,max_nrow,min_elapsedt,max_elapsedt,((double) nrow/max_elapsedt));

  /* destroy global ScamacGenerator */
  SCAMAC_TRY(scamac_generator_destroy(gen));

  return 0;
}
