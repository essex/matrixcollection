#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>

/**  examples/library/ex_scale.c
 *
 *   basic example:
 *   - scale up the dimension of three matrices (Hubbard, Anderson, Tridiagonal)
 *     up to SCAMAC_IDX_MAX (=2^(SCAMAC_IDX-1) = 2^63 for SCAMAC_IDX=64, 2^31 for SCAMAC_IDX=32)
 *
 *   The matrix dimension depends on (integer-valued) matrix parameters,
 *   which are set with scamac_generator_set_int or scamac_generator_set_idx.
 *   To query the matrix dimension with scamac_generator_query_nrow,
 *   generation of matrix rows is not required.
 *
 */

#include "scamac.h"

int main(int argc, char *argv[]) {

  ScamacErrorCode err;
  ScamacGenerator * my_gen;
  char *errstr = NULL;

 
  /* Hubbard */
  
  SCAMAC_TRY(scamac_generator_obtain("Hubbard", &my_gen));

  printf("=====================================================================\n(1) scale dimension of matrix \"Hubbard\"\n\nn_sites n_fermions                      nrows  x                ncols\n=====================================================================\n");

  /* matrix parameters for scaling */
  int n_sites, n_fermions;
  n_sites=2;
  n_fermions=n_sites/2;

  while (true) {

    SCAMAC_TRY(scamac_generator_set_int(my_gen, "n_sites", n_sites));
    SCAMAC_TRY(scamac_generator_set_int(my_gen, "n_fermions", n_fermions));
  
    SCAMAC_TRY(scamac_generator_check(my_gen, &errstr));

    err = scamac_generator_finalize(my_gen);
    if (err==SCAMAC_EOVERFLOW) {
      /* exit regularly: matrix dimension cannot be increased further */
      break;
    }
    /* catch remaining errors */
    SCAMAC_CHKERR(err);
    
    ScamacIdx nrow = scamac_generator_query_nrow(my_gen);
    ScamacIdx ncol = scamac_generator_query_ncol(my_gen);

    printf("%7d    %7d       %20"SCAMACPRIDX"  x %20"SCAMACPRIDX"\n",n_sites,n_fermions, nrow,ncol);

    n_sites++;
    n_fermions=n_sites/2;

  }

  printf("=====================================================================\n");
  
  SCAMAC_TRY(scamac_generator_destroy(my_gen));

  /* Anderson */

  SCAMAC_TRY(scamac_generator_obtain("Anderson", &my_gen));

  printf("=====================================================================\n(2) scale dimension of matrix \"Anderson\"\n\n           L                            nrows  x                ncols\n=====================================================================\n");

  /* matrix parameters for scaling */
  int L;
  L=1;

  while (true) {
    
    SCAMAC_TRY(scamac_generator_set_int(my_gen, "Lx", L));
    SCAMAC_TRY(scamac_generator_set_int(my_gen, "Ly", L));
    SCAMAC_TRY(scamac_generator_set_int(my_gen, "Lz", L));
	       
    SCAMAC_TRY(scamac_generator_check(my_gen, &errstr));
    
    err = scamac_generator_finalize(my_gen);
    if (err==SCAMAC_EOVERFLOW) {
      /* exit regularly: matrix dimension cannot be increased further */
      break;
    }
    /* catch remaining errors */
    SCAMAC_CHKERR(err);
    
    ScamacIdx nrow = scamac_generator_query_nrow(my_gen);
    ScamacIdx ncol = scamac_generator_query_ncol(my_gen);

    printf("%12d             %20"SCAMACPRIDX"  x %20"SCAMACPRIDX"\n",L, nrow,ncol);

    L = 2*L;

  }

  printf("=====================================================================\n");

  SCAMAC_TRY(scamac_generator_destroy(my_gen));

  /* Tridiagonal */

  SCAMAC_TRY(scamac_generator_obtain("Tridiagonal", &my_gen));

  printf("=====================================================================\n(3) scale dimension of matrix \"Tridiagonal\"\n\n                   n                    nrows  x                ncols\n=====================================================================\n");

  /* matrix parameters for scaling */
  ScamacIdx n;
  n=1;

  while (true) {

    /* to allow for n >= 2^31, we use set_idx instead of set_int */
    SCAMAC_TRY(scamac_generator_set_idx(my_gen, "n", n));
    	       
    SCAMAC_TRY(scamac_generator_check(my_gen, &errstr));
    
    err = scamac_generator_finalize(my_gen);
    if (err==SCAMAC_EOVERFLOW) {
      /* exit regularly: matrix dimension cannot be increased further */
      break;
    }
    /* catch remaining errors */
    SCAMAC_CHKERR(err);
    
    ScamacIdx nrow = scamac_generator_query_nrow(my_gen);
    ScamacIdx ncol = scamac_generator_query_ncol(my_gen);

    printf("%20"SCAMACPRIDX"     %20"SCAMACPRIDX"  x %20"SCAMACPRIDX"\n",n, nrow,ncol);

    n = 2*n;

  }

  printf("=====================================================================\n");

  SCAMAC_TRY(scamac_generator_destroy(my_gen));
  
  return 0;
}
